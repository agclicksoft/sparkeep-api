#!/bin/bash

printf "\n\n\nNpm install:"
npm ci

printf "\n\n\nRun migration:\n"
adonis migration:run --force

printf "\n\n\nStart node server:"
adonis serve --dev --polling --debug=0.0.0.0:9229
