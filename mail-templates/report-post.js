exports.report = (post, denunciator, comment) => {
  // return `
  //     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  //     <h4>Recebemos uma denúncia referente ao post com ID: ${post.id}</h4>
  //     <p>Caso queira excluir imediatamente, <a href="/">clique aqui</a></p>
  //     <br>
  //     <div class="d-flex align-items-center">
  //         <h3 class="m-0 mr-2">Autor do post:</h3>
  //         <p class="m-0">${post.user.name}</p>
  //     </div>
  //     <div class="d-flex align-items-center">
  //         <h3 class="m-0 mr-2">Descrição do post:</h3>
  //         <p class="m-0">${post.description}</p>
  //     </div>
  //     <div class="d-flex align-items-center">
  //         <h3 class="m-0 mr-2">Comentário do denunciante:</h3>
  //         <p class="m-0">${comment}</p>
  //     </div>
  //   `

  return `

<table align="center" cellpadding="0" cellspacing="0" width="100%" style="font-family: Helvetica, sans-serif;">
<td align="center" style="background-color: #560F7B; padding: 40px 0 30px 0;">
<img src="https://sparkeep-web.herokuapp.com/assets/img/logo/logo-white.png" alt="Logo" width="150"
    style="display: block;" />
</td>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 50px 30px; color:#560F7B; font-size: 16px; line-height: 20px;">
    <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="color: #560F7B; font-size: 22px; padding: 0 0 10px 0;">
        <p style="line-height:20px;text-align:center;"><b>Recebemos uma denúncia referente ao post com ID:</b></p>
        <p style="color: #ff0000; text-align:center;"><b>${post.id}</b></p>
        </td>
    </tr>
    <tr>
        <td style="color: #272727; font-size: 20px; padding: 10px 0 10px 0;">
        <p style="line-height:20px;text-align:center;"><b>Autor do post:</b></p>
        <p style="text-align:center;">${post.user.name}</p>
        </td>
    </tr>
    <tr>
        <td style="color: #272727; font-size: 20px; padding: 10px 0 10px 0;">
        <p style="line-height:20px;text-align:center;"><b>Descrição do post:</b></p>
        <p style="text-align:center;">${post.description}</p>
        </td>
    </tr>
    <tr>
        <td style="color: #272727; font-size: 20px; padding: 10px 0 10px 0;">
        <p style="line-height:20px;text-align:center;"><b>Motivo da denúncia:</b></p>
        <p style="text-align:center;">${comment}</p>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; padding: 30px 0; ">
        <div
            style="max-width: 230px;margin: 0 auto; border: 2px solid #560F7B; border-radius: 5px; padding: 10px 20px; font-size: 20px;">
          <a href="https://sparkeep-web.herokuapp.com">Clique aqui</a>
        </div>
        </td>
    </tr>
    <tr>
        <td>
        <p style="color: #272727;line-height:27px;">&reg; Sparkeep | Conectando um mundo melhor.</p>
        </td>

    </tr>
    </table>
</td>
</tr>
<tr>
<td style="background-color: #560F7B; padding: 20px 30px 0 30px;">
    <div>
    <table cellpadding="0" cellspacing="0" width="100%"
        style="color: #560F7B; font-size: 16px; line-height: 20px;">
        <tr>
        <td style="color: #ffffff; font-size: 14px; padding-right:20px;line-height:22px;">
          &reg; Sparkeep | Conectando um mundo melhor.<br />
          Essa mensagem é automática, não é necessário respondê-la.
        </td>
        <td align="right">
            <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                <img src="https://sparkeep-web.herokuapp.com/assets/img/logo/logo-white.png"
                    alt="logotipo" width="80">
                </td>
            </tr>
            </table>
        </td>
        </tr>
    </table>
    </div>
    <div>
    <p style="font-size: 12px; padding: 5px 0 6px 0;color: #fff;text-align:center;">
        <a href="https://clicksoft.com.br" style="text-decoration: none; color: #fff">
        Desenvolvido por Clicksoft</a>
    </p>
    </div>
</td>
</tr>
</table>
  `;
};
