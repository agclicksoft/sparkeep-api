exports.forgotPassword = (code, name) => {
    return `
    <table align="center" cellpadding="0" cellspacing="0" width="100%">
        <td align="center" style="background-image: linear-gradient(to right, #392244, #560F7B);padding: 40px 0 30px 0;">
        <img src="https://sparkeep-web.herokuapp.com/assets/img/logo/logo-white.png" alt="Logo" width="150"
            style="display: block;" />
        </td>
        <tr>
        <td bgcolor="#ffffff" style="padding: 40px 30px 50px 30px; color: #392244; font-size: 16px; line-height: 20px;">
            <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="color: #560F7B; font-size: 22px; padding: 0 0 10px 0;">
                <p style="line-height:30px;text-align:center;color:#272727;">Olá ${name}.</p>
                <p style="line-height:33px;text-align:center;"><b>Recebemos seu pedido de redefinição de senha do
                    Sparkeep</b></p>
                </td>
            </tr>
            <tr>
                <td style="color: #272727;padding: 0;">
                <p style="line-height:27px;">O código de recuperação de senha é este abaixo.</p>
            </tr>
            <tr>
                <td style="text-align: center; padding: 30px 0; ">
                <div
                    style="max-width: 230px;margin: 0 auto; border: 2px solid #560F7B; border-radius: 5px; padding: 10px 20px; font-size: 20px;">
                    ${code}</div>
                </td>
            </tr>
            <tr>
                <td>
                <p style="color: #272727;line-height:27px;">Este código irá expirar em 10 minutos.</br>Se você não fez
                    essa solicitação, ignore este e-mail.
                    <br>O Sparkeep nunca solicita que você informe sua senha por e-mail.</p>
                </td>

            </tr>
            </table>
        </td>
        </tr>
        <tr>
        <td style="background-image: linear-gradient(to right, #392244, #560F7B); padding: 20px 30px 0 30px;">
            <div>
            <table cellpadding="0" cellspacing="0" width="100%"
                style="color: #392244; font-size: 16px; line-height: 20px;">
                <tr>
                <td style="color: #ffffff; font-size: 14px; padding-right:20px;line-height:22px;">
                    &reg; Sparkeep, ação, empatia, amor e compaixão.<br>
                    Essa mensagem é automática, não é necessário respondê-la.
                </td>
                <td align="right">
                    <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                        <img src="https://sparkeep-web.herokuapp.com/assets/img/logo/logo-white.png"
                            alt="logotipo" width="80">
                        </td>
                        <!-- <td>
                        <a href="https://www.linkedin.com/company/sparkeep/" target="_blank">
                            <img src="https://trezed.app/assets/icons/linkedin.png" alt="LinkedIn" width="24" height="24" style="display: block;" />
                        </a>
                        </td>
                        <td style="font-size: 0; line-height: 0;" width="15px">&nbsp;</td>
                        <td>
                        <a href="https://www.facebook.com/sparkeep/" target="_blank">
                            <img src="https://trezed.app/assets/icons/facebook.png" alt="Facebook" width="24" height="24" style="display: block;" />
                        </a>
                        </td>
                        <td style="font-size: 0; line-height: 0;" width="15px">&nbsp;</td>
                        <td>
                        <a href="https://www.instagram.com/sparkeep/?hl=pt-br" target="_blank">
                            <img src="https://trezed.app/assets/icons/instagram.png" alt="Instagram" width="24" height="24" style="display: block;" />
                        </a>
                        </td> -->
                    </tr>
                    </table>
                </td>
                </tr>
            </table>
            </div>
            <div>
            <p style="font-size: 12px; padding: 5px 0 6px 0;color: #fff;text-align:center;">
                <a href="https://clicksoft.com.br" style="text-decoration: none; color: #fff">
                Desenvolvido por Clicksoft</a>
            </p>
            </div>
        </td>
        </tr>
    </table>`
}