'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group( () => {
    Route.post('posts/:id/like', 'ActionController.like')
    Route.post('posts/:id/report', 'ActionController.report')
}).prefix('v1').namespace('Post/Actions')

Route.group( () => {
    Route.get('posts/my-interests', 'PostController.index')
}).prefix('v1').namespace('Post/MyInterests')

Route.group( () => {
    Route.get('posts/global', 'PostController.index')
}).prefix('v1').namespace('Post/Global')

Route.group( () => {
    Route.get('posts/my-network', 'PostController.index')
}).prefix('v1').namespace('Post/MyNetwork')

Route.group( () => {
    Route.resource('posts', 'PostController')
    Route.post('posts/:id/uploadfiles', 'PostController.uploadFile')
}).prefix('v1').namespace('Post')
