'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group( () => {
    Route.put('events/administrators/:id', 'administratorController.update')
}).prefix('v1').namespace('Event/Administrators')

Route.group( () => {
    Route.get('events/:id/confirmed-persons', 'ConfirmedPersonsController.index')
    Route.post('events/:id/confirm', 'ConfirmedPersonsController.create')
}).prefix('v1').namespace('Event/ConfirmedPersons')

Route.group( () => {
    Route.get('events/:id/posts', 'PostController.index')
}).prefix('v1').namespace('Event/Posts')

Route.group( () => {
    Route.post('events/:id/attachment', 'EventController.uploadAttachment')
    Route.resource('events', 'EventController')
}).prefix('v1').namespace('Event')