'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group( () => {
    Route.get('users/:id/administrator-proposals', 'AdministratorController.index')
    Route.put('users/:user_id/:role/:id', 'AdministratorController.update')
}).prefix('v1').namespace('User/Administrators')

Route.group( () => {
    Route.get('users/:id/request-partners', 'RequestPartnerController.index')
    Route.get('users/:id/partners', 'PartnerController.index')
}).prefix('v1').namespace('User/Partners')

Route.group( () => {
    Route.post('users/:id/chat-badge', 'NotificationController.viewBadge')
    Route.get('users/:id/chat-badge', 'NotificationController.chatBadge')
    Route.get('users/:id/notifications-badge', 'NotificationController.notViewed')
    Route.get('users/:id/notifications', 'NotificationController.index')
    Route.post('users/:id/notifications', 'NotificationController.view')
}).prefix('v1').namespace('User/Notifications')

Route.group( () => {
    Route.get('users/:id/organizations', 'OrganizationController.index')
}).prefix('v1').namespace('User/Organizations')

Route.group( () => {
    Route.get('users/:id/projects', 'ProjectController.index')
}).prefix('v1').namespace('User/Projects')

Route.group( () => {
    Route.get('users/:id/events', 'EventController.index')
}).prefix('v1').namespace('User/Events')

Route.group( () => {
    Route.get('users/:id/posts', 'PostController.index')
}).prefix('v1').namespace('User/Posts')

Route.group( () => {
    Route.post('users/attachment', 'UserController.uploadAttachment')
    Route.resource('users', 'UserController')
}).prefix('v1').namespace('User')