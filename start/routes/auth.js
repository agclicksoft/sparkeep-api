'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group( () => {
    Route.get('auth/account', 'AuthController.account')
    Route.post('auth/signin', 'AuthController.signIn')
    Route.post('auth/signup', 'AuthController.signUp')
    Route.post('auth/forgotpassword', 'AuthController.forgotPassword')
    Route.post('auth/validatecode', 'AuthController.validateCode')
    Route.post('auth/resetpassword', 'AuthController.resetPassword')
    Route.post('auth/refresh', 'AuthController.refresh')
    Route.post('auth/teste', 'AuthController.teste')
    Route.post('auth/insp', 'AuthController.insp')
}).prefix('v1').namespace('Auth')

Route.group( () => {
    Route.post('social/login', 'AuthController.login')
}).prefix('v1').namespace('Auth/Social')
