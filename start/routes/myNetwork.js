'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group( () => {

    Route.resource('my-network/users', 'userController').only(['index']);
    Route.resource('my-network/events', 'eventController').only(['index']);
    Route.resource('my-network/projects', 'projectController').only(['index']);
    Route.resource('my-network/organizations', 'organizationController').only(['index']);

}).prefix('v1').namespace('MyNetwork')