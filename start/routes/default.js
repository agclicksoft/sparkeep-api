'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

const Passion = use('App/Models/Passion');

const Shortage = use('App/Models/Shortage');

const User = use('App/Models/User');

const MaintenanceTime = use('App/Models/MaintenanceTime')

const Events = use('App/Models/Event');

const Project = use('App/Models/Project');

const Organization = use('App/Models/Organization');

const Post = use('App/Models/Post');

// DADOS PADRÕES PARA CRIAR PAIXÕES
const passions_default = [
  {name: 'Filantropia e Assistência Social', description: 'Lorem ipsum dolor sit a met', order: 1},
  {name: 'Cultura', description: 'Lorem ipsum dolor sit a met', order: 4},
  {name: 'Direito dos Animais', description: 'Lorem ipsum dolor sit a met', order: 5},
  {name: 'Direitos Humanos', description: 'Lorem ipsum dolor sit a met', order: 6},
  {name: 'Esporte e Lazer', description: 'Lorem ipsum dolor sit a met', order: 7},
  {name: 'Meio Ambiente', description: 'Lorem ipsum dolor sit a met', order: 8},
  {name: 'Saúde', description: 'Lorem ipsum dolor sit a met', order: 9},
  {name: 'Espiritualidade', description: 'Lorem ipsum dolor sit a met', order: 10},
  {name: 'Pesquisa e Educação', description: 'Lorem ipsum dolor sit a met', order: 11},
  {name: 'Política', description: 'Lorem ipsum dolor sit a met', order: 12},
  {name: 'Economia Social', description: 'Lorem ipsum dolor sit a met', order: 2},
  {name: 'Motivação', description: 'Lorem ipsum dolor sit a met', order: 3},
]

const shortage_default = [
    {description: 'Patrocínio material'},
    {description: 'Patrocínio financeiro'},
    {description: 'Suporte local'},
    {description: 'Suporte remoto'}
]

const maintenance_time_default = [
    {description: 'Diário'},
    {description: 'Uma vez por semana'},
    {description: 'De 15 em 15 dias'},
    {description: 'Uma vez por mês'},
    {description: 'Uma vez por ano '},
    {description: 'Uma única vez'}
]

const user_default = [
    {
        name: 'João Batista',
        email: 'usuario@mail.com',
        password: 'root',
        accepted_terms: true,
    },
    {
        name: 'Claudio Moreira',
        email: 'usuario02@mail.com',
        password: 'root'
    },
    {
        name: 'Luciana Chaves',
        email: 'usuario03@mail.com',
        password: 'root'
    }
]

const event_default = {
    user_id: 1,
    organization_id: null,
    project_id: null,
    name: "Doação de roupas e cobertores para moradores de rua.",
    date: "2020-12-30",
    start_time: "12:00",
    end_time: null,
    description: "Nas noites frias de inverno, enquanto estamos abrigados do frio e da chuva muitos moradores de rua estão passando por necessidades. Ajude com o que puder, aquela roupa que está há um tempo no armário, ou aquele cobertor rasgado serão muito bem vindos",
    local: "Praça XV",
    address_country: "Brazil",
    address_state: "Rio de Janeiro",
    address_city: "Rio de Janeiro",
    address_neighborhood: "Centro",
    address_street: "Rua primeiro de março",
    address_complement: null,
    address_number: null,
    google_maps_url: null,
    image_url: "https://cdn.falauniversidades.com.br/wp-content/uploads/2018/06/street-store-doa%C3%A7%C3%A3o-roupas-fala-2-1.jpeg",
    telephone: "21999999999",
    frequency: null,
}

const event_default_administrators = [2, 3];

const event_default_passions = [1, 4, 6, 7];

const event_default_shortages = [1, 2, 3];

const project_default = {
    user_id: 1,
    organization_id: null,
    name: "Comunidade Social",
    description: "Projeto destinado à ajuda comunitária de diversos grupos e comunidades. Nosso objetivo é conseguir ajuda para os grupos que necessitam de ajuda.",
    local: "Rio de Janeiro",
    telephone: "21999999999",
    show_telephone: true,
    image_url: "https://comps.canstockphoto.com.br/sociedade-pessoas-comunidade-social-imagem_csp44734179.jpg",
}

const project_default_administrators = [2, 3];

const project_default_passions = [1, 4];

const project_default_shortages = [1, 2, 3, 4];

const organization_default = {
    user_id: 1,
    project_id: null,
    cnpj: '00000000000000',
    address_country: 'Brazil',
    address_state: 'RJ',
    address_city: 'Nova Iguaçu',
    address_neighborhood: 'Centro',
    address_street: 'Rua a',
    name: "ONG - Viver é mais",
    description: "Organização Social destinada para ajudar todas aqueles que necessitam de amparo, sem distinção de cor, raça, religião, gênero ou qualquer outro tipo de rótulo.",
    local: "Rio de Janeiro",
    telephone: "21999999999",
    image_url: "https://vivermais.org/wp-content/uploads/2020/03/VIVER-MAIS_TRANSP-CONTORNO-BRANCO-01.png",
    cnpj: null,
}

const organization_default_administrators = [2, 3];

const organization_default_passions = [1, 4, 7];

const organization_default_maintenance = [1];

const posts_default = [
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 1,
        author: "user",
        author_user_id: 1,
        description: "Ação social que aconteceu no Aterro do Flamengo. Gratidão por poder ajudar e fazer parte dessa ação linda.",
        privacy: "public",
    },
    {
        user_id: 2,
        author: "user",
        author_user_id: 2,
        description: "Procuro parcerias para ações sociais.",
        privacy: "public",
    },
    {
        user_id: 3,
        author: "user",
        author_user_id: 3,
        description: "Parabéns a todos pelas mensagens de carinho ! Muito feliz em poder ajudar os necessitados",
        privacy: "public",
    },
    {
        author: "event",
        author_event_id: 1,
        description: "Obrigado a todos que compareceram ao nosso último evento. Gratidão !",
        privacy: "public",
    },
    {
        author: "project",
        author_project_id: 1,
        description: "Procuramos trabalhadores voluntários para nos ajudar com nossos projetos.",
        privacy: "public",
    },
    {
        author: "organization",
        author_organization_id: 1,
        description: "Conhece alguma comunidade que esteja precisando de cuidados? Entre em contato conosco e ficaremos felizes em ajudar",
        privacy: "public",
    },
]


Route.group( () => {

    //ROTA POST PARA /V1 PARA CRIAR REGISTROS PADRÕES
    Route.post('', async ({request, response}) => {

        const passions = await Passion.createMany(passions_default);

        const shortages = await Shortage.createMany(shortage_default);

        // const user = await User.createMany(user_default);

        const maintenance = await MaintenanceTime.createMany(maintenance_time_default);

        // const event = await Events.create(event_default);
        //               await event.administrators().attach(event_default_administrators);
        //               await event.passions().attach(event_default_passions);
        //               await event.shortages().attach(event_default_shortages);


        // const project = await Project.create(project_default);
        //                 await project.administrators().attach(project_default_administrators);
        //                 await project.passions().attach(project_default_passions);
        //                 await project.shortages().attach(project_default_shortages);

        // const organization = await Organization.create(organization_default);
        //                      await organization.administrators().attach(organization_default_administrators);
        //                      await organization.passions().attach(organization_default_passions);

        // const posts = await Post.createMany(posts_default)

        // return {passions, shortages, maintenance, user, event, project, organization, posts};

        return { passions, shortages, maintenance }
    } )
}).prefix('v1')

