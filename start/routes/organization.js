'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group( () => {
    Route.put('organizations/administrators/:id', 'administratorController.update')
}).prefix('v1').namespace('Organization/Administrators')

Route.group( () => {
    Route.get('organizations/:id/events', 'EventController.index')
}).prefix('v1').namespace('Organization/Events')

Route.group( () => {
    Route.get('organizations/:id/projects', 'ProjectController.index')
}).prefix('v1').namespace('Organization/Projects')

Route.group( () => {
    Route.get('organizations/:id/partners', 'PartnerController.index')
}).prefix('v1').namespace('Organization/Partners')

Route.group( () => {
    Route.get('organizations/:id/posts', 'PostController.index')
}).prefix('v1').namespace('Organization/Posts')

Route.group( () => {
    Route.post('organizations/:id/attachment', 'OrganizationController.uploadAttachment')
    Route.resource('organizations', 'OrganizationController')
}).prefix('v1').namespace('Organization')
