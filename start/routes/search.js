'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group( () => {
    Route.post('search/location', 'LocationController.index')
}).prefix('v1').namespace('Search/Location')

Route.group( () => {
    Route.post('search/shortages', 'ShortageController.index')
}).prefix('v1').namespace('Search/Shortages')

Route.group( () => {
    Route.post('search/passions', 'PassionController.index')
}).prefix('v1').namespace('Search/Passions')
