'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group( () => {
    Route.put('projects/administrators/:id', 'administratorController.update')
}).prefix('v1').namespace('Project/Administrators')

Route.group( () => {
    Route.get('projects/:id/partners', 'PartnerController.index')
}).prefix('v1').namespace('Project/Partners')

Route.group( () => {
    Route.get('projects/:id/events', 'EventController.index')
}).prefix('v1').namespace('Project/Events')

Route.group( () => {
    Route.get('projects/:id/posts', 'PostController.index')
}).prefix('v1').namespace('Project/Posts')

Route.group( () => {
    Route.post('projects/:id/attachment', 'ProjectController.uploadAttachment')
    Route.resource('projects', 'ProjectController')
}).prefix('v1').namespace('Project')
