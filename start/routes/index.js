'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

require('./auth');

require ('./user');

require('./passion');

require('./shortage');

require ('./post');

require ('./project');

require('./event');

require('./organization');

require('./default');

require('./partnership');

require('./maintenanceTime');

require('./search');

require('./inspire');

require('./myNetwork');