'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Shortage extends Model {
    user() {
        return this.belongsTo('App/Models/User')
    }

    posts() {
        return this.belongsTo('App/Models/Post')
    }

    event() {
        return this.belongsTo('App/Models/Event')
    }

    organization() {
        return this.belongsTo('App/Models/Organization')
    }

    project() {
        return this.belongsTo('App/Models/Project')
    }
}

module.exports = Shortage
