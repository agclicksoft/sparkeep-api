'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Organization extends Model {
    user() {
        return this.belongsTo('App/Models/User')
    }

    // CRIADOR DA ORGANIZAÇÃO
    project() {
        return this.belongsTo('App/Models/Project')
    }

    // PROJETOS CRIADOS PELA ORGANIZAÇÃO
    projects() {
        return this.hasMany('App/Models/Project')
    }

    // EVENTOS CRIADOS PELA ORGANIZAÇÃO
    events() {
        return this.hasMany('App/Models/Event')
    }

    passions() {
        return this.belongsToMany('App/Models/Passion')
            .pivotTable('organizations_passions')
    }

    administrators() {
        return this.belongsToMany('App/Models/User')
            .pivotTable('organizations_administrators')
            .withPivot(['id', 'status'])
    }

    posts() {
        return this.hasMany('App/Models/Post')
    }

    send_partners() {
        return this.hasMany('App/Models/Partner', 'id', 'organization_send_id')
    }

    received_partners() {
        return this.hasMany('App/Models/Partner', 'id', 'organization_receiver_id')
    }

    inspireds() {
        return this.hasMany('App/Models/Inspired', 'id', 'organization_inspiration_id')
    }

    userInspireds() {
        return this.belongsToMany('App/Models/User', 'organization_inspiration_id', 'user_inspired_id')
            .pivotTable('inspireds')
    }

    // inspirations() {
    //     return this.hasMany('App/Models/Inspired', 'id', 'organization_inspired_id')
    // }

    //PARCERIAS QUE O PROJETO ENVIOU SOLICITAÇÃO PARA UM USUÁRIO
    partnerSendToUser() {
        return this.belongsToMany('App/Models/User', 'organization_send_id', 'user_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO RECEBEU SOLICITAÇÃO DE UM USUÁRIO
    partnerReceivedFromUser() {
        return this.belongsToMany('App/Models/User', 'organization_receiver_id', 'user_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO ENVIOU SOLICITAÇÃO PARA UM EVENTO
    partnerSendToEvent() {
        return this.belongsToMany('App/Models/Event', 'organization_send_id', 'event_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO RECEBEU SOLICITAÇÃO DE UM EVENTO
    partnerReceivedFromEvent() {
        return this.belongsToMany('App/Models/Event', 'organization_receiver_id', 'event_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO ENVIOU SOLICITAÇÃO PARA UM PROJETO
    partnerSendToProject() {
        return this.belongsToMany('App/Models/Project', 'organization_send_id', 'project_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO RECEBEU SOLICITAÇÃO DE UM PROJETO
    partnerReceivedFromProject() {
        return this.belongsToMany('App/Models/Project', 'organization_receiver_id', 'project_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO ENVIOU SOLICITAÇÃO PARA UMA ORGANIZAÇÃO
    partnerSendToOrganization() {
        return this.belongsToMany('App/Models/Organization', 'organization_send_id', 'organization_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO RECEBEU SOLICITAÇÃO DE UMA ORGANIZAÇÃO
    partnerReceivedFromOrganization() {
        return this.belongsToMany('App/Models/Organization', 'organization_receiver_id', 'organization_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }
}

module.exports = Organization
