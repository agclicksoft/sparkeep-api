'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class OrganizationsPassion extends Model {
    organization() {
        return this.belongsTo('App/Models/Organization')
    }

    passion() {
        return this.belongsTo('App/Models/Passion')
    }
}

module.exports = OrganizationsPassion
