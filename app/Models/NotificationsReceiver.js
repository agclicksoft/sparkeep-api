'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class NotificationsReceiver extends Model {
    user_receiver() {
        return this.belongsTo('App/Models/User')
    }

    notification() {
        return this.belongsTo('App/Models/Notification')
    }
}

module.exports = NotificationsReceiver
