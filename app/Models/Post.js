'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const moment = require('moment');

class Post extends Model {
    user() {
        return this.belongsTo('App/Models/User')
    }
    
    authorUser() {
        return this.hasOne('App/Models/User', 'author_user_id', 'id')
    }

    authorEvent() {
        return this.hasOne('App/Models/Event', 'author_event_id', 'id')
    }

    authorOrganization() {
        return this.hasOne('App/Models/Organization', 'author_organization_id', 'id')
    }

    authorProject() {
        return this.hasOne('App/Models/Project', 'author_project_id', 'id')
    }

    images() {
        return this.hasMany('App/Models/PostsImage')
    }

    passions() {
        return this.belongsToMany('App/Models/Passion', 'post_id', 'passion_id')
            .pivotTable('posts_passions')
    }

    inspireds() {
        return this.hasMany('App/Models/Inspired', 'id', 'post_inspiration_id')
    }

    userNotifications() {
        return this.hasOne('App/Models/Notification', 'author_user_id', 'user_send_id')
    }

    eventNotifications() {
        return this.hasOne('App/Models/Notification', 'author_event_id', 'event_send_id')
    }

    projectNotifications() {
        return this.hasOne('App/Models/Notification', 'author_project_id', 'project_send_id')
    }

    organizationNotifications() {
        return this.hasOne('App/Models/Notification', 'author_organization_id', 'organization_send_id')
    }

    // inspirations() {
    //     return this.hasMany('App/Models/Inspired')
    // }
}

module.exports = Post
