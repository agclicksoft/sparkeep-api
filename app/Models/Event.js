'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Event extends Model {

    /**
     * 1:1
     * HAS ONE...
     */
    maintenance_time() {
        return this.hasOne('App/Models/MaintenanceTime')
    }

    /** 
     * N:1
     * ...BELONGS TO AN SINGLE EVENT
    */
    user() {
        return this.belongsTo('App/Models/User')
    }

    project() {
        return this.belongsTo('App/Models/Project')
    }

    organization() {
        return this.belongsTo('App/Models/Organization')
    }

    /** 
     * 1:N
     * AN EVENT HAS MANY...
    */
    posts() {
        return this.hasMany('App/Models/Post')
    }

    partners() {
        return this.hasMany('App/Models/Partner')
    }

    // inspireds() {
    //     return this.hasMany('App/Models/Inspired', 'id', 'event_inspiration_id')
    // }

    // inspirations() {
    //     return this.hasMany('App/Models/Inspired', 'id', 'event_inspired_id')
    // }

    /** 
     * N:N
     * AN EVENT HAS MANY AND BELONGS TO MANY ...
    */
    passions() {
        return this.belongsToMany('App/Models/Passion')
            .pivotTable('events_passions')
    }

    shortages() {
        return this.belongsToMany('App/Models/Shortage', 'event_id', 'shortage_id')
            .pivotTable('events_shortages')
    }

    confirmedPeople() {
        return this.belongsToMany('App/Models/User')
            .pivotTable('events_confirmed_people')
    }

    administrators() {
        return this.belongsToMany('App/Models/User')
            .pivotTable('events_administrators')
            .withPivot(['id', 'status'])
    }

    //PARCERIAS QUE O EVENTO ENVIOU SOLICITAÇÃO PARA UM USUÁRIO
    partnerSendToUser() {
        return this.belongsToMany('App/Models/User', 'event_send_id', 'user_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O EVENTO RECEBEU SOLICITAÇÃO DE UM USUÁRIO
    partnerReceivedFromUser() {
        return this.belongsToMany('App/Models/User', 'event_receiver_id', 'user_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O EVENTO ENVIOU SOLICITAÇÃO PARA UM EVENTO
    partnerSendToEvent() {
        return this.belongsToMany('App/Models/Event', 'event_send_id', 'event_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O EVENTO RECEBEU SOLICITAÇÃO DE UM EVENTO
    partnerReceivedFromEvent() {
        return this.belongsToMany('App/Models/Event', 'event_receiver_id', 'event_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O EVENTO ENVIOU SOLICITAÇÃO PARA UM PROJETO
    partnerSendToProject() {
        return this.belongsToMany('App/Models/Project', 'event_send_id', 'project_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O EVENTO RECEBEU SOLICITAÇÃO DE UM PROJETO
    partnerReceivedFromProject() {
        return this.belongsToMany('App/Models/Project', 'event_receiver_id', 'project_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O EVENTO ENVIOU SOLICITAÇÃO PARA UMA ORGANIZAÇÃO
    partnerSendToOrganization() {
        return this.belongsToMany('App/Models/Organization', 'event_send_id', 'organization_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O EVENTO RECEBEU SOLICITAÇÃO DE UMA ORGANIZAÇÃO
    partnerReceivedFromOrganization() {
        return this.belongsToMany('App/Models/Organization', 'event_receiver_id', 'organization_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    maintenance_times() {
        return this.belongsToMany('App/Models/MaintenanceTime')
            .pivotTable('events_maintenance_times')
    }

}

module.exports = Event
