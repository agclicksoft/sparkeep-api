'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PostsPassion extends Model {
    post() {
        return this.belongsTo('App/Models/Post')
    }

    passion() {
        return this.belongsTo('App/Models/Passion')
    }
}

module.exports = PostsPassion
