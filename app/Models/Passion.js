'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Passion extends Model {
    users() {
        return this.belongsToMany('App/Models/User')
    }

    projects() {
        return this.belongsToMany('App/Models/Project')
    }

    events() {
        return this.belongsToMany('App/Models/Event')
    }

    organizations() {
        return this.belongsToMany('App/Models/Organization')
    }
    
    posts() {
        return this.belongsToMany('App/Models/Post')
    }
}

module.exports = Passion
