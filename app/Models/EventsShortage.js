'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class EventsShortage extends Model {
    event() {
        return this.belongsTo('App/Models/Event')
    }

    shortage() {
        return this.belongsTo('App/Models/Shortage')
    }
}

module.exports = EventsShortage
