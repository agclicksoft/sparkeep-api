'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class OrganizationsAdministrator extends Model {
    organization() {
        return this.belongsTo('App/Models/Organization')
    }

    user() {
        return this.belongsTo('App/Models/User')
    }
}

module.exports = OrganizationsAdministrator
