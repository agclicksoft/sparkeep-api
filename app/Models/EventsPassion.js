'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class EventsPassion extends Model {
    event() {
        return this.belongsTo('App/Models/Event')
    }

    passion() {
        return this.belongsTo('App/Models/Passion')
    }
}

module.exports = EventsPassion
