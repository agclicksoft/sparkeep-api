'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserShortage extends Model {
    user() {
        return this.belongsTo('App/Models/User')
    }

    shortage() {
        return this.belongsTo('App/Models/Shortage')
    }
}

module.exports = UserShortage
