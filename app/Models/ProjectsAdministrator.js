'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProjectsAdministrator extends Model {
    project() {
        return this.belongsTo('App/Models/Project')
    }

    administrator() {
        return this.belongsTo('App/Models/User')
    }

    user() {
      return this.belongsTo('App/Models/User')
    }
}

module.exports = ProjectsAdministrator
