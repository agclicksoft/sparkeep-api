'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Project extends Model {
    user() {
        return this.belongsTo('App/Models/User')
    }

    maintenance_time() {
        return this.hasOne('App/Models/MaintenanceTime')
    }

    organization() {
        return this.belongsTo('App/Models/Organization')
    }

    organizations() {
        return this.hasMany('App/Models/Organizations')
    }

    events() {
        return this.hasMany('App/Models/Event')
    }

    passions() {
        return this.belongsToMany('App/Models/Passion')
            .pivotTable('projects_passions')
    }

    shortages() {
        return this.belongsToMany('App/Models/Shortage', 'project_id', 'shortage_id')
            .pivotTable('projects_shortages')
    }

    administrators() {
        return this.belongsToMany('App/Models/User')
            .pivotTable('projects_administrators')
            .withPivot(['id', 'status'])
    }

    posts() {
        return this.hasMany('App/Models/Post')
    }

    partners() {
        return this.hasMany('App/Models/Partner')
    }

    // inspireds() {
    //     return this.hasMany('App/Models/Inspired', 'id', 'project_inspiration_id')
    // }

    // inspirations() {
    //     return this.hasMany('App/Models/Inspired', 'id', 'project_inspired_id')
    // }

    //PARCERIAS QUE O PROJETO ENVIOU SOLICITAÇÃO PARA UM USUÁRIO
    partnerSendToUser() {
        return this.belongsToMany('App/Models/User', 'project_send_id', 'user_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO RECEBEU SOLICITAÇÃO DE UM USUÁRIO
    partnerReceivedFromUser() {
        return this.belongsToMany('App/Models/User', 'project_receiver_id', 'user_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO ENVIOU SOLICITAÇÃO PARA UM EVENTO
    partnerSendToEvent() {
        return this.belongsToMany('App/Models/Event', 'project_send_id', 'event_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO RECEBEU SOLICITAÇÃO DE UM EVENTO
    partnerReceivedFromEvent() {
        return this.belongsToMany('App/Models/Event', 'project_receiver_id', 'event_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO ENVIOU SOLICITAÇÃO PARA UM PROJETO
    partnerSendToProject() {
        return this.belongsToMany('App/Models/Project', 'project_send_id', 'project_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO RECEBEU SOLICITAÇÃO DE UM PROJETO
    partnerReceivedFromProject() {
        return this.belongsToMany('App/Models/Project', 'project_receiver_id', 'project_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO ENVIOU SOLICITAÇÃO PARA UMA ORGANIZAÇÃO
    partnerSendToOrganization() {
        return this.belongsToMany('App/Models/Organization', 'project_send_id', 'organization_receiver_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    //PARCERIAS QUE O PROJETO RECEBEU SOLICITAÇÃO DE UMA ORGANIZAÇÃO
    partnerReceivedFromOrganization() {
        return this.belongsToMany('App/Models/Organization', 'project_receiver_id', 'organization_send_id')
        .pivotTable('partners')
        .withPivot(['id', 'status', 'message', 'partnership_agreement', 'created_at', 'shortage_id', 'maintenance_time_id'])
    }

    maintenance_times() {
        return this.belongsToMany('App/Models/MaintenanceTime')
            .pivotTable('projects_maintenance_times')
    }
    
}

module.exports = Project
