'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  passions() {
    return this.belongsToMany('App/Models/Passion')
      .pivotTable('user_passions')
  }

  shortage() {
    return this.belongsToMany('App/Models/Shortage')
      .pivotTable('user_shortages')
  }

  posts() {
    return this.hasMany('App/Models/Post')
  }

  notifications() {
    return this.hasMany('App/Models/Notification', 'id', 'user_send_id' )
  }

  connectionsSend() {
    return this.hasMany('App/Models/UserConnection', 'id', 'user_send_id')
  }

  connectionsReceived() {
    return this.hasMany('App/Models/UserConnection', 'id', 'user_receiver_id')
  }

  events() {
    return this.hasMany('App/Models/Event')
  }

  projects() {
    return this.hasMany('App/Models/Project')
  }

  organizations() {
    return this.hasMany('App/Models/Organization')
  }

  inspireds() {
    return this.hasMany('App/Models/Inspired', 'id', 'user_inspiration_id')
  }

  interest_regions() {
    return this.hasMany('App/Models/UserInterestRegion', 'id', 'user_id')
  }

  userInspireds() {
    return this.belongsToMany('App/Models/User', 'user_inspiration_id', 'user_inspired_id')
        .pivotTable('inspireds')
}

  // projectInspireds() {
  //   return this.hasMany('App/Models/Inspired', 'id', 'project_inspiration_id')
  // }

  organizationInspireds() {
    return this.hasMany('App/Models/Inspired', 'id', 'organization_inspiration_id')
  }

  // eventInspireds() {
  //   return this.hasMany('App/Models/Inspired', 'id', 'event_inspiration_id')
  // }

  postInspireds() {
    return this.hasMany('App/Models/Inspired', 'id', 'post_inspiration_id')
  }

  inspirations() {
    return this.hasMany('App/Models/Inspired', 'id', 'user_inspired_id')
  }

  userInspirations() {
    return this.hasMany('App/Models/Inspired', 'id', 'user_inspired_id' )
  }

  // projectInspirations() {
  //   return this.hasMany('App/Models/Inspired', 'id', 'project_inspired_id' )
  // }

  // organizationInspirations() {
  //   return this.hasMany('App/Models/Inspired', 'id', 'organization_inspired_id' )
  // }

  // eventInspirations() {
  //   return this.hasMany('App/Models/Inspired', 'id', 'event_inspired_id' )
  // }

  // postInspirations() {
  //   return this.hasMany('App/Models/Inspired', 'id', 'post_inspired_id' )
  // }

  //PARCERIAS QUE O USUÁRIO ENVIOU SOLICITAÇÃO
  partnerSendToUser() {
    return this.belongsToMany('App/Models/User', 'user_send_id', 'user_receiver_id')
      .pivotTable('partners')
      .withPivot(['id', 'status', 'message', 'partnership_agreement', 'shortage_id', 'message', 'maintenance_time_id'])
  }

  //PARCERIAS QUE O USUÁRIO RECEBEU SOLICITAÇÃO
  partnerReceivedFromUser() {
    return this.belongsToMany('App/Models/User', 'user_receiver_id', 'user_send_id')
      .pivotTable('partners')
      .withPivot(['id', 'status', 'message', 'partnership_agreement', 'shortage_id', 'message', 'maintenance_time_id'])
  }

  //PARCERIAS QUE O USUÁRIO ENVIOU SOLICITAÇÃO
  partnerSendToEvent() {
    return this.belongsToMany('App/Models/Event', 'user_send_id', 'event_receiver_id')
      .pivotTable('partners')
      .withPivot(['id', 'status', 'message', 'partnership_agreement', 'shortage_id', 'message', 'maintenance_time_id'])
  }

  //PARCERIAS QUE O USUÁRIO RECEBEU SOLICITAÇÃO
  partnerReceivedFromEvent() {
    return this.belongsToMany('App/Models/Event', 'user_receiver_id', 'event_send_id')
      .pivotTable('partners')
      .withPivot(['id', 'status', 'message', 'partnership_agreement', 'shortage_id', 'message', 'maintenance_time_id'])
  }

  //PARCERIAS QUE O USUÁRIO ENVIOU SOLICITAÇÃO
  partnerSendToProject() {
    return this.belongsToMany('App/Models/Project', 'user_send_id', 'project_receiver_id')
      .pivotTable('partners')
      .withPivot(['id', 'status', 'message', 'partnership_agreement', 'shortage_id', 'message', 'maintenance_time_id'])
  }

  //PARCERIAS QUE O USUÁRIO RECEBEU SOLICITAÇÃO
  partnerReceivedFromProject() {
    return this.belongsToMany('App/Models/Project', 'user_receiver_id', 'project_send_id')
      .pivotTable('partners')
      .withPivot(['id', 'status', 'message', 'partnership_agreement', 'shortage_id', 'message', 'maintenance_time_id'])
  }

  //PARCERIAS QUE O USUÁRIO ENVIOU SOLICITAÇÃO
  partnerSendToOrganization() {
    return this.belongsToMany('App/Models/Organization', 'user_send_id', 'organization_receiver_id')
      .pivotTable('partners')
      .withPivot(['id', 'status', 'message', 'partnership_agreement', 'shortage_id', 'message', 'maintenance_time_id'])
  }

  //PARCERIAS QUE O USUÁRIO RECEBEU SOLICITAÇÃO
  partnerReceivedFromOrganization() {
    return this.belongsToMany('App/Models/Organization', 'user_receiver_id', 'organization_send_id')
      .pivotTable('partners')
      .withPivot(['id', 'status', 'message', 'partnership_agreement', 'shortage_id', 'message', 'maintenance_time_id'])
  }
  
  projectPartners() {
    return this.hasMany('App/Models/Partner', 'id', 'project_receiver_id')
  }

  organizationPartners() {
    return this.hasMany('App/Models/Partner', 'id', 'organization_receiver_id')
  }

  eventPartners() {
    return this.hasMany('App/Models/Partner', 'id', 'event_receiver_id')
  }
}

module.exports = User
