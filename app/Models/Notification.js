'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Notification extends Model {
    user() {
        return this.belongsTo('App/Models/User', 'user_send_id', 'id')
    }

    event() {
        return this.belongsTo('App/Models/Event', 'event_send_id', 'id')
    }

    project() {
        return this.belongsTo('App/Models/Project', 'project_send_id', 'id')
    }

    organization() {
        return this.belongsTo('App/Models/Organization', 'organization_send_id', 'id')
    }

    receivers() {
        return this.hasMany('App/Models/NotificationsReceiver', 'id', 'notification_id')
    }

    user_receiver() {
        return this.belongsToMany('App/Models/User', 'notification_id', 'user_receiver_id',)
            .pivotTable('notifications_receivers')
    }

    post() {
        return this.belongsTo('App/Models/Post')
    }
}

module.exports = Notification
