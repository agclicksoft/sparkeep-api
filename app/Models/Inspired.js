'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Inspired extends Model {
    user() {
        return this.belongsTo('App/Models/User')
    }

    userInspiration() {
        return this.belongsTo('App/Models/User', 'user_inspiration_id', 'id');
    }

    userInspired() {
        return this.belongsTo('App/Models/User', 'user_inspired_id', 'id');
    }

    organizationInspiration() {
        return this.belongsTo('App/Models/User', 'organization_inspiration_id', 'id');
    }

    organization() {
        return this.belongsTo('App/Models/Organization')
    }

    posts() {
        return this.belongsTo('App/Models/Post')
    }
}

module.exports = Inspired
