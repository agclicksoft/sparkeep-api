'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProjectsPassion extends Model {
    project() {
        return this.belongsTo('App/Models/Project')
    }

    passion() {
        return this.belongsTo('App/Models/Passion')
    }
}

module.exports = ProjectsPassion
