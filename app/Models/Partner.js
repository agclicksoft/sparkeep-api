'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Partner extends Model {
    user() {
        return this.belongsTo('App/Models/User')
    }

    project() {
        return this.belongsTo('App/Models/Project')
    }

    organization() {
        return this.belongsTo('App/Models/Organization')
    }

    event() {
        return this.belongsTo('App/Models/Event')
    }

    //!!!!!!!!!!!!!
    userSend() {
        return this.belongsTo('App/Models/User', 'user_send_id', 'id')
    }

    userReceiver() {
        return this.belongsTo('App/Models/User', 'user_receiver_id', 'id')
    }

    eventSend() {
        return this.belongsTo('App/Models/Event', 'event_send_id', 'id')
    }

    eventReceiver() {
        return this.belongsTo('App/Models/Event', 'event_receiver_id', 'id')
    }

    projectSend() {
        return this.belongsTo('App/Models/Project', 'project_send_id', 'id')
    }

    projectReceiver() {
        return this.belongsTo('App/Models/Project', 'project_receiver_id', 'id')
    }

    organizationSend() {
        return this.belongsTo('App/Models/Organization', 'organization_send_id', 'id')
    }

    organizationReceiver() {
        return this.belongsTo('App/Models/Organization', 'organization_receiver_id', 'id')
    }

    shortage() {
        return this.belongsTo('App/Models/Shortage', 'shortage_id', 'id')
    }

    favorites() {
        return this.belongsToMany('App/Models/User')
            .pivotTable('partner_favorites')
    }

    maintenance_time() {
        return this.hasOne('App/Models/MaintenanceTime', 'maintenance_time_id', 'id')
    }
}

module.exports = Partner
