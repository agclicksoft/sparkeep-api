'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProjectsShortage extends Model {
    project() {
        return this.belongsTo('App/Models/Project')
    }

    shortage() {
        return this.belongsTo('App/Models/Shortage')
    }
}

module.exports = ProjectsShortage
