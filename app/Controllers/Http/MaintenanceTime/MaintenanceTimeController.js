'use strict'

const MaintenanceTime = use('App/Models/MaintenanceTime')

class MaintenanceTimeController {
    async index() {
        try {
            const maintenanceTime = await MaintenanceTime.query()
                .fetch();

            return maintenanceTime
        }
        catch (error) {
            return error
        }
    }

    async show({params}) {
        try {
            const maintenanceTime = await MaintenanceTime.query()
            .where('id', params.id)
            .first()

            return maintenanceTime
        }
        catch (error) {
            return error
        }
    }

    async store({request, response}) {
        try {
            const body = request.only([
                'description'
            ])

            let maintenanceTime = await MaintenanceTime.query()
                .where('description', body.description)
                .first();

            if (maintenanceTime)
                return response.status(400).send({message: 'Tipo de manutenção já cadastrada.'})

            maintenanceTime = await MaintenanceTime.create(body);

            return maintenanceTime;

        }
        catch (error) {
            return error
        }
    }

    async update({request, params, response}) {
        try {

        }
        catch (error) {
            return error
        }
    }

    async delete() {
        try {

        }
        catch (error) {
            return error
        }
    }
}

module.exports = MaintenanceTimeController
