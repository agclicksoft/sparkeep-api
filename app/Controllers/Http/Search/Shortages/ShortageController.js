'use strict'

const User = use('App/Models/User');
const Event = use('App/Models/Event');
const Project = use('App/Models/Project');
const Organization = use('App/Models/Project');

class ShortageController {
    async index({request, response}) {
        try {
            const {
                eventPage = 1,
                projectPage = 1,
                eventPerpage = 8,
                projectPerpage = 8,
                shortages,
                keyword
            } = request.only([
                'eventPage',
                'projectPage',
                'eventPerpage',
                'projectPerpage',
                'shortages',
                'keyword'
            ]);
                
            
            const events = await Event.query()
                .where(function() {
                    for (let item of shortages) {
                        this.orWhereHas('shortages', builder => {
                            builder.where('shortage_id', item)
                        })
                    }

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                })
                .paginate(eventPage, eventPerpage);

            const projects = await Project.query()
                .where(function() {
                    for (let item of shortages) {
                        this.orWhereHas('shortages', builder => {
                            builder.where('shortage_id', item)
                        })
                    }

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                })
                .paginate(projectPage, projectPerpage);

            return {events, projects}
        }
        catch (error) {
            return error
        }
    }
}

module.exports = ShortageController;
