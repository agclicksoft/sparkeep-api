'use strict'

const User = use('App/Models/User');
const Event = use('App/Models/Event');
const Project = use('App/Models/Project');
const Organization = use('App/Models/Organization');

class ProjectController {
    async index({request, response}) {
        try {
            const {
                userPage = 1,
                userPerpage = 8,
                eventPage = 1,
                eventPerpage = 8,
                projectPage = 1,
                projectPerpage = 8,
                organizationPage = 1,
                organizationPerpage = 8,
                passions,
                keyword
            } = request.only([
                'userPage',
                'userPerpage',
                'eventPage',
                'projectPage',
                'eventPerpage',
                'projectPerpage',
                'organizationPage',
                'organizationPerpage',
                'passions',
                'keyword'
            ]);

            const users = await User.query()
                .where(function() {
                    for (let item of passions) {
                        this.orWhereHas('passions', builder => {
                            builder.where('passion_id', item)
                        })
                    }

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                })
                .paginate(userPage, userPerpage);
                
            
            const events = await Event.query()
                .where(function() {
                    for (let item of passions) {
                        this.orWhereHas('passions', builder => {
                            builder.where('passion_id', item)
                        })
                    }

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                })
                .paginate(eventPage, eventPerpage);

            const projects = await Project.query()
                .where(function() {
                    for (let item of passions) {
                        this.orWhereHas('passions', builder => {
                            builder.where('passion_id', item)
                        })
                    }

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                })
                .paginate(projectPage, projectPerpage);

            const organizations = await Organization.query()
                .where(function() {
                    for (let item of passions) {
                        this.orWhereHas('passions', builder => {
                            builder.where('passion_id', item)
                        })
                    }

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                })
                .paginate(organizationPage, organizationPerpage);

            return {users, events, projects, organizations}
        }
        catch (error) {
            return error
        }
    }
}

module.exports = ProjectController;
