'use strict'

const User = use('App/Models/User');
const Event = use('App/Models/Event');
const Project = use('App/Models/Project');
const Organization = use('App/Models/Organization');

class LocationController {
    async index({request, response}) {
        try {
            const {
                userPage = 1,
                userPerpage = 8,
                eventPage = 1,
                eventPerpage = 8,
                projectPage = 1,
                projectPerpage = 8,
                organizationPage = 1,
                organizationPerpage = 8,
                address_country,
                address_state,
                address_city,
                keyword
            } = request.only([
                'page',
                'perpage',
                'userPage',
                'userPerpage',
                'eventPage',
                'projectPage',
                'eventPerpage',
                'projectPerpage',
                'organizationPage',
                'organizationPerpage',
                'address_country',
                'address_state',
                'address_city',
                'keyword'
            ]);

            const users = await User.query()
                .where(function() {
                    if (address_country) this.where('address_country', address_country);

                    if (address_state) this.andWhere('address_state', address_state);

                    if (address_city) this.andWhere('address_city', address_city);

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                })
                .paginate(userPage, userPerpage);
                
            
            const events = await Event.query()
                .where(function() {
                    if (address_country) this.where('address_country', address_country);

                    if (address_state) this.andWhere('address_state', address_state);

                    if (address_city) this.andWhere('address_city', address_city);

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                })
                .paginate(eventPage, eventPerpage);

            const projects = await Project.query()
                .where(function() {
                    if (address_country) this.where('address_country', address_country);

                    if (address_state) this.andWhere('address_state', address_state);

                    if (address_city) this.andWhere('address_city', address_city);

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                })
                .paginate(projectPage, projectPerpage);

            const organizations = await Organization.query()
                .where(function() {
                    if (address_country) this.where('address_country', address_country);

                    if (address_state) this.andWhere('address_state', address_state);

                    if (address_city) this.andWhere('address_city', address_city);

                    if (keyword) this.andWhere('name', 'like', `%${keyword}%`);
                    
                })
                .paginate(organizationPage, organizationPerpage);

            return {users, events, projects, organizations};
        }
        catch (error) {
            return error
        }
    }
}

module.exports = LocationController;
