'use strict'

const User = use('App/Models/User');
const Inspired = use('App/Models/Inspired');
const InterestRegions = use('App/Models/UserInterestRegion');
const Notification = use('App/Models/Notification');
const Drive = use('Drive');

class UserController {
    async index({request}) {
        try {
            let {
                page = 1,
                perpage = 8,
                keyword,
                receive_administrator_proposals
            } = request.only([
                'page',
                'perpage',
                'keyword',
                'receive_administrator_proposals'
            ]);

            const users = await User.query()
                .where(function() {
                    if (keyword) this.where('name', 'LIKE', `%${keyword}%`)
                    if (receive_administrator_proposals == "true") this.andWhere('receive_administrator_proposals', true)
                })
                .whereNot('exc', true)
                .paginate(page, perpage);

            return users
        }
        catch (error) {
            return error
        }
    }

    async show({auth, params}) {
        try {
            const user = await User.query()
            .where('id', params.id)
            .whereNot('exc', true)
            .with('passions')
            .with('interest_regions')
            .withCount('inspireds as total_inspireds', builder => {
                builder.whereNotNull('user_inspired_id')
            })
            .withCount('inspirations as total_inspirations', builder => {
                builder.whereNotNull('user_inspiration_id')
                // builder.orWhereNotNull('organization_inspiration_id')
            })
            .withCount('eventPartners as total_event_partners', builder => {
                builder.where('status', '1')
            })
            .withCount('projectPartners as total_project_partners', builder => {
                builder.where('status', '1')
            })
            .withCount('organizationPartners as total_organization_partners', builder => {
                builder.where('status', '1')
            })
            .first()

            const userLogged = await auth.getUser();

            const inspired = await Inspired.query()
                .where('user_inspiration_id', user.id)
                .andWhere('user_inspired_id', userLogged.id)
                .first();

            inspired ? user.inspired = true : user.inspired = false;

            return user
        }
        catch (error) {
            return error
        }
    }

    async store() {
        try {

        }
        catch (error) {
            return error
        }
    }

    async update({request, params, response}) {
        try {
            const userBody = request.only([
                'user_id',
                'email',
                'name',
                'description',
                'image_url',
                'cellphone',
                'address_country',
                'address_state',
                'address_city',
                'address_neighborhood',
                'address_street',
                'address_complement',
                'address_number',
                'address_zip_code',
                'help_duration',
                'help_description',
                'accepted_terms',
                'receive_administrator_proposals',
                'telephone',
                'youtube',
                'facebook',
                'instagram',
                'twitter',
                'site'
            ])

            const interestRegionBody = request.only(['interest_regions'])['interest_regions'];

            const passionsBody = request.only(['passions'])['passions']

            const user = await User.find(params.id);

            let inspiredsIds = await Inspired.query()
                .where('user_inspiration_id', params.id)
                .pluck('user_inspired_id')


            inspiredsIds = inspiredsIds.map( id => {
                return { user_receiver_id: id }
            })

            if (!user) return response.status(404).send({message: "Usuário não encontrado"});

            await InterestRegions.query()
                .where('user_id', user.id)
                .delete();

            await user.interest_regions().createMany(interestRegionBody);

            await user.passions().detach();
            await user.passions().attach(passionsBody)
            await user.merge(userBody);
            await user.save();

            await user.loadMany(['passions', 'interest_regions']);

            const notification = await Notification.create({
                user_send_id: params.id,
                description: `O usuário ${user.name} fez alterações em seu perfil.`,
                type: 'message'
            })

            await notification.receivers().createMany(inspiredsIds);

            return user
        }
        catch (error) {
            return error.message
        }
    }

    async destroy({auth, params, response}) {
        try {
            const user = await auth.getUser();

            if (user.id != params.id) return response.status(400).send({ message: "Você não tem permissão para excluir este usuário" });

            await user.merge({
                email: null,
                name: null,
                description: null,
                image_url: null,
                cellphone: null,
                address_country: null,
                address_state: null,
                address_city: null,
                address_neighborhood: null,
                address_street: null,
                address_complement: null,
                address_number: null,
                address_zip_code: null,
                help_duration: null,
                help_description: null,
                accepted_terms: null,
                receive_administrator_proposals: null,
                telephone: null,
                youtube: null,
                facebook: null,
                instagram: null,
                twitter: null,
                site: null,
                exc: true
            })
            await user.save();

            return user
        }
        catch (error) {
            return error
        }
    }

    async uploadAttachment({request, response, params, auth}) {

        try {
            const user = await auth.getUser();

            const validationOptions = {
                types: ['jpeg', 'jpg', 'png'],
                size: '5mb'
            }

            request.multipart.file('attachment', validationOptions, async file => {
                // Set file size from stream byteCount, so adonis can validate file size
                file.size = file.stream.byteCount
                // Catches validation errors, if any and then throw exception
                const error = file.error()

                if (error.message) throw new Error(error.message)

                const Key =`profile_images/${(Math.random() * 100).toString()}-${file.clientName}`
                const ContentType = file.headers['content-type']
                const ACL = 'public-read'
                // upload file to s3
                const path = await Drive.put(Key, file.stream, { ContentType, ACL })

                await user.merge({image_url: path});
                await user.save();
            })

            await request.multipart.process()

            return user
        }
        catch(error) {
            return error
        }
    }
}

module.exports = UserController
