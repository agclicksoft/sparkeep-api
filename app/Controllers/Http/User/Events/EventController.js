'use strict'

const User = use('App/Models/User');
const Event = use('App/Models/Event');
const EventAdministrators = use('App/Models/EventsAdministrator');

class EventController {
    async index({request, params, response, auth}) {
        try {
            // const user = await auth.getUser();

            const ownerEvents = await Event.query()
                .whereNot('exc', true)
                .where(function() {
                    this.whereHas('user', builder => builder.where('id', params.id))
                    this.orWhereHas('organization', builder => builder.where('user_id', params.id))
                    this.orWhereHas('project', builder => builder.where('user_id', params.id))
                })
                .with('user')
                .with('organization')
                .with('project')
                .fetch()

                // const myOrganizationsIds = user.$relations.organizations.rows.map( el => el.id)

                // const myProjectsIds = user.$relations.projects.rows.map( el => el.id)

                let administratorsEvents = await Event.query()
                    .whereHas('administrators', builder => {
                        builder.where('user_id', params.id)
                        builder.where('status', 'accepted')
                    })
                    //TODO: SE FOR PARA EXIBIR OS EVENTOS QUE UM PROJETO/ORGANIZAÇÃO
                    //QUE EU SOU PROPIETÁRIO, OU ADMINISTRO
                    .fetch()

                return {ownerEvents, administratorsEvents};
        }
        catch (error) {
            return error.message
        }
    }
}

module.exports = EventController
