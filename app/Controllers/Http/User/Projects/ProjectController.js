"use strict";

const User = use("App/Models/User");
const Project = use("App/Models/Project");

class ProjectController {
  async index({ params }) {
    try {
      const user = await User.find(params.id);

      const ownerProjects = await Project.query()
        .whereNot("exc", true)
        .andWhere(function () {
          this.where("user_id", user.id);
          this.orWhereIn("organization_id", function() {
            this.select("id").from("organizations").where("user_id", user.id);
          });
        })
        .with("passions")
        .with("shortages")
        .fetch();

      const administratorsProjects = await Project.query()
        .whereHas("administrators", (builder) => {
          builder.where("user_id", user.id);
          builder.andWhere("status", "accepted");
        })
        .with("passions")
        .with("shortages")
        /**
         * @todo: SE FOR PARA EXIBIR OS PROJETOS DE UMA ORGANIZAÇÃO QUE EU SOU PROPIETÁRIO
         */
        .fetch();

      return { ownerProjects, administratorsProjects };
    } catch (error) {
      return error;
    }
  }
}

module.exports = ProjectController;
