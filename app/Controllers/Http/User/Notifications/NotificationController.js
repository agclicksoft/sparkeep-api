'use strict'

const Event = use('App/Models/Event');
const Project = use('App/Models/Project');
const Organization = use('App/Models/Organization');
const Partner = use('App/Models/Partner');


const NotificationReceiver = use('App/Models/NotificationsReceiver')

class NotificationController {
    async index({request, response, params}) {

        const {page = 1, perpage = 8} = request.only(['page', 'perpage'])

        const notifications = await NotificationReceiver.query()
            .where('user_receiver_id', params.id)
            .with('notification', builder => {
                builder.select(
                    'id',
                    'description',
                    'post_id',
                    'user_send_id',
                    'event_send_id',
                    'project_send_id',
                    'organization_send_id',
                    'type'
                )
                builder.with('user', builder => {
                    builder.select('')
                })
                builder.with('event')
                builder.with('project')
                builder.with('organization')
            })
            .orderBy('id', 'DESC')
            .paginate(page, perpage);

            for (let item of notifications.rows) {
                for (let property in item.$relations.notification.$relations) {
                    if (!item.$relations.notification.$relations[property]) {
                        delete item.$relations.notification.$relations[property]
                    }
                    else {
                        item.$relations.notification.name_send = item.$relations.notification.$relations[property].name
                        item.$relations.notification.image_send = item.$relations.notification.$relations[property].image_url
                        delete item.$relations.notification.$relations[property]
                    }
                }
            }

        return notifications
    }

    async view({request, response, params}) {
        const notifications = await NotificationReceiver.query()
        .where('user_receiver_id', params.id)
        .andWhere('viewed', false)
        .fetch();

        for (let item of notifications.rows) {
            item.viewed = true;
            await item.save();
        }

        return notifications;
    }

    async notViewed({params}) {
        const notifications = await NotificationReceiver.query()
            .where('user_receiver_id', params.id)
            .andWhere('viewed', false)
            .last();

        if (notifications) return {show_badge: true};

        return {show_badge: false};
    }

    async chatBadge({ params, response }) {

        const myEvents = await Event.query()
            .where('user_id', params.id)
            .orWhereHas('administrators', builder => builder.where('user_id', params.id))
            .whereNot('exc', true)
            .pluck('id');

        const myProjects = await Project.query()
            .where('user_id', params.id)
            .orWhereHas('administrators', builder => builder.where('user_id', params.id))
            .whereNot('exc', true)
            .pluck('id');

        const myOrganizations = await Organization.query()
            .where('user_id', params.id)
            .orWhereHas('administrators', builder => builder.where('user_id', params.id))
            .whereNot('exc', true)
            .pluck('id');

        let partner = await Partner.query()
            .whereIn('event_receiver_id', myEvents)
            .where('viewed', false)
            .first()

        if (partner) return response.status(200).send({ show_badge: true })

        partner = await Partner.query()
            .whereIn('project_receiver_id', myProjects)
            .where('viewed', false)
            .first()

        if (partner) return response.status(200).send({ show_badge: true })

        partner = await Partner.query()
            .whereIn('organization_receiver_id', myOrganizations)
            .where('viewed', false)
            .first()

        if (partner) return response.status(200).send({ show_badge: true })

        return response.status(200).send({ show_badge: false })
    }

    async viewBadge({response, params}) {

        const myEvents = await Event.query()
            .where('user_id', params.id)
            .orWhereHas('administrators', builder => builder.where('user_id', params.id))
            .whereNot('exc', true)
            .pluck('id');

        const myProjects = await Project.query()
            .where('user_id', params.id)
            .orWhereHas('administrators', builder => builder.where('user_id', params.id))
            .whereNot('exc', true)
            .pluck('id');

        const myOrganizations = await Organization.query()
            .where('user_id', params.id)
            .orWhereHas('administrators', builder => builder.where('user_id', params.id))
            .whereNot('exc', true)
            .pluck('id');

        await Partner.query()
            .whereIn('event_receiver_id', myEvents)
            .where('viewed', false)
            .update({viewed: true})

        await Partner.query()
            .whereIn('project_receiver_id', myProjects)
            .where('viewed', false)
            .update({viewed: true})

        await Partner.query()
            .whereIn('organization_receiver_id', myOrganizations)
            .where('viewed', false)
            .update({viewed: true})

        return response.status(204).send();

    }
}

module.exports = NotificationController;