'use strict'

const User = use('App/Models/User');
const EventAdministrator = use('App/Models/EventsAdministrator')
const ProjectAdministrator = use('App/Models/ProjectsAdministrator')
const OrganizationAdministrator = use('App/Models/OrganizationsAdministrator')
const Notification = use('App/Models/Notification')

const moment = require('moment');

class AdministratorController {

    async index({ auth, request }) {
        const user = await auth.getUser();

        const {
            eventPage,
            eventPerpage,
            projectPage,
            projectPerpage,
            organizationPage,
            organizationPerpage
        } = request.only([
            'eventPage',
            'eventPerpage',
            'projectPage',
            'projectPerpage',
            'organizationPage',
            'organizationPerpage'
        ])

        const eventProposals = await EventAdministrator.query()
            .where('user_id', user.id)
            .andWhere('status', 'pending')
            .with('event', builder => builder.select('id', 'name', 'image_url'))
            .paginate(eventPage, eventPerpage)


        const projectProposals = await ProjectAdministrator.query()
            .where('user_id', user.id)
            .andWhere('status', 'pending')
            .with('project', builder => builder.select('id', 'name', 'image_url'))
            .paginate(projectPage, projectPerpage)


        const organizationProposals = await OrganizationAdministrator.query()
            .where('user_id', user.id)
            .andWhere('status', 'pending')
            .with('organization', builder => builder.select('id', 'name', 'image_url'))
            .paginate(organizationPage, organizationPerpage)

        return {eventProposals, projectProposals, organizationProposals}
    }

    async update({ request, response, auth, params}) {
        const role = params.role;
        const user_id = params.user_id;
        const id = params.id;

        const { notification_id } = request.only(['notification_id']);

        const { action } = request.only(['action'])

        var result = null;

        switch(role) {
            case 'event':
                result = await EventAdministrator.query()
                    .where('user_id', user_id)
                    .andWhere('event_id', id)
                    .whereDoesntHave('user', builder => {
                        builder.where('exc', true)
                    })
                    .last();
            break
            case 'project':
                result = await ProjectAdministrator.query()
                    .where('user_id', user_id)
                    .andWhere('project_id', id)
                    .whereDoesntHave('user', builder => {
                        builder.where('exc', true)
                    })
                    .last();
            break
            case 'organization':
                result = await OrganizationAdministrator.query()
                    .where('user_id', user_id)
                    .andWhere('organization_id', id)
                    .whereDoesntHave('user', builder => {
                        builder.where('exc', true)
                    })
                    .last();
            break
        }

        console.log('aqui')
        console.log(result);

        if (notification_id) {
          const notification = await Notification.find(notification_id);

          if (notification.type == 'request') {
              await notification.merge({ type: 'message' });
              await notification.save();
          }
        }

        if (!result) return response.status(400).send({ message: "Você não tem permissão para aceitar/recusar esta solicitação" })

        await result.merge({status: action});
        await result.save();

        return result
    }
}

module.exports = AdministratorController;
