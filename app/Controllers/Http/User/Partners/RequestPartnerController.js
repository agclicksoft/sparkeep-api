"use strict";

const Partner = use("App/Models/Partner");
const User = use("App/Models/User");
const Event = use("App/Models/Event");
const Project = use("App/Models/Project");
const Organization = use("App/Models/Organization");

class PartnerController {
  async index({ request, auth, params }) {
    try {
      const {
        userPage = 1,
        userPerpage = 8,
        eventPage = 1,
        eventPerpage = 8,
        projectPage = 1,
        projectPerpage = 8,
        organizationPage = 1,
        organizationPerpage = 8,
        keyword,
        status,
      } = request.only([
        "userPage",
        "userPerpage",
        "eventPage",
        "eventPerpage",
        "projectPage",
        "projectPerpage",
        "organizationPage",
        "organizationPerpage",
        "keyword",
        "status",
      ]);

      let myEvents = await Event.query()
        .where("user_id", params.id)
        .whereNotNull("name")
        .orWhereHas("administrators", (builder) =>
          builder.where("user_id", params.id)
        )
        .fetch();
      if (myEvents.rows.length == 0) {
        myEvents = { rows: [{ id: 0 }] };
      }

      let myProjects = await Project.query()
        .where("user_id", params.id)
        .whereNotNull("name")
        .orWhereHas("administrators", (builder) =>
          builder.where("user_id", params.id)
        )
        .fetch();
      if (myProjects.rows.length == 0) {
        myProjects = { rows: [{ id: 0 }] };
      }

      let myOrganizations = await Organization.query()
        .where("user_id", params.id)
        .orWhereHas("administrators", (builder) =>
          builder.where("user_id", params.id)
        )
        .fetch();
      if (myOrganizations.rows.length == 0) {
        myOrganizations = { rows: [{ id: 0 }] };
      }

      const usersPartners = await User.query()
        .select("id", "name")
        .where(function () {
          if (keyword) this.where("name", "like", `%${keyword}%`);
        })
        .andWhere(function () {
          this.whereHas("partnerSendToEvent", (builder) => {
            builder.where(function () {
              myEvents.rows.map((event) => {
                this.orWhere("event_receiver_id", event.id);
              });
            });
            builder.andWhere(function () {
              this.whereNot("user_send_id", params.id);
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToProject", (builder) => {
            builder.where(function () {
              myProjects.rows.map((project) => {
                this.orWhere("project_receiver_id", project.id);
              });
            });
            builder.andWhere(function () {
              this.whereNot("user_send_id", params.id);
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToOrganization", (builder) => {
            builder.where(function () {
              myOrganizations.rows.map((organization) => {
                this.orWhere("organization_receiver_id", organization.id);
              });
            });
            builder.andWhere(function () {
              this.whereNot("user_send_id", params.id);
            });

            builder.wherePivot("status", status);
          });
        })
        .with("partnerSendToUser", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToEvent", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToProject", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToOrganization", (builder) =>
          builder.wherePivot("status", status)
        )
        .paginate(userPage, userPerpage);

      for (let userPartner of usersPartners.rows) {
        userPartner.myProfile = [];
        for (let property in userPartner.$relations) {
          for (let profile of userPartner.$relations[property].rows) {
            userPartner.myProfile.push(profile);
          }
          delete userPartner.$relations[property];
        }
      }

      const eventPartners = await Event.query()
        .select("id", "name")
        .where(function () {
          if (keyword) this.where("name", "like", `%${keyword}%`);
        })
        .andWhere(function () {
          this.whereHas("partnerSendToUser", (builder) => {
            builder.where("user_receiver_id", params.id);
            builder.andWhere(function () {
              myEvents.rows.map((event) => {
                this.andWhereNot("event_send_id", event.id);
              });
            });
            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToEvent", (builder) => {

            builder.where(function () {
              myEvents.rows.map((event) =>
                this.orWhere("event_receiver_id", event.id)
              );
            });
            builder.andWhere(function () {
              myEvents.rows.map((event) => {
                this.andWhereNot("event_send_id", event.id);
              });
            });
            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToProject", (builder) => {
            builder.where(function () {
              myProjects.rows.map((project) =>
                this.orWhere("project_receiver_id", project.id)
              );
            });
            builder.andWhere(function () {
              myEvents.rows.map((event) => {
                this.andWhereNot("event_send_id", event.id);
              });
            });
            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToOrganization", (builder) => {
            builder.where(function () {
              myOrganizations.rows.map((organization) =>
                this.orWhere("organization_receiver_id", organization.id)
              );
            });
            builder.andWhere(function () {
              myEvents.rows.map((event) => {
                this.andWhereNot("event_send_id", event.id);
              });
            });
            builder.wherePivot("status", status);
          });
        })
        .with("partnerSendToUser", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToEvent", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToProject", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToOrganization", (builder) =>
          builder.wherePivot("status", status)
        )
        .paginate(eventPage, eventPerpage);

      for (let eventPartner of eventPartners.rows) {
        eventPartner.myProfile = [];
        for (let property in eventPartner.$relations) {
          for (let profile of eventPartner.$relations[property].rows) {
            eventPartner.myProfile.push(profile);
          }
          delete eventPartner.$relations[property];
        }
      }

      const projectPartners = await Project.query()
        .select("id", "name")

        .where(function () {
          if (keyword) this.where("name", "like", `%${keyword}%`);
        })
        .andWhere(function () {
          this.whereHas("partnerSendToUser", (builder) => {
            builder.where("user_receiver_id", params.id);
            builder.andWhere(function () {
              myProjects.rows.map((project) => {
                this.andWhereNot("project_send_id", project.id);
              });
            });
            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToEvent", (builder) => {
            builder.where(function () {
              myEvents.rows.map((event) =>
                this.orWhere("event_receiver_id", event.id)
              );
            });
            builder.andWhere(function () {
              myProjects.rows.map((project) => {
                this.andWhereNot("project_send_id", project.id);
              });
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToProject", (builder) => {
            builder.where(function () {
              myProjects.rows.map((project) =>
                this.orWhere("project_receiver_id", project.id)
              );
            });
            builder.andWhere(function () {
              myProjects.rows.map((project) => {
                this.andWhereNot("project_send_id", project.id);
              });
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToOrganization", (builder) => {
            builder.where(function () {
              myOrganizations.rows.map((organization) =>
                this.orWhere("organization_receiver_id", organization.id)
              );
            });
            builder.andWhere(function () {
              myProjects.rows.map((project) => {
                this.andWhereNot("project_send_id", project.id);
              });
            });

            builder.wherePivot("status", status);
          });
        })
        .with("partnerSendToUser", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToEvent", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToProject", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToOrganization", (builder) =>
          builder.wherePivot("status", status)
        )
        .paginate(projectPage, projectPerpage);

      for (let projectPartner of projectPartners.rows) {
        projectPartner.myProfile = [];
        for (let property in projectPartner.$relations) {
          for (let profile of projectPartner.$relations[property].rows) {
            projectPartner.myProfile.push(profile);
          }
          delete projectPartner.$relations[property];
        }
      }

      const organizationPartners = await Organization.query()
        .select("id", "name")
        .where(function () {
          if (keyword) this.where("name", "like", `%${keyword}%`);
        })
        .andWhere(function () {
          this.whereHas("partnerSendToUser", (builder) => {
            builder.where("user_receiver_id", params.id);
            builder.andWhere(function () {
              myOrganizations.rows.map((organization) => {
                this.andWhereNot("organization_send_id", organization.id);
              });
            });

            builder.wherePivot("status", status);
          });

          this.orWhereHas("partnerSendToEvent", (builder) => {
            builder.where(function () {
              myEvents.rows.map((event) =>
                this.orWhere("event_receiver_id", event.id)
              );
            });
            builder.andWhere(function () {
              myOrganizations.rows.map((organization) => {
                this.andWhereNot("organization_send_id", organization.id);
              });
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToProject", (builder) => {
            builder.where(function () {
              myProjects.rows.map((project) =>
                this.orWhere("project_receiver_id", project.id)
              );
            });
            builder.andWhere(function () {
              myOrganizations.rows.map((organization) => {
                this.andWhereNot("organization_send_id", organization.id);
              });
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerSendToOrganization", (builder) => {
            builder.where(function () {
              myOrganizations.rows.map((organization) =>
                this.orWhere("organization_receiver_id", organization.id)
              );
            });
            builder.andWhere(function () {
              myOrganizations.rows.map((organization) => {
                this.andWhereNot("organization_send_id", organization.id);
              });
            });

            builder.wherePivot("status", status);
          });
        })
        .with("partnerSendToUser", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToEvent", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToProject", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerSendToOrganization", (builder) =>
          builder.wherePivot("status", status)
        )
        .paginate(organizationPage, organizationPerpage);

      for (let organizationPartner of organizationPartners.rows) {
        organizationPartner.myProfile = [];
        for (let property in organizationPartner.$relations) {
          for (let profile of organizationPartner.$relations[property].rows) {
            organizationPartner.myProfile.push(profile);
          }
          delete organizationPartner.$relations[property];
        }
      }

      //----------------------------------------------//

      const usersReceivedPartners = await User.query()
        .where(function () {
          if (keyword) this.where("name", "like", `%${keyword}%`);
        })
        .andWhere(function () {
          this.whereHas("partnerReceivedFromEvent", (builder) => {
            builder.where(function () {
              myEvents.rows.map((event) => {
                this.orWhere("event_send_id", event.id);
              });
            });
            builder.andWhere(function () {
              this.whereNot("user_receiver_id", params.id);
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromProject", (builder) => {
            builder.where(function () {
              myProjects.rows.map((project) => {
                this.orWhere("project_send_id", project.id);
              });
            });
            builder.andWhere(function () {
              this.whereNot("user_receiver_id", params.id);
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromOrganization", (builder) => {
            builder.where(function () {
              myOrganizations.rows.map((organization) => {
                this.orWhere("organization_send_id", organization.id);
              });
            });
            builder.andWhere(function () {
              this.whereNot("user_receiver_id", params.id);
            });

            builder.wherePivot("status", status);
          });
        })
        .with("partnerReceivedFromUser", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerReceivedFromEvent", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerReceivedFromProject", (builder) =>
          builder.wherePivot("status", status)
        )
        .with("partnerReceivedFromOrganization", (builder) =>
          builder.wherePivot("status", status)
        )
        .paginate(userPage, userPerpage);

      for (let userReceivedPartner of usersReceivedPartners.rows) {
        userReceivedPartner.myProfile = [];
        for (let property in userReceivedPartner.$relations) {
          for (let profile of userReceivedPartner.$relations[property].rows) {
            userReceivedPartner.myProfile.push(profile);
          }
          delete userReceivedPartner.$relations[property];
        }
      }

      const eventReceivedPartners = await Event.query()
        .select("id", "user_id", "project_id", "organization_id", "name")
        .where(function () {
          if (keyword) this.where("name", "like", `%${keyword}%`);
        })
        .andWhere(function () {
          this.whereHas("partnerReceivedFromUser", (builder) => {
            builder.where("user_send_id", params.id);
            builder.andWhere(function () {
              myEvents.rows.map((event) => {
                this.andWhereNot("event_receiver_id", event.id);
              });
            });
            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromEvent", (builder) => {
            builder.where(function () {
              myEvents.rows.map((event) =>
                this.orWhere("event_send_id", event.id)
              );
            });
            builder.andWhere(function () {
              myEvents.rows.map((event) => {
                this.andWhereNot("event_receiver_id", event.id);
              });
            });
            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromProject", (builder) => {
            builder.where(function () {
              myProjects.rows.map((project) =>
                this.orWhere("project_send_id", project.id)
              );
            });
            builder.andWhere(function () {
              myEvents.rows.map((event) => {
                this.andWhereNot("event_receiver_id", event.id);
              });
            });
            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromOrganization", (builder) => {
            builder.where(function () {
              myOrganizations.rows.map((organization) =>
                this.orWhere("organization_send_id", organization.id)
              );
            });
            builder.andWhere(function () {
              myEvents.rows.map((event) => {
                this.andWhereNot("event_receiver_id", event.id);
              });
            });
            // if(status) builder.wherePivot('status', '0')
          });
        })
        .with("partnerReceivedFromUser", (builder) => {
          builder.where("users.id", params.id);
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .with("partnerReceivedFromEvent", (builder) => {
          builder.where(function () {
            for (let item of myEvents.rows) {
              this.orWhere("events.id", item.id);
            }
          });
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .with("partnerReceivedFromProject", (builder) => {
          builder.where(function () {
            for (let item of myProjects.rows) {
              this.orWhere("projects.id", item.id);
            }
          });
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .with("partnerReceivedFromOrganization", (builder) => {
          builder.where(function () {
            for (let item of myOrganizations.rows) {
              this.orWhere("organizations.id", item.id);
            }
          });
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .paginate(eventPage, eventPerpage);

      for (let eventReceivedPartner of eventReceivedPartners.rows) {
        eventReceivedPartner.myProfile = [];
        for (let property in eventReceivedPartner.$relations) {
          for (let profile of eventReceivedPartner.$relations[property].rows) {
            eventReceivedPartner.myProfile.push(profile);
          }
          delete eventReceivedPartner.$relations[property];
        }
      }

      const projectReceivedPartners = await Project.query()
        .where(function () {
          if (keyword) this.where("name", "like", `%${keyword}%`);
        })
        .andWhere(function () {
          this.whereHas("partnerReceivedFromUser", (builder) => {
            builder.where("users.id", params.id);
            builder.select("id", "email", "name");
            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromEvent", (builder) => {
            builder.where(function () {
              myEvents.rows.map((event) =>
                this.orWhere("event_send_id", event.id)
              );
            });
            builder.andWhere(function () {
              myProjects.rows.map((project) => {
                this.andWhereNot("project_receiver_id", project.id);
              });
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromProject", (builder) => {
            builder.where(function () {
              myProjects.rows.map((project) =>
                this.orWhere("project_send_id", project.id)
              );
            });
            builder.andWhere(function () {
              myProjects.rows.map((project) => {
                this.andWhereNot("project_receiver_id", project.id);
              });
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromOrganization", (builder) => {
            builder.where(function () {
              myOrganizations.rows.map((organization) =>
                this.orWhere("organization_send_id", organization.id)
              );
            });
            builder.andWhere(function () {
              myProjects.rows.map((project) => {
                this.andWhereNot("project_receiver_id", project.id);
              });
            });

            builder.wherePivot("status", status);
          });
        })
        .with("partnerReceivedFromUser", (builder) => {
          builder.where("users.id", params.id);
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .with("partnerReceivedFromEvent", (builder) => {
          builder.where(function () {
            for (let item of myEvents.rows) {
              this.orWhere("events.id", item.id);
            }
          });
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .with("partnerReceivedFromProject", (builder) => {
          builder.where(function () {
            for (let item of myProjects.rows) {
              this.orWhere("projects.id", item.id);
            }
          });
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .with("partnerReceivedFromOrganization", (builder) => {
          builder.where(function () {
            for (let item of myOrganizations.rows) {
              this.orWhere("organizations.id", item.id);
            }
          });
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .paginate(projectPage, projectPerpage);

      for (let projectReceivedPartner of projectReceivedPartners.rows) {
        projectReceivedPartner.myProfile = [];
        for (let property in projectReceivedPartner.$relations) {
          for (let profile of projectReceivedPartner.$relations[property]
            .rows) {
            projectReceivedPartner.myProfile.push(profile);
          }
          delete projectReceivedPartner.$relations[property];
        }
      }

      const organizationReceivedPartners = await Organization.query()
        .where(function () {
          if (keyword) this.where("name", "like", `%${keyword}%`);
        })
        .andWhere(function () {
          this.whereHas("partnerReceivedFromUser", (builder) => {
            builder.where("user_send_id", params.id);
            builder.andWhere(function () {
              myOrganizations.rows.map((organization) => {
                this.andWhereNot("organization_receiver_id", organization.id);
              });
            });

            builder.wherePivot("status", status);
          });

          this.orWhereHas("partnerReceivedFromEvent", (builder) => {
            builder.where(function () {
              myEvents.rows.map((event) =>
                this.orWhere("event_send_id", event.id)
              );
            });
            builder.andWhere(function () {
              myOrganizations.rows.map((organization) => {
                this.andWhereNot("organization_receiver_id", organization.id);
              });
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromProject", (builder) => {
            builder.where(function () {
              myProjects.rows.map((project) =>
                this.orWhere("project_send_id", project.id)
              );
            });
            builder.andWhere(function () {
              myOrganizations.rows.map((organization) => {
                this.andWhereNot("organization_receiver_id", organization.id);
              });
            });

            builder.wherePivot("status", status);
          });
          this.orWhereHas("partnerReceivedFromOrganization", (builder) => {
            builder.where(function () {
              myOrganizations.rows.map((organization) =>
                this.orWhere("organization_send_id", organization.id)
              );
            });
            builder.andWhere(function () {
              myOrganizations.rows.map((organization) => {
                this.andWhereNot("organization_receiver_id", organization.id);
              });
            });

            builder.wherePivot("status", status);
          });
        })
        .with("partnerReceivedFromUser", (builder) => {
          builder.where("users.id", params.id);
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .with("partnerReceivedFromEvent", (builder) => {
          builder.where(function () {
            for (let item of myEvents.rows) {
              this.orWhere("events.id", item.id);
            }
          });
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .with("partnerReceivedFromProject", (builder) => {
          builder.where(function () {
            for (let item of myProjects.rows) {
              this.orWhere("projects.id", item.id);
            }
          });
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .with("partnerReceivedFromOrganization", (builder) => {
          builder.where(function () {
            for (let item of myOrganizations.rows) {
              this.orWhere("organizations.id", item.id);
            }
          });
          builder.select("id", "email", "name");
          builder.wherePivot("status", status);
        })
        .paginate(organizationPage, organizationPerpage);

      for (let organizationReceivedPartner of organizationReceivedPartners.rows) {
        organizationReceivedPartner.myProfile = [];
        for (let property in organizationReceivedPartner.$relations) {
          for (let profile of organizationReceivedPartner.$relations[property]
            .rows) {
            organizationReceivedPartner.myProfile.push(profile);
          }
          delete organizationReceivedPartner.$relations[property];
        }
      }

      return {
        send: {
          usersPartners,
          eventPartners,
          projectPartners,
          organizationPartners,
        },
        received: {
          usersPartners: usersReceivedPartners,
          eventPartners: eventReceivedPartners,
          projectPartners: projectReceivedPartners,
          organizationPartners: organizationReceivedPartners,
        },
      };
    } catch (error) {
      return error.message;
    }
  }
}

module.exports = PartnerController;
