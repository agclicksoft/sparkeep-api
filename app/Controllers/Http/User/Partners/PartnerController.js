'use strict'

const Partner = use('App/Models/Partner');
const User = use('App/Models/User');
const Event = use('App/Models/Event');
const EventAdministrators = use('App/Models/EventsAdministrator');
const Project = use('App/Models/Project');
const ProjectAdministrators = use('App/Models/ProjectsAdministrator');
const Organization = use('App/Models/Organization');
const OrganizationAdministrators = use('App/Models/OrganizationsAdministrator');
const Shortage = use('App/Models/Shortage');

class PartnerController {
    async index({request, auth, params}) {
        try {
            const {
                userPage = 1,
                userPerpage = 8,
                userKeyword,
                eventPage = 1,
                eventPerpage = 8,
                eventKeyword,
                projectPage = 1,
                projectPerpage = 8,
                projectKeyword,
                organizationPage = 1,
                organizationPerpage = 8,
                organizationKeyword,
                status
            } = request.only([
                'userPage',
                'userPerpage',
                'userKeyword',
                'eventPage',
                'eventPerpage',
                'eventKeyword',
                'projectPage',
                'projectPerpage',
                'projectKeyword',
                'organizationPage',
                'organizationPerpage',
                'organizationKeyword',
                'status'
            ]);

            const user = await User.find(params.id);

            const myEvents = await Event.query()
                .where('user_id', user.id)
                .orWhereHas('administrators', builder => builder.where('user_id', user.id))
                .whereNot('exc', true)
                .fetch();

            const myProjects = await Project.query()
                .where('user_id', user.id)
                .orWhereHas('administrators', builder => builder.where('user_id', user.id))
                .whereNot('exc', true)
                .fetch();

            const myOrganizations = await Organization.query()
                .where('user_id', user.id)
                .orWhereHas('administrators', builder => builder.where('user_id', user.id))
                .whereNot('exc', true)
                .fetch();

            const userPartners = await Partner.query()
                .where(function() {
                    if (myEvents.rows.length > 0 || myProjects.rows.length > 0 || myOrganizations.rows.length > 0) {
                        myEvents.rows.map( event => {
                            // this.orWhere('event_send_id', event.id )
                            this.orWhere('event_receiver_id', event.id )
                        } );
                        myProjects.rows.map( project => {
                            // this.orWhere('project_send_id', project.id )
                            this.orWhere('project_receiver_id', project.id )
                        } );
                        myOrganizations.rows.map( organization => {
                            // this.orWhere('organization_send_id', organization.id )
                            this.orWhere('organization_receiver_id', organization.id )
                        } )
                    } else {
                        this.where('user_send_id', 0)
                    }
                })
                .andWhere(function() {
                    this.whereNotNull('user_send_id')
                    this.orWhereNotNull('user_receiver_id')
                })
                .andWhere('status', '1')
                .andWhere(function() {
                    this.whereDoesntHave('userSend', builder => {
                        builder.where('exc', 'true')
                    })
                    this.orWhereDoesntHave('userReceiver', builder => {
                        builder.where('exc', 'true')
                    })
                })
                .with('favorites', builder => {
                    builder.select('id'),
                    builder.where('users.id', user.id)
                })
                .with('userSend', builder => builder.select('id', 'name', 'image_url', 'description'))
                .with('userReceiver', builder => builder.select('id', 'name', 'image_url', 'description'))
                .with('maintenance_time')
                .paginate(userPage, userPerpage);

            for (let userPartner of userPartners.rows) {
                for (let property in userPartner) {
                    for (let prop in userPartner[property]) {
                        if (!userPartner[property][prop]) {
                            delete userPartner[property][prop]
                        }
                    }
                }
                for (let property in userPartner.$relations) {
                    if(userPartner.$relations[property]) {
                        userPartner.user = userPartner.$relations[property]
                    }
                    if (property != 'favorites') {
                        delete userPartner.$relations[property]
                    }
                }

                if (userPartner.$relations.favorites.rows.length > 0) {
                    userPartner.favorite = true;
                    delete userPartner.$relations.favorites
                }
                else {
                    userPartner.favorite = false;
                    delete userPartner.$relations.favorites
                }
            };

            const eventPartners = await Partner.query()
                .where(function() {
                    this.where(function() {
                        this.where(function() {
                            this.where('user_send_id', user.id)
                            this.orWhere('user_receiver_id', user.id)
                        })
                        this.andWhere(function() {
                            this.whereNotNull('event_send_id')
                            this.orWhereNotNull('event_receiver_id')
                        })
                    })
                    this.orWhere(function() {
                      if (myEvents.rows.length > 0) {
                        this.where(function() {
                                myEvents.rows.map( event => {
                                    this.orWhere('event_send_id', event.id )
                                    this.orWhere('event_receiver_id', event.id )
                                });
                        })
                        this.andWhere(function() {
                                myEvents.rows.map( event => {
                                    this.whereNot('event_send_id', event.id )
                                    this.orWhereNot('event_receiver_id', event.id )
                                });
                        })
                        this.andWhere(function() {
                          this.whereNotNull('event_send_id')
                          this.whereNotNull('event_receiver_id')
                        })
                      }
                    })
                    this.orWhere(function() {
                      if (myProjects.rows.length > 0 || myOrganizations.rows.length > 0) {
                        this.where(function() {
                          myProjects.rows.map( project => {
                              this.orWhere('project_send_id', project.id )
                              this.orWhere('project_receiver_id', project.id )
                          });
                          myOrganizations.rows.map( organization => {
                              this.orWhere('organization_send_id', organization.id )
                              this.orWhere('organization_receiver_id', organization.id )
                          });

                        })
                        this.andWhere(function() {
                          this.whereNotNull('event_send_id')
                          this.orWhereNotNull('event_receiver_id')
                        })
                      }
                    })
                })
                .andWhere('status', '1')
                .with('favorites')
                .with('eventSend', builder => builder.select('id', 'name', 'image_url', 'description'))
                .with('eventReceiver', builder => builder.select('id', 'name', 'image_url', 'description'))
                .with('maintenance_time')
                .paginate(eventPage, eventPerpage)

                for (let [index, eventPartner] of eventPartners.rows.entries()) {
                    for (let property in eventPartner) {
                        for (let prop in eventPartner[property]) {
                            if (!eventPartner[property][prop]) {
                                delete eventPartner[property][prop]
                            }
                        }
                    }
                    for (let property in eventPartner.$relations) {
                        if(eventPartner.$relations[property]) {
                            if (property == 'eventReceiver' || property == 'eventSend') {
                                if (myEvents.rows.map( el => { return el.id }).indexOf(eventPartner.$relations[property].id) == -1) {
                                    eventPartner.eventData = eventPartner.$relations[property]
                                }
                            }
                        }
                        if (property != 'favorites')
                            delete eventPartner.$relations[property]
                    }

                    if (eventPartner.$relations.favorites.rows.length > 0) {
                        eventPartner.favorite = true;
                        delete eventPartner.$relations.favorites
                    }
                    else {
                        eventPartner.favorite = false;
                        delete eventPartner.$relations.favorites
                    }
                }

            const projectPartners = await Partner.query()
                .where(function() {
                    this.where(function() {
                        this.where(function() {
                            this.where('user_send_id', user.id)
                            this.orWhere('user_receiver_id', user.id)
                        })
                        this.andWhere(function() {
                            this.whereNotNull('project_send_id')
                            this.orWhereNotNull('project_receiver_id')
                        })
                    })
                    this.orWhere(function() {
                      if (myProjects.rows.length > 0) {
                        this.where(function() {
                          myProjects.rows.map( project => {
                              this.orWhere('project_send_id', project.id )
                              this.orWhere('project_receiver_id', project.id )
                          });
                        })
                        this.andWhere(function() {
                          myProjects.rows.map( project => {
                              this.whereNot('project_send_id', project.id )
                              this.orWhereNot('project_receiver_id', project.id )
                          });
                        })
                        this.andWhere(function() {
                          this.whereNotNull('project_send_id')
                          this.whereNotNull('project_receiver_id')
                        })
                      }
                    })
                    this.orWhere(function() {
                      if (myEvents.rows.length > 0 || myOrganizations.rows.length > 0) {
                        this.where(function() {
                                myEvents.rows.map( event => {
                                    this.orWhere('event_send_id', event.id )
                                    this.orWhere('event_receiver_id', event.id )
                                });
                                myOrganizations.rows.map( organization => {
                                    this.orWhere('organization_send_id', organization.id )
                                    this.orWhere('organization_receiver_id', organization.id )
                                });
                            }
                        )
                        this.andWhere(function() {
                            this.whereNotNull('project_send_id')
                            this.orWhereNotNull('project_receiver_id')
                        })
                      }
                    })
                })
                .andWhere('status', '1')
                .with('favorites')
                .with('projectSend', builder => builder.select('id', 'name', 'image_url', 'description'))
                .with('projectReceiver', builder => builder.select('id', 'name', 'image_url', 'description'))
                .with('maintenance_time')
                .paginate(projectPage, projectPerpage)

                for (let [index, projectPartner] of projectPartners.rows.entries()) {
                    for (let property in projectPartner) {
                        for (let prop in projectPartner[property]) {
                            if (!projectPartner[property][prop]) {
                                delete projectPartner[property][prop]
                            }
                        }
                    }
                    for (let property in projectPartner.$relations) {
                        if(projectPartner.$relations[property]) {
                            if (property == 'projectReceiver' || property == 'projectSend') {
                                if (myProjects.rows.map( el => { return el.id }).indexOf(projectPartner.$relations[property].id) == -1) {
                                    projectPartner.project = projectPartner.$relations[property]
                                }
                            }
                        }
                        if (property != 'favorites')
                            delete projectPartner.$relations[property]
                    }

                    if (projectPartner.$relations.favorites.rows.length > 0) {
                        projectPartner.favorite = true;
                        delete projectPartner.$relations.favorites
                    }
                    else {
                        projectPartner.favorite = false;
                        delete projectPartner.$relations.favorites
                    }
                };


            const organizationPartners = await Partner.query()
                .where(function() {
                    this.where(function() {
                        this.where(function() {
                            this.where('user_send_id', user.id)
                            this.orWhere('user_receiver_id', user.id)
                        })
                        this.andWhere(function() {
                            this.whereNotNull('organization_send_id')
                            this.orWhereNotNull('organization_receiver_id')
                        })
                    })
                    this.orWhere(function() {
                      if (myOrganizations.rows.length > 0) {
                        this.where(function() {
                          myOrganizations.rows.map( organization => {
                              this.orWhere('organization_send_id', organization.id )
                              this.orWhere('organization_receiver_id', organization.id )
                          });
                        })
                        this.andWhere(function() {
                          myOrganizations.rows.map( organization => {
                              this.whereNot('organization_send_id', organization.id )
                              this.orWhereNot('organization_receiver_id', organization.id )
                          });
                        })
                        this.where(function() {
                          this.whereNotNull('organization_send_id')
                          this.whereNotNull('organization_receiver_id')
                        })
                      }
                    })
                    this.orWhere(function() {
                      if (myEvents.rows.length > 0 || myProjects.rows.length > 0) {
                        this.where(function() {
                                myEvents.rows.map( event => {
                                    this.orWhere('event_send_id', event.id )
                                    this.orWhere('event_receiver_id', event.id )
                                });
                                myProjects.rows.map( project => {
                                    this.orWhere('project_send_id', project.id )
                                    this.orWhere('project_receiver_id', project.id )
                                });
                            }
                        )
                        this.andWhere(function() {
                            this.whereNotNull('organization_send_id')
                            this.orWhereNotNull('organization_receiver_id')
                        })
                      }
                    })
                })
                .andWhere('status', '1')
                .with('favorites')
                .with('organizationSend', builder => builder.select('id', 'name', 'image_url', 'description'))
                .with('organizationReceiver', builder => builder.select('id', 'name', 'image_url', 'description'))
                .with('maintenance_time')
                .paginate(organizationPage, organizationPerpage)

                for (let organizationPartner of organizationPartners.rows) {
                    for (let property in organizationPartner) {
                        for (let prop in organizationPartner[property]) {
                            if (!organizationPartner[property][prop]) {
                                delete organizationPartner[property][prop]
                            }
                        }
                    }
                    for (let property in organizationPartner.$relations) {
                        if(organizationPartner.$relations[property]) {
                            if (property == 'organizationReceiver' || property == 'organizationSend') {
                                if (myOrganizations.rows.map( el => { return el.id }).indexOf(organizationPartner.$relations[property].id) == -1) {
                                    organizationPartner.organization = organizationPartner.$relations[property]
                                }
                            }
                        }
                        if (property != 'favorites')
                        delete organizationPartner.$relations[property]
                    }

                    if (organizationPartner.$relations.favorites.rows.length > 0) {
                        organizationPartner.favorite = true;
                        delete organizationPartner.$relations.favorites
                    }
                    else {
                        organizationPartner.favorite = false;
                        delete organizationPartner.$relations.favorites
                    }
                }

            return {userPartners, eventPartners, projectPartners, organizationPartners}
        }
        catch (error) {
            return error.message
        }
    }
}

module.exports = PartnerController
