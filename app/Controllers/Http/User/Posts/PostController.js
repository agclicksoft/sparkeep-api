'use strict'

const Post = use('App/Models/Post');
const Inspired = use('App/Models/Inspired');
const User = use('App/Models/User');
const moment = require('moment');
const Database = use('Database');

class PostController {

    async index({request, response, params, auth}) {

        try {
            const id = params.id;

            const {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

            let user = await auth.getUser();

            const time = moment().subtract(48, 'hours').format('YYYY-MM-DD ');

            const posts = await Post.query()
                .select(
                    Database.raw(`*,
                        CASE
                            WHEN posts.created_at > '${time}' THEN 1
                            WHEN posts.created_at < '${time}' THEN 2
                        END as ord`
                    )
                )
                .with('authorUser')
                .with('images')
                .whereNot('exc', true)
                .withCount('inspireds as total_inspireds')
                .whereHas('authorUser', builder => {
                    builder.where('id', id)
                })
                // .orderByRaw( `
                //     (CASE WHEN ord = 1 THEN total_inspireds END) DESC, id DESC,
                //     (CASE WHEN ord = 2 THEN id END) DESC
                // ` )
                .paginate(page, perpage);

            for (let item of posts.rows) {
                if (item.$relations.authorUser) {
                    item.authorName = item.$relations.authorUser.name;
                    item.authorImage = item.$relations.authorUser.image_url
                }
                else if (item.$relations.authorEvent) {
                    item.authorName = item.$relations.authorEvent.name;
                    item.authorImage = item.$relations.authorEvent.image_url
                }
                else if (item.$relations.authorProject) {
                    item.authorName = item.$relations.authorProject.name;
                    item.authorImage = item.$relations.authorProject.image_url
                }
                else if (item.$relations.authorOrganization) {
                    item.authorName = item.$relations.authorOrganization.name;
                    item.authorImage = item.$relations.authorOrganization.image_url
                }

                const liked = await Inspired.query()
                    .where('post_inspiration_id', item.id)
                    .andWhere('user_inspired_id', user.id)
                    .first()

                liked ? item.liked = true : item.liked = false;
            }



            return posts
        }
        catch(error) {
            return response.status(500).send({message: error.message})
        }
    }
}

module.exports = PostController;
