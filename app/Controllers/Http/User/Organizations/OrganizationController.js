'use strict'

const User = use('App/Models/User');
const Organization = use('App/Models/Organization');
const OrganizationAdministrators = use('App/Models/OrganizationsAdministrator');

class OrganizationController {
    async index({request, params, response, auth}) {
        try {

            const ownerOrganizations = await Organization.query()
                .whereNot('exc', true)
                .where('user_id', params.id)
                .with('user')
                .with('project')
                .fetch()

                // const myOrganizationsIds = user.$relations.organizations.rows.map( el => el.id)

                // const myProjectsIds = user.$relations.projects.rows.map( el => el.id)

            const administratorsOrganizations = await Organization.query()
                .whereHas('administrators', builder => {
                    builder.where('user_id', params.id)
                    builder.where('status', 'accepted')
                })
                .fetch()

                return {ownerOrganizations, administratorsOrganizations};
        }
        catch (error) {
            return error
        }
    }
}

module.exports = OrganizationController
