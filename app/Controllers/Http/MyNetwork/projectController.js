'use strict'

const Project = use('App/Models/Project');

class ProjectController {

    async index({request, params, auth}) {

        let {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

        const user = await auth.getUser();

        const myProjects = await Project.query()
            .where('user_id', user.id)
            .whereNotNull('name')
            .orWhereHas('administrators', builder => builder.where('user_id', user.id))
            .fetch();

        const projects = await Project.query()
            .where(function() {
                if (keyword) this.where('name', 'like', `%${keyword}%`)
            })
            .andWhere(function() {
                for (let project of myProjects.rows)
                    this.andWhereNot('id', project.id)
            })
            .whereHas('partnerReceivedFromUser', builder => builder.where('user_send_id', user.id))
            .orWhereHas('partnerSendToUser', builder => builder.where('user_receiver_id', user.id))
            .paginate(page, perpage);

        return projects
    }

}

module.exports = ProjectController;