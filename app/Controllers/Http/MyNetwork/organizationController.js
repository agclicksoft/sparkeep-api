'use strict'

const Inspired = use('App/Models/Inspired');
const Organization = use('App/Models/Organization');

class OrganizationController {

    async index({request, params, auth}) {

        let {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

        const user = await auth.getUser();

        const myOrganizations = await Organization.query()
        .where('user_id', user.id)
        .whereNotNull('name')
        .orWhereHas('administrators', builder => builder.where('user_id', user.id))
        .fetch();

        const organizations = await Organization.query()
            .where(function() {
                if (keyword) this.where('name', 'like', `%${keyword}%`)
            })
            .andWhere(function() {
                for (let organization of myOrganizations.rows)
                    this.andWhereNot('id', organization.id)
            })
            .whereHas('userInspireds', builder => builder.where('user_inspired_id', user.id))
            .paginate(page, perpage);
            
        return organizations
    }

}

module.exports = OrganizationController;