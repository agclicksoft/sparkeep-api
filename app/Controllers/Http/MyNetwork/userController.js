'use strict'

const Inspired = use('App/Models/Inspired');

const User = use('App/Models/User');

class UserController {

    async index({request, params, auth}) {

        let {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

        const user = await auth.getUser();

        const users = await User.query()
            .where(function() {
                if (keyword) this.where('name', 'like', `%${keyword}%`)
            })
            .whereHas('userInspireds', builder => builder.where('user_inspired_id', user.id))
            .paginate(page, perpage);
            
        return users
    }

}

module.exports = UserController;