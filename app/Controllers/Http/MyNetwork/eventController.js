'use strict'

const Event = use('App/Models/Event');

class EventController {

    async index({request, params, auth}) {

        let {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

        const body = request.only(['page', 'perpage'])

        const user = await auth.getUser();

        const myEvents = await Event.query()
            .where('user_id', user.id)
            .whereNotNull('name')
            .orWhereHas('administrators', builder => builder.where('user_id', user.id))
            .fetch();

        const events = await Event.query()
            .where(function() {
                if (keyword) this.where('name', 'like', `%${keyword}%`)
            })
            .andWhere(function() {
                for (let event of myEvents.rows)
                    this.andWhereNot('id', event.id)
            })
            .whereHas('confirmedPeople', builder => builder.where('user_id', user.id))
            .paginate(page, perpage);

        return events
    }

}

module.exports = EventController;