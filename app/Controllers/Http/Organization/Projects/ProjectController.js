'use strict'

const Project = use('App/Models/Project')

class ProjectController {

    async index({request, params}) {
        const id = params.id;

        const {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

        const projects = await Project.query()
            .where('organization_id', params.id)
            .whereNot('exc', true)
            .paginate(page, perpage);

        return projects
    }

}

module.exports = ProjectController;