'use strict'

const Event = use('App/Models/Event')

class EventController {

    async index({request, params}) {
        const id = params.id;

        const {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

        const events = await Event.query()
            .where('organization_id', params.id)
            .whereNot('exc', true)
            .paginate(page, perpage);

        return events
    }

}

module.exports = EventController;