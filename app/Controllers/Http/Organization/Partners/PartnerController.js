'use strict'

const User = use('App/Models/User');
const Event = use('App/Models/Event');
const Project = use('App/Models/Project');
const Organization = use('App/Models/Organization');

const Partner = use ('App/Models/Partner')

class EventController {

    async index({request, params, auth}) {

        const {page = 1, perpage = 8} = request.only(['page', 'perpage']);

        const organization = await Organization.find(params.id);

        const user_id = organization.user_id;

        const myEvents = await Event.query()
            .where('user_id', user_id)
            .whereNot('exc', true)
            .orWhereHas('administrators', builder => builder.where('user_id', user_id))
            .fetch();

        const myProjects = await Project.query()
            .where('user_id', user_id)
            .whereNot('exc', true)
            .orWhereHas('administrators', builder => builder.where('user_id', user_id))
            .fetch();

        const myOrganizations = await Organization.query()
            .where('user_id', user_id)
            .whereNot('exc', true)
            .orWhereHas('administrators', builder => builder.where('user_id', user_id))
            .fetch();


        const partners = await Partner.query()
            .where(function() {
                this.where('organization_send_id', params.id)
                this.orWhere('organization_receiver_id', params.id)
            })
            .andWhere('status', '1')
            .with('userSend', builder => {
                builder.whereNot('id', user_id)
                builder.whereNot('exc', true)
                builder.select('id', 'name', 'image_url', 'description')
            })
            .with('userReceiver', builder => {
                builder.whereNot('id', user_id)
                builder.whereNot('exc', true)
                builder.select('id', 'name', 'image_url', 'description')
            })
            .with('eventSend', builder => {
                builder.where(function() {
                    myEvents.rows.map( event => this.andWhereNot('id', event.id))
                })
                builder.select('id', 'name', 'image_url', 'description')
            })
            .with('eventReceiver', builder => {
                builder.where(function() {
                    myEvents.rows.map( event => this.andWhereNot('id', event.id))
                })
                builder.select('id', 'name', 'image_url', 'description')
            })
            .with('projectSend', builder => {
                builder.where(function() {
                    myProjects.rows.map( project => this.andWhereNot('id', project.id))
                })
                builder.select('id', 'name', 'image_url', 'description')
            })
            .with('projectReceiver', builder => {
                builder.where(function() {
                    myProjects.rows.map( project => this.andWhereNot('id', project.id))
                })
                builder.select('id', 'name', 'image_url', 'description')
            })
            .with('organizationSend', builder => {
                builder.where(function() {
                    myOrganizations.rows.map( organization => this.andWhereNot('id', organization.id))
                })
                builder.select('id', 'name', 'image_url', 'description')
            })
            .with('organizationReceiver', builder => {
                builder.where(function() {
                    myOrganizations.rows.map( organization => this.andWhereNot('id', organization.id))
                })
                builder.select('id', 'name', 'image_url', 'description')
            })
            .paginate(page, perpage);

            for (let partner of partners.rows) {
                for (let property in partner) {
                    for (let prop in partner[property]) {
                        if (!partner[property][prop]) {
                            delete partner[property][prop]
                        }
                    }
                }
                for (let property in partner.$relations) {
                    if(partner.$relations[property]) {
                        partner.partner = partner.$relations[property]
                    }
                    delete partner.$relations[property]
                }
            };

        return partners
    }

}

module.exports = EventController;