"use strict";

const Organization = use("App/Models/Organization");
const Inspired = use("App/Models/Inspired");
const Partner = use('App/Models/Partner');
const OrganizationAdministrator = use("App/Models/OrganizationsAdministrator");

const Notification = use("App/Models/Notification");
const NotificationReceiver = use("App/Models/NotificationsReceiver");
const moment = require("moment");
const Drive = use("Drive");

class OrganizationController {
  async index({ request, response, params }) {
    try {
      const { page = 1, perpage = 8, keyword } = request.only([
        "page",
        "perpage",
        "keyword",
      ]);

      const organizations = await Organization.query()
        .where(function () {
          if (keyword) this.where("name", "LIKE", `%${keyword}%`);
        })
        .whereNot('exc', true)
        .paginate(page, perpage);

      return organizations;
    } catch (error) {
      return error;
    }
  }

    async show({auth, params}) {
        try {
            const user = await auth.getUser();

            const organization = await Organization.query()
            .where('id', params.id)
            .whereNot('exc', true)
            .with('administrators', builder => {
                builder.where('status', 'accepted')
                builder.whereNot('exc', true)
                builder.select(
                    'id',
                    'email',
                    'name',
                    'description',
                    'image_url'
                )
            })
            .with('passions')
            .withCount('inspireds as total_inspireds', builder => {
                builder.whereNotNull('user_inspired_id')
            })
            .withCount('send_partners as total_send_partners', builder => builder.where('status', '1'))
            .withCount('received_partners as total_received_partners', builder => builder.where('status', '1'))
            .first()

            const inspired = await Inspired.query()
                .where('organization_inspiration_id', organization.id)
                .andWhere('user_inspired_id', user.id)
                .first()

            inspired ? organization.inspired = true : organization.inspired = false;

            return organization
        }
        catch (error) {
            return error
        }
    }

  async store({ request, response }) {
    try {
      const organizationBody = request.only([
        "owner",
        "user_id",
        "project_id",
        "name",
        "description",
        "local",
        "address_country",
        "address_state",
        "address_city",
        "address_neighborhood",
        "address_street",
        "address_complement",
        "telephone",
        "show_telephone",
        "cellphone",
        "show_cellphone",
        "cnpj",
        "cpf",
        "philantropy_record",
        "has_philantropy",
        "image_url",
        "email",
        "youtube",
        "facebook",
        "instagram",
        "twitter",
        "site",
      ]);

      const { administrators, passions } = request.only([
        "administrators",
        "passions",
      ]);

      let organization = await Organization.query()
        .where("name", organizationBody.name)
        .first();

      // if (organization)
      //     return response.status(400).send({message: 'Já existe um projeto com este nome'})

      let cnpj = await Organization.query().where("cnpj", organizationBody.cnpj).first();
      if (cnpj) {
        return response.status(400).send({
          message: "CNPJ já cadastrado!",
        });
      }

      organization = await Organization.create(organizationBody);

      await organization.administrators().detach();
      await organization.administrators().attach(administrators);
      await organization.passions().detach();
      await organization.passions().attach(passions);

      const notificationRequestBody = {
        organization_send_id: organization.id,
        description: `A organização ${organization.name} fez uma solicitação para você ser administrador.`,
        type: "request",
      };

      const notificationRequest = await Notification.create(
        notificationRequestBody
      );
      const notificationReceiverBody = [];
      for (let property of administrators) {
        notificationReceiverBody.push({
          notification_id: notificationRequest.id,
          user_receiver_id: property,
          viewed: false,
        });
      }
      await NotificationReceiver.createMany(notificationReceiverBody);

      return organization;
    } catch (error) {
      return response.status(400).send({
        message: "Algo deu errado em criar a entidade",
      });
      // return error
    }
  }

  async update({ request, params, response }) {
    try {
      const organizationBody = request.only([
        "owner",
        "user_id",
        "project_id",
        "name",
        "description",
        "local",
        "address_country",
        "address_state",
        "address_city",
        "address_neighborhood",
        "address_street",
        "address_complement",
        "telephone",
        "show_telephone",
        "cellphone",
        "show_cellphone",
        "cnpj",
        "cpf",
        "philantropy_record",
        "has_philantropy",
        "image_url",
        "email",
        "youtube",
        "facebook",
        "instagram",
        "twitter",
        "site",
      ]);

      const { administrators, passions } = request.only([
        "administrators",
        "passions",
      ]);

      const organization = await Organization.find(params.id);

      let cnpj = await Organization.query().where("cnpj", organizationBody.cnpj).first();
      if (cnpj.$attributes.id !== organization.$attributes.id) {
        return response.status(400).send({
          message: "CNPJ já cadastrado!",
        });
      }

      const administratorsExistents = [];
      const administratorsToSend = [];
      for (let [index, item] of administrators.entries()) {
        const ev = await OrganizationAdministrator.query()
          .where("organization_id", organization.id)
          .andWhere("user_id", item)
          .first();

        if (
          ev &&
          ev.status == "rejected" &&
          ev.updated_at > moment().subtract(30, "days")
        ) {
          administratorsExistents.push(item);
        }

        if (ev && (ev.status == "pending" || ev.status == "accepted")) {
          administratorsExistents.push(item);
        }

        if (
          ev &&
          ev.status == "rejected" &&
          ev.updated_at < moment().subtract(30, "days")
        ) {
          administratorsToSend.push(item);
        }
      }

      for (let item of administratorsExistents) {
        administrators.splice(administrators.indexOf(item), 1);
      }

      for (let item in administratorsToSend) {
        const ev = await OrganizationAdministrator.query()
          .where("organization_id", organization.id)
          .andWhere("user_id", item)
          .delete();

        await OrganizationAdministrator.create({
          organization_id: organization.id,
          user_id: item,
        });
      }

      if (!organization)
        return response.status(404).send({ message: "Entidade não encontrada" });

      await organization.administrators().attach(administrators);

      await organization.passions().detach();
      await organization.passions().attach(passions);

      await organization.merge(organizationBody);
      await organization.save();

      const notificationRequestBody = {
        organization_send_id: organization.id,
        description: `A organização ${organization.name} fez uma solicitação para você ser administrador.`,
        type: "request",
      };

      const notificationRequest = await Notification.create(
        notificationRequestBody
      );
      const notificationReceiverBody = [];
      for (let property of administrators) {
        notificationReceiverBody.push({
          notification_id: notificationRequest.id,
          user_receiver_id: property,
          viewed: false,
        });
      }
      await NotificationReceiver.createMany(notificationReceiverBody);

      let partnerReceivedFromUser = await Partner.query()
        .where('organization_receiver_id', organization.id)
        .andWhere('status', '1')
        .whereNotNull('user_send_id')
        .pluck('user_send_id');

      partnerReceivedFromUser = partnerReceivedFromUser.map( id => {
        return { user_receiver_id: id }
      })

      let userInspireds = await Inspired.query()
        .where('organization_inspiration_id', organization.id)
        .whereNotNull('user_inspired_id')
        .pluck('user_inspired_id')

      userInspireds = userInspireds.map( id => {
        return { user_receiver_id: id }
      })

      let receiversIds = [...partnerReceivedFromUser, ...userInspireds];

      receiversIds = receiversIds
        .map(el => el.user_receiver_id )
        .filter((elem, index, self) => index === self.indexOf(elem))
        .map(id => { return {user_receiver_id: id} })

      const notificationUpdated = await Notification.create({
        organization_send_id: organization.id,
        description: `A entidade ${organization.name} fez alterações importantes em suas informações! Dê uma olhada para não ficar de fora de nenhuma novidade.`,
        type: 'message'
      })

      await notificationUpdated.receivers().createMany(receiversIds);


    await notificationUpdated.receivers().createMany(receiversIds);

      return organization;
    } catch (error) {
      return response.status(400).send({
        message: error.message,
      });
    }
  }

  async destroy({ auth, request, response, params }) {
    try {
      const user = await auth.getUser();
      const organization = await Organization.find(params.id);

      if (organization.user_id && user.id != organization.user_id)
        return response.status(400).send({
          message: "Você não tem permissão para excluir esta organização.",
        });

      await organization.merge({
        owner: null,
        name: null,
        description: null,
        local: null,
        address_country: null,
        address_state: null,
        address_city: null,
        telephone: null,
        show_telephone: null,
        cnpj: null,
        cpf: null,
        philantropy_record: null,
        image_url: null,
        email: null,
        youtube: null,
        facebook: null,
        instagram: null,
        twitter: null,
        site: null,
        exc: true
      });
      await organization.save();

      return organization;
    } catch (error) {
      return error;
    }
  }

  async uploadAttachment({ request, response, params }) {
    try {
      const organization = await Organization.find(params.id);

      const validationOptions = {
        types: ["jpeg", "jpg", "png"],
        size: "5mb",
      };

      // return organization

      request.multipart.file("attachment", validationOptions, async (file) => {
        // Set file size from stream byteCount, so adonis can validate file size
        file.size = file.stream.byteCount;
        // Catches validation errors, if any and then throw exception
        const error = file.error();

        if (error.message) throw new Error(error.message);

        const Key = `organization_images/${(Math.random() * 100).toString()}-${
          file.clientName
        }`;
        const ContentType = file.headers["content-type"];
        const ACL = "public-read";
        // upload file to s3
        const path = await Drive.put(Key, file.stream, { ContentType, ACL });

        await organization.merge({ image_url: path });
        await organization.save();
      });

      await request.multipart.process();

      return organization;
    } catch (error) {
      return error;
    }
  }

  async inspire({ request, response, auth }) {
    try {
      const user = await auth.getUser();

      const body = request.only(["organization_inspiration_id"]);
      body.user_inspired_id = user.id;

      const inspired = await Inspired.create(body);

      return inspired;
    } catch (error) {
      return response.status(500).send({
        message: "Ocorreu um erro inesperado. Tente novamente mais tarde",
      });
    }
  }
}

module.exports = OrganizationController;
