'use strict'

const Inspired = use('App/Models/Inspired');

class InspireController {

    async store({request, response, auth}) {
        try {
            const user = await auth.getUser();
    
            const body = request.only([
                    'user_inspiration_id',
                    'organization_inspiration_id'
                ]);
            body.user_inspired_id = user.id;

            let inspired = await Inspired.query()
                .where('user_inspired_id', user.id)
                .andWhere(function() {
                    if (body.user_inspiration_id)
                        this.where('user_inspiration_id', body.user_inspiration_id)

                    if (body.organization_inspiration_id)
                        this.where('organization_inspiration_id', body.organization_inspiration_id)
                })
                .first();

            if (inspired) {
                await inspired.delete();

                return response.status(200).send({
                    message: 'Inspiração removida com sucesso',
                    status: false
                })
            }
            else {
                inspired = await Inspired.create(body);
    
                return {
                    inspired,
                    status: true
                }
            }
        }
        catch (error) {
            return response.status(500).send({message: "Ocorreu um erro inesperado. Tente novamente mais tarde"})
        }
    }

}

module.exports = InspireController;
