"use strict";

const Drive = use("Drive");
const Project = use("App/Models/Project");
const Inspired = use("App/Models/Inspired");
const Partner = use("App/Models/Partner");
const ProjectAdministrator = use("App/Models/ProjectsAdministrator");
const Notification = use("App/Models/Notification");
const NotificationReceiver = use("App/Models/NotificationsReceiver");
const moment = require("moment");


class ProjectController {
  async index({ request, response }) {
    try {
      const { page = 1, perpage = 8, keyword } = request.only([
        "page",
        "perpage",
        "keyword",
      ]);

      const projects = await Project.query()
        .where(function () {
          if (keyword) this.where("name", "LIKE", `%${keyword}%`);
        })
        .whereNot("exc", true)
        .paginate(page, perpage);

      return projects;
    } catch (error) {
      return error;
    }
  }

    async show({auth, params}) {
        try {
            const project = await Project.query()
            .where('id', params.id)
            .whereNot("exc", true)
            .with('administrators', builder => {
                builder.where('status', 'accepted')
                builder.whereNot('exc', true)
                builder.select(
                    'id',
                    'email',
                    'name',
                    'description',
                    'image_url'
                )
            })
            .with('passions')
            .with('shortages')
            .with('maintenance_times')
            .first()

            const user = await auth.getUser();

            // const inspired = await Inspired.query()
            //     .where('project_inspiration_id', project.id)
            //     .andWhere('user_inspired_id', user.id)
            //     .first()

            // inspired ? project.inspired = true : project.inspired = false;

            return project
        }
        catch (error) {
            return error
        }
    }

  async store({ request, response }) {
    try {
      const projectBody = request.only([
        "user_id",
        "organization_id",
        "name",
        "description",
        "local",
        "address_country",
        "address_state",
        "address_city",
        "address_neighborhood",
        "address_street",
        "address_complement",
        "telephone",
        "show_telephone",
        "cellphone",
        "show_cellphone",
        "image_url",
        "kind_of_help",
        "interest_region",
        "email",
        "youtube",
        "facebook",
        "instagram",
        "twitter",
        "site",
        "has_shortages",
      ]);

      const {
        administrators,
        passions,
        shortages,
        maintenance_times,
      } = request.only([
        "administrators",
        "passions",
        "shortages",
        "maintenance_times",
      ]);

      let project = await Project.query()
        .where("name", projectBody.name)
        .first();

      // if (project)
      //     return response.status(400).send({message: 'Já existe um projeto com este nome'})

      project = await Project.create(projectBody);

      await project.administrators().detach();
      await project.administrators().attach(administrators);
      await project.passions().detach();
      await project.passions().attach(passions);
      await project.shortages().detach();
      await project.shortages().attach(shortages);
      await project.maintenance_times().sync(maintenance_times);

      const notificationRequestBody = {
        project_send_id: project.id,
        description: `O projeto ${project.name} fez uma solicitação para você ser administrador.`,
        type: "request",
      };

      const notificationRequest = await Notification.create(
        notificationRequestBody
      );
      const notificationReceiverBody = [];
      for (let property of administrators) {
        notificationReceiverBody.push({
          notification_id: notificationRequest.id,
          user_receiver_id: property,
          viewed: false,
        });
      }
      await NotificationReceiver.createMany(notificationReceiverBody);

      if (projectBody.user_id) {
        await project.loadMany({
          user: (builder) => {
            builder.with("inspireds");
            builder.select("id");
          },
        });
        const notificationBody = {
          user_send_id: projectBody.user_id,
          description: `O usuário ${project.$relations.user.name} criou um novo projeto. Fique por dentro das informações deste projeto.`,
        };
        const notification = await Notification.create(notificationBody);
        for (let item of project.$relations.user.$relations.inspireds.rows) {
          await NotificationReceiver.create({
            notification_id: notification.id,
            user_receiver_id: item.user_inspired_id,
            viewed: false,
          });
        }
      } else if (projectBody.organization_id) {
        await project.loadMany({
          organization: (builder) => {
            builder.with("inspireds");
            builder.select("id");
          },
        });
        const notificationBody = {
          organization_send_id: projectBody.organization_id,
          description: `A organização ${project.$relations.organization.name} criou um novo projeto. Fique por dentro das informações deste projeto.`,
        };
        const notification = await Notification.create(notificationBody);
        for (let item of project.$relations.organization.$relations.inspireds
          .rows) {
          await NotificationReceiver.create({
            notification_id: notification.id,
            user_receiver_id: item.id,
            viewed: false,
          });
        }
      }

      return project;
    } catch (error) {
      return error;
    }
  }

  async update({ request, params, response }) {
    try {
      const projectBody = request.only([
        "user_id",
        "organization_id",
        "name",
        "description",
        "local",
        "address_country",
        "address_state",
        "address_city",
        "address_neighborhood",
        "address_street",
        "address_complement",
        "telephone",
        "show_telephone",
        "cellphone",
        "show_cellphone",
        "image_url",
        "kind_of_help",
        "interest_region",
        "email",
        "youtube",
        "facebook",
        "instagram",
        "twitter",
        "site",
        "has_shortages",
      ]);

      const {
        administrators,
        passions,
        shortages,
        maintenance_times,
      } = request.only([
        "administrators",
        "passions",
        "shortages",
        "maintenance_times",
      ]);

      const project = await Project.find(params.id);

      const administratorsExistents = [];
      const administratorsToSend = [];
      for (let [index, item] of administrators.entries()) {
        const ev = await ProjectAdministrator.query()
          .where("project_id", project.id)
          .andWhere("user_id", item)
          .first();

        if (
          ev &&
          ev.status == "rejected" &&
          ev.updated_at > moment().subtract(30, "days")
        ) {
          administratorsExistents.push(item);
        }

        if (ev && (ev.status == "pending" || ev.status == "accepted")) {
          administratorsExistents.push(item);
        }

        if (
          ev &&
          ev.status == "rejected" &&
          ev.updated_at < moment().subtract(30, "days")
        ) {
          administratorsToSend.push(item);
        }
      }

      for (let item of administratorsExistents) {
        administrators.splice(administrators.indexOf(item), 1);
      }

      for (let item in administratorsToSend) {
        const ev = await ProjectAdministrator.query()
          .where("project_id", project.id)
          .andWhere("user_id", item)
          .delete();

        await ProjectAdministrator.create({
          project_id: project.id,
          user_id: item,
        });
      }

      if (!project)
        return response.status(404).send({ message: "Projeto não encontrado" });

      await project.administrators().attach(administrators);

      await project.passions().detach();
      await project.passions().attach(passions);
      await project.shortages().detach();
      await project.shortages().attach(shortages);
      await project.merge(projectBody);
      await project.maintenance_times().sync(maintenance_times);

      await project.save();

      const notificationRequestBody = {
        project_send_id: project.id,
        description: `O projeto ${project.name} fez uma solicitação para você ser administrador.`,
        type: "request",
      };

      const notificationRequest = await Notification.create(
        notificationRequestBody
      );
      const notificationReceiverBody = [];
      for (let property of administrators) {
        notificationReceiverBody.push({
          notification_id: notificationRequest.id,
          user_receiver_id: property,
          viewed: false,
        });
      }
      await NotificationReceiver.createMany(notificationReceiverBody);

      let partnerReceivedFromUser = await Partner.query()
        .where('project_receiver_id', project.id)
        .andWhere('status', '1')
        .whereNotNull('user_send_id')
        .pluck('user_send_id');

      partnerReceivedFromUser = partnerReceivedFromUser.map( id => {
        return { user_receiver_id: id }
      })

      const notificationUpdated = await Notification.create({
        project_send_id: project.id,
        description: `O projeto ${project.name} fez alterações importantes em suas informações! Dê uma olhada para não ficar de fora de nenhuma novidade.`,
        type: 'message'
      })

      await notificationUpdated.receivers().createMany(partnerReceivedFromUser);

      return project;
    } catch (error) {
      return error.message;
    }
  }

  async destroy({ auth, request, response, params }) {
    try {
      const user = await auth.getUser();
      const project = await Project.find(params.id);

      if (project.user_id && user.id != project.user_id)
        return response.status(400).send({
          message: "Você não tem permissão para excluir este projeto.",
        });

      await project.merge({
        name: null,
        description: null,
        local: null,
        telephone: null,
        show_telephone: null,
        image_url: null,
        kind_of_help: null,
        interest_region: null,
        email: null,
        youtube: null,
        facebook: null,
        instagram: null,
        twitter: null,
        site: null,
        exc: true
      });
      await project.save();

      return project;
    } catch (error) {
      return error;
    }
  }

  async uploadAttachment({ request, response, params }) {
    try {
      const project = await Project.find(params.id);

      const validationOptions = {
        types: ["jpeg", "jpg", "png"],
        size: "5mb",
      };

      request.multipart.file("attachment", validationOptions, async (file) => {
        // Set file size from stream byteCount, so adonis can validate file size
        file.size = file.stream.byteCount;
        // Catches validation errors, if any and then throw exception
        const error = file.error();

        if (error.message) throw new Error(error.message);

        const Key = `project_images/${(Math.random() * 100).toString()}-${
          file.clientName
        }`;
        const ContentType = file.headers["content-type"];
        const ACL = "public-read";
        // upload file to s3
        const path = await Drive.put(Key, file.stream, { ContentType, ACL });

        await project.merge({ image_url: path });
        await project.save();
      });

      await request.multipart.process();

      return project;
    } catch (error) {
      return error;
    }
  }

  async inspire({ request, response, auth }) {
    try {
      const user = await auth.getUser();

      const body = request.only(["project_inspiration_id"]);
      body.user_inspired_id = user.id;

      const inspired = await Inspired.create(body);

      return inspired;
    } catch (error) {
      return response.status(500).send({
        message: "Ocorreu um erro inesperado. Tente novamente mais tarde",
      });
    }
  }
}

module.exports = ProjectController;
