'use strict'

const ProjectAdministrator = use('App/Models/ProjectsAdministrator')

class AdministratorController {
    
    async update({request, response, params}) {
        try {
            /**
             * STATUS PODEM SER DE 3 TIPOS:
             * rejected
             * pending
             * accepted
             */
            const body = request.only([ 'status' ]);

            const administrator = await ProjectAdministrator.find(params.id);

            await administrator.merge(body);
            await administrator.save();

            return administrator
                
        }
        catch(error) {
            return response.status(500).send({message: error.message})
        }
    }

}

module.exports = AdministratorController