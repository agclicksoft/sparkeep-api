'use strict'

const Post = use('App/Models/Post');
const Inspired = use('App/Models/Inspired');
const moment = require('moment');
const Database = use('Database');

class PostController {

    async index({request, params, auth}) {
        const id = params.id;

        const {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

        let user = await auth.getUser();

        const time = moment().subtract(48, 'hours').format('YYYY-MM-DD ');

        const posts = await Post.query()
            .select(
                Database.raw(`*,
                    CASE
                        WHEN posts.created_at > '${time}' THEN 1
                        WHEN posts.created_at < '${time}' THEN 2
                    END as ord`
                )
            )
            .with('authorProject')
            .with('images')
            .withCount('inspireds as total_inspireds')
            .whereNot('exc', true)
            .whereHas('authorProject', builder => {
                builder.where('id', id)
            })
            // .orderByRaw( `
            //     (CASE WHEN ord = 1 THEN total_inspireds END) DESC, id DESC,
            //     (CASE WHEN ord = 2 THEN id END) DESC
            // ` )
            .paginate(page, perpage);

            for (let item of posts.rows) {
                item.authorName = item.$relations.authorProject.name;
                item.authorImage = item.$relations.authorProject.image_url

                const liked = await Inspired.query()
                    .where('post_inspiration_id', item.id)
                    .andWhere('user_inspired_id', user.id)
                    .first()

                liked ? item.liked = true : item.liked = false;
            }

        return posts
    }

}

module.exports = PostController;
