'use strict'

const Partner = use('App/Models/Partner');
const User = use('App/Models/User');
const Event = use('App/Models/Event');
const Project = use('App/Models/Project');
const Organization = use('App/Models/Organization');
const Notification = use('App/Models/Notification');
const NotificationReceiver = use('App/Models/NotificationsReceiver');
const PartnerFavorite = use('App/Models/PartnerFavorite')

class UserController {
    async index({request, auth}) {
        try {
            var user = null;
            var event = null;
            var project = null;
            var organization = null;
            const {
                page,
                perpage,
                status,
                role,
                id,
                keyword
            } = request.only([
                'page',
                'perpage',
                'status',
                'role',
                'id',
                'keyword'
            ]);

            if (role === 'user')
                user = await User.query().where('id', id).first()

            else if (role === 'event')
                event = await Event.query().where('id', id).first()

            else if (role === 'project')
                project = await Project.query().where('id', id).first()

            else if (role === 'organization')
                organization = await Organization.query().where('id', id).first()

            const partners = await Partner.query()
                .where(function() {
                    if (status) this.where('status', status)
                })
                .andWhere(function () {
                    if (role === 'user') {
                        this.where('user_send_id', user.id)
                        this.orWhere('user_receiver_id', user.id)
                    }
                    else if (role === 'event') {
                        this.where('event_send_id', event.id)
                        this.orWhere('event_receiver_id', event.id)
                    }
                    else if (role === 'project') {
                        this.where('project_send_id', project.id)
                        this.orWhere('project_receiver_id', project.id)
                    }
                    else if (role === 'organization') {
                        this.where('organization_send_id', organization.id)
                        this.orWhere('organization_receiver_id', organization.id)
                    }
                })
                .andWhere(function() {
                    if (keyword) {
                        if (role === 'user') {
                            this.whereHas('userSend', builder => {
                                builder.where('name', 'like', `%${keyword}%`);
                            })
                            this.orWhereHas('userReceiver', builder => {
                                builder.where('name', 'like', `%${keyword}%`);
                            })
                        }
                        else if (role === 'event') {
                            this.whereHas('eventSend', builder => {
                                builder.where('name', 'like', `%${keyword}%`);
                            })
                            this.orWhereHas('eventReceiver', builder => {
                                builder.where('name', 'like', `%${keyword}%`);
                            })
                        }
                        else if (role === 'project') {
                            this.whereHas('projectSend', builder => {
                                builder.where('name', 'like', `%${keyword}%`);
                            })
                            this.orWhereHas('projectReceiver', builder => {
                                builder.where('name', 'like', `%${keyword}%`);
                            })
                        }
                        else if (role === 'organization') {
                            this.whereHas('organizationSend', builder => {
                                builder.where('name', 'like', `%${keyword}%`);
                            })
                            this.orWhereHas('organizationReceiver', builder => {
                                builder.where('name', 'like', `%${keyword}%`);
                            })
                        }
                    }

                })
                .with('userSend')
                .with('userReceiver')
                .with('eventSend')
                .with('eventReceiver')
                .with('projectSend')
                .with('projectReceiver')
                .with('organizationSend')
                .with('organizationReceiver')
                .paginate(page, perpage);

                for (let item of partners.rows) {

                    if (item.$relations.userSend)
                        item.partnerSend = item.$relations.userSend

                    else if (item.$relations.eventSend)
                        item.partnerSend = item.$relations.eventSend

                    else if (item.$relations.projectSend)
                        item.partnerSend = item.$relations.projectSend

                    else if (item.$relations.organizationSend)
                        item.partnerSend = item.$relations.organizationSend

                    delete item.$relations.userSend
                    delete item.$relations.userReceiver
                    delete item.$relations.eventSend
                    delete item.$relations.eventReceiver
                    delete item.$relations.projectSend
                    delete item.$relations.projectReceiver
                    delete item.$relations.organizationSend
                    delete item.$relations.organizationReceiver
                }

            return partners
        }
        catch (error) {
            return error
        }
    }

    async show({params, response}) {
        try {
            const partner = await Partner.query()
                .where('id', params.id)
                .first();

            if (!partner) return response.status(404).send({message: 'Nenhuma parceria encontrada'});

            return partner
        }
        catch (error) {
            return error
        }
    }

    async store({request, response, auth}) {
        try {

            const user = await auth.getUser();

            const body = request.only([
                'user_send_id',
                'user_receiver_id',
                'project_send_id',
                'project_receiver_id',
                'organization_send_id',
                'organization_receiver_id',
                'event_send_id',
                'event_receiver_id',
                'shortage_id',
                'message',
                'partnership_agreement',
                'status',
                'maintenance_time_id'
            ])

            var receiver = null;
            var send = null;
            var profileSend = null;
            var userReceiver = null;

            var userSend = null;
            var eventSend = null;
            var projectSend = null;
            var organizationSend = null;


            for (let property in body) {
                if (body[property] && property.indexOf('receiver') > -1) {
                    if (property.indexOf('event') > -1) {
                        receiver = property;

                        userReceiver = await Event.query()
                            .where('id', body[receiver])
                            .first();
                    }
                    else if (property.indexOf('project') > -1) {
                        receiver = property;

                        userReceiver = await Project.query()
                        .where('id', body[receiver])
                        .first();
                    }
                    else if (property.indexOf('organization') > -1) {
                        receiver = property;

                        userReceiver = await Organization.query()
                        .where('id', body[receiver])
                        .first();
                    }
                }
                else if (body[property] && property.indexOf('send') > -1) {
                    if (property.indexOf('event') > -1) {
                        send = property;

                        profileSend = await Event.query()
                            .where('id', body[send])
                            .first();
                    }
                    else if (property.indexOf('project') > -1) {
                        send = property;

                        profileSend = await Project.query()
                            .where('id', body[send])
                            .first();
                    }
                    else if (property.indexOf('organization') > -1) {
                        send = property;

                        profileSend = await Organization.query()
                            .where('id', body[send])
                            .first();
                    }
                    else if (property.indexOf('user') > -1) {
                        send = property;

                        profileSend = await User.query()
                            .where('id', body[send])
                            .first();
                    }
                }
            }

            let partner = await Partner.query()
                .where(send, body[send])
                .andWhere(receiver, body[receiver])
                .first();

            if (partner) {
                return response.status(401).send({message: 'Você já enviou esta solicitação de parceria. Aguarde pela resposta do proprietário !'})
            }
            else {
                const partner = await Partner.create(body);

                const notificationBody = {
                    partnership_id: partner.id,
                    description: `${profileSend.name} fez uma nova solicitação de parceria. Não o deixe esperando !`,
                    type: 'message'
                };

                notificationBody[send] = body[send];

                const notification = await Notification.create(notificationBody);

                const notificationReceiver = await NotificationReceiver.create({
                    notification_id: notification.id,
                    user_receiver_id: userReceiver.user_id,
                    viewed: false
                })



                return {partner, notification, notificationReceiver}
            }
        }
        catch (error) {
            return error
        }
    }

    async update({request, params, response}) {
        try {
            const body = request.only([
                'user_send_id',
                'user_receiver_id',
                'project_send_id',
                'project_receiver_id',
                'organization_send_id',
                'organization_receiver_id',
                'event_send_id',
                'event_receiver_id',
                'shortage_id',
                'message',
                'partnership_agreement',
                'status',
                'maintenance_time_id'
            ])

            for (let property in body) {
                if ( body[property] && property.indexOf('receiver') > -1 ) {

                    if (property.indexOf('event') > -1) {

                        receiver = property;

                        const event = await Event.find(body[property]);

                        if (event.id !== body[property])
                            return response.status(401).send({message: 'Você não tem permissão para aceitar esta parceria, apenas propietários podem aceitar parcerias'});

                    }
                    else if (property.indexOf('project') > -1) {

                        receiver = property;

                        const project = await Project.find(body[property]);

                        if (project.id !== body[property])
                            return response.status(401).send({message: 'Você não tem permissão para aceitar esta parceria, apenas propietários podem aceitar parcerias'});

                    }
                    else if (property.indexOf('organization') > -1) {

                        receiver = property;

                        const organization = await Organization.find(body[property]);

                        if (organization.id !== body[property])
                            return response.status(401).send({message: 'Você não tem permissão para aceitar esta parceria, apenas propietários podem aceitar parcerias'});
                    }

                }

                if (body[property] && property.indexOf('send') > -1) {
                    send = property;
                }
            }

            const partner = await Partner.query()
                .where('id', params.id)
                .first();

            if (!partner) return response.status(404).send({message: 'Nenhuma parceria encontrada'});

            await partner.merge(body);
            await partner.save();

            return partner
        }
        catch (error) {
            return error
        }
    }

    async delete() {
        try {
            const partner = await Partner.query()
                .where('id', params.id)
                .first();

            await partner.delete();

            return response.status(200).send({message: 'Parceria excluída com sucesso'});
        }
        catch (error) {
            return error
        }
    }

    async acceptAll({request, response}) {
        const {partnersIds} = request.only(['partnersIds']);

        for (let item of partnersIds) {
            const partner = await Partner.query().where('id', item).first();
            await partner.merge({status: '1'})
            await partner.save();
        }

        return response.status(200).send({message: 'Todas as parcerias foram atualizadas.'})
    }

    async rejectAll({ request, response }) {
        const {partnersIds} = request.only(['partnersIds']);

        for (let item of partnersIds) {
            const partner = await Partner.query().where('id', item).first();
            await partner.merge({status: '2'})
            await partner.save();
        }

        return response.status(200).send({message: 'Todas as parcerias foram atualizadas.'})
    }

    async favorite({ request, response}) {
        const body = request.only([
            'partner_id',
            'user_id',
        ]);

        const { action } = request.only(['action']);

        let partnerFavorite = await PartnerFavorite.query()
            .where('partner_id', body.partner_id)
            .andWhere('user_id', body.user_id)
            .first();

        if (partnerFavorite) {
            if (action === 'favorite') {
                return response.status(400).send({ message: "Esta parceria já está favoritada" })
            }
            else if (action === 'unfavorite') {
                await partnerFavorite.delete();

                return response.status(200).send({ message: "Parceria desfavoritada com sucesso." })
            }
        }
        else {
            if (action === 'favorite') {
                await PartnerFavorite.create(body);
                return response.status(200).send({message: "Parceria favoritada com sucesso."})
            }
        }

    }
}

module.exports = UserController
