'use strict'

const User = use('App/Models/User')

class AuthController {

  async login({request, response, auth}) {

    const body = request.only(['name', 'email', 'provider', 'password'])
          body.password = "b8781366b492544f455b6a3bc588dba9"
          body.socialid = request.only(['id'])['id'];

    const findUser = await User.findBy('email', body.email)
    const findUserBySocialId = await User.findBy('socialid', body.socialid);
    
    if (findUserBySocialId && findUserBySocialId.provider != body.provider)
      return response.status(400).send({message: 'SocialID do usuário ja cadastrado'})

    if (!findUser) {
      var user = await User.create(body);

      const data = await auth.attempt(body.email, body.password)

      return response.status(200).send({data, user})
    }

    if (findUser.provider !== body.provider) {
      return response.status(401).send({message: `Usuário cadastro com ${findUser.provider}. Utilize-o para fazer login`})
    }

    if (findUser.socialid !== body.socialid) {
      return response.status(401).send({message: `ID enviado não corresponde ao id cadastrado`})
    }


    if (findUser.provider === body.provider) {
      const data = await auth.attempt(body.email, body.password)

      return response.status(200).send({data, findUser})
    }
  }

}

module.exports = AuthController;