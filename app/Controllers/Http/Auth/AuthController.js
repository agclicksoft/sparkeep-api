'use strict'

const User = use('App/Models/User');
const resetedPassword = use('App/Models/ResetedPassword')

const InterestRegions = use('App/Models/UserInterestRegion')

const moment = require('moment');
const sgMail = require('@sendgrid/mail');

const mailTemplate = require('../../../../mail-templates/forgot-password')

class AuthController {

    // MÉTODO PARA REALIZAR LOGIN
    async signIn({auth, request, response}) {
        try {

            const {email, password} = request.only(['email', 'password'])

            const data = await auth.withRefreshToken().attempt(email, password);

            const user = await User.query()
                .where('email', email)
                .with('passions')
                .with('shortage')
                .with('posts')
                .with('notifications')
                .with('connectionsSend')
                .with('connectionsReceived')
                .with('projects')
                .with('organizations')
                .with('events')
                .with('userInspireds')
                // .with('projectInspireds')
                .with('organizationInspireds')
                // .with('eventInspireds')
                .with('postInspireds')
                .with('userInspirations')
                .with('projectPartners')
                .with('organizationPartners')
                .with('eventPartners')
                .with('interest_regions')
                .first();

            return {data, user};

        }

        catch (error) {
            return response.status(400).send({message: error.message})
        }

    }

    // MÉTODO PARA REGISTRAR UM NOVO USUÁRIO
    async signUp({auth, request, response}) {

        try {
            const body = request.only(['email', 'password', 'name']);
                  body.provider = 'SPARKEEP';

            const findUser = await User.findBy('email', body.email)

            if (findUser) return response.status(409).send({message: 'email já cadastrado'})

            const user = await User.create(body);

            const data = await auth.attempt(body.email, body.password);

            return {data, user}
        }
        catch (error) {
            return response.status(400).send({message: error.message})
        }
    }

    // MÉTODO PARA BUSCAR OS DADOS DO USUÁRIO LOGADO PELO TOKEN
    async account({ auth, response }) {
        try {
            const findUser = await auth.getUser();

            const user = await User.query().where('id', findUser.id).first();

            return user
        }

        catch (error) {
            return response.status(400).send({message: error.message})
        }
    }

    async forgotPassword({request, response}) {
        try {

            const email = request.only(['email'])

            const user = await User.findBy('email', email.email)

            sgMail.setApiKey(process.env.SENDGRID_API_KEY)

            // if (!user)
            //     response.status(400).send({message: 'Email não encontrado.'})

            const resetedInfo = await resetedPassword.create(email);

            const id = resetedInfo.id;
            const max = 6 - id.toString().length;
            let code = Math.random() * 1000000
            code = code.toString();
            code = code.substr(0, max);
            code = `${id}${code}`

            resetedInfo.code = code;

            await resetedInfo.save();

            // return {code, resetedInfo};

            const template = mailTemplate.forgotPassword(code, user.name);

            const msg = {
                to: email.email,
                from: 'sparkeepdesenvolvimento@gmail.com',
                subject: 'Recuperação de senha',
                text: 'Email teste para recuperar senha',
                html: template
              };


            await sgMail.send(msg);

            return {message: 'Email enviado com sucesso.'}
        }
        catch (error) {
            return response.status(400).send({message: error.message})
        }
    }

    async validateCode({request, params, response}) {
        const {code, email} = request.only(['code', 'email'])

        const requested = await resetedPassword.query()
            .where('code', code)
            .andWhere('email', email)
            .first();

        if (!requested)
            return response.status(404).send({message: 'Código e/ou email inválido'})

        const limitTime = moment(requested.created_at).add(10, 'minutes');

        if (moment() > limitTime)
            return response.status(401).send({message: 'Link expirado.'})

        return requested
    }

    async resetPassword({auth, request, params, response}) {

        const {password, id} = request.only(['email', 'password', 'id']);

        const requested = await resetedPassword.find(id);

        if (!requested)
            return response.status(404).send({message: 'Código e/ou email inválido'})

        if (requested.status == 0)
            return response.status(404).send({message: 'Código já utilizado'})

        const user = await User.query()
            .where('email', requested.email)
            .first()

        user.password = password
        await user.save()

        const data = await auth.attempt(user.email, password)

        requested.status = 0;
        await requested.save();

        return {data, user};
    }

    async refresh({ request, response, auth }) {

        var refresh_token = request.input("refresh_token");

        if (!refresh_token) {
            refresh_token = request.header("refresh_token");
        }

        const data = await auth
        .newRefreshToken()
        .generateForRefreshToken(refresh_token);

        const userAuth = await auth.getUser();

        const user = await User.query()
        .where('email', userAuth.email)
        .with('passions')
        .with('shortage')
        .with('posts')
        .with('notifications')
        .with('connectionsSend')
        .with('connectionsReceived')
        .with('projects')
        .with('organizations')
        .with('events')
        .with('userInspireds')
        .with('projectInspireds')
        .with('organizationInspireds')
        .with('eventInspireds')
        .with('postInspireds')
        .with('userInspirations')
        .with('organizationInspirations')
        .with('eventInspirations')
        .with('postInspirations')
        .with('projectPartners')
        .with('organizationPartners')
        .with('eventPartners')
        .first();

        return response.send({ data: data, user });
  }

}

module.exports = AuthController
