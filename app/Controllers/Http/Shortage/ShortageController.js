'use strict'

const Shortage = use('App/Models/Shortage')

class ShortageController {
    async index() {
        try {
            const shortages = await Shortage.query()
                .fetch();

            return shortages
        }
        catch (error) {
            return error
        }
    }

    async show({params}) {
        try {
            const shortage = await Shortage.query()
            .where('id', params.id)
            .first()

            return shortage
        }
        catch (error) {
            return error
        }
    }

    async store({request, response}) {
        try {
            const body = request.only([
                'description'
            ])

            let shortage = await Shortage.query()
                .where('description', body.description)
                .first();

            if (shortage)
                return response.status(400).send({message: 'Tipo de manutenção já cadastrada.'})

            shortage = await Shortage.create(body);

            return shortage;

        }
        catch (error) {
            return error
        }
    }

    async update({request, params, response}) {
        try {

        }
        catch (error) {
            return error
        }
    }

    async delete() {
        try {

        }
        catch (error) {
            return error
        }
    }
}

module.exports = ShortageController
