'use strict'

const Post = use('App/Models/Post');
const Event = use('App/Models/Event');
const moment = require('moment');
const Database = use('Database');

class EventController {

    async index({request, params}) {
        const id = params.id;

        const {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

        const time = moment().subtract(48, 'hours').format('YYYY-MM-DD ');

        const posts = await Post.query()
            .select(
                Database.raw(`*,
                    CASE
                        WHEN posts.created_at > '${time}' THEN 1
                        WHEN posts.created_at < '${time}' THEN 2
                    END as ord`
                )
            )
            .with('authorEvent')
            .with('images')
            .whereNot('exc', true)
            .withCount('inspireds as total_inspireds')
            .whereHas('authorEvent', builder => {
                builder.where('id', id)
            })
            // .orderByRaw( `
            //     (CASE WHEN ord = 1 THEN total_inspireds END) DESC, id DESC,
            //     (CASE WHEN ord = 2 THEN id END) DESC
            // ` )
            .paginate(page, perpage);

            for (let item of posts.rows) {
                item.authorName = item.$relations.authorEvent.name;
                item.authorImage = item.$relations.authorEvent.image_url
            }

        return posts
    }

}

module.exports = EventController;
