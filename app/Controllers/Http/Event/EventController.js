'use strict'

const Event = use('App/Models/Event');
const EventAdministrator = use('App/Models/EventsAdministrator');
const EventConfirmedPeople = use('App/Models/EventsConfirmedPerson');

const Notification = use('App/Models/Notification');
const NotificationReceiver = use('App/Models/NotificationsReceiver');

const moment = require('moment')
const Drive = use('Drive');
const sgMail = require('@sendgrid/mail');

class EventController {
    async index({request, response, params}) {
        try {
            const {page = 1, perpage = 8, keyword} = request.only(['page', 'perpage', 'keyword']);

            const events = await Event.query()
                .where(function() {
                    if (keyword) this.where('name', 'LIKE', `%${keyword}%`)
                })
                .whereNot('exc', true)
                .paginate(page, perpage);

            return events
        }
        catch (error) {
            return error
        }
    }

    async show({request, response, params}) {
        try {
            const event = await Event.query()
            .where('id', params.id)
            .whereNot('exc', true)
            .with('administrators', builder => {
                builder.where('status', 'accepted')
                builder.whereNot('exc', true)
                builder.select(
                    'id',
                    'email',
                    'name',
                    'description',
                    'image_url'
                )
            })
            .with('passions')
            .with('shortages')
            .with('maintenance_times')
            // .with('inspireds')
            // .with('inspirations')
            .first()

            // event.totalInspireds = event.$relations.inspireds.rows.length;
            // event.totalInspirations = event.$relations.inspirations.rows.length;

            return event
        }
        catch (error) {
            return error
        }
    }

    async store({request, response, params}) {
        try {
            const eventBody = request.only([
                'user_id',
                'organization_id',
                'project_id',
                'name',
                'date',
                'start_time',
                'end_time',
                'description',
                'local',
                'address_country',
                'address_state',
                'address_city',
                'address_neighborhood',
                'address_street',
                'address_complement',
                'address_number',
                'google_maps_url',
                'image_url',
                'telephone',
                'show_telephone',
                'cellphone',
                'show_cellphone',
                'frequency',
                'kind_of_help',
                'email',
                'youtube',
                'facebook',
                'instagram',
                'twitter',
                'site',
                'has_shortages'
            ]);
            const {
                administrators,
                passions,
                shortages,
                maintenance_times
            } = request.only([
                'administrators',
                'passions',
                'shortages',
                'maintenance_times'
            ]);
            let event = await Event.query()
                .where('name', eventBody.name)
                .first();

            // if (event)
            //     return response.status(400).send({message: 'Já existe um evento com este nome'})

            event = await Event.create(eventBody);

            await event.administrators().attach(administrators);
            await event.passions().detach();
            await event.passions().attach(passions);
            await event.shortages().detach();
            await event.shortages().attach(shortages);
            await event.maintenance_times().sync(maintenance_times)

            await event.load('maintenance_times');

            const notificationBody = {
                event_send_id: event.id,
                description: `O evento ${event.name} fez uma solicitação para você ser administrador.`,
                type: 'request',
            }

            const notification = await Notification.create(notificationBody);

            const notificationReceiverBody = [];

            for (let property of administrators) {
                notificationReceiverBody.push({
                    notification_id: notification.id,
                    user_receiver_id: property,
                    viewed: false
                })
            }

            await NotificationReceiver.createMany(notificationReceiverBody);

            if (eventBody.user_id) {
                await event.loadMany({
                    user: builder => {
                        builder.with('inspireds')
                        builder.select('id')
                    },
                    partnerSendToUser: builder => {
                        builder.with('userReceiver')
                        builder.select('id')
                    },
                    partnerReceivedFromUser: builder => {
                        builder.with('userSend')
                        builder.select('id')
                    }
                });

                const notificationBody = {
                    user_send_id: eventBody.user_id,
                    description: `O usuário ${event.$relations.user.name} criou um novo evento. Fique por dentro das informações deste evento.`
                };
                const notification = await Notification.create(notificationBody);
                const usersToReceive = [];
                for (let item of event.$relations.user.$relations.inspireds.rows) {
                    usersToReceive.push({
                        notification_id: notification.id,
                        user_receiver_id: item.user_inspired_id,
                        viewed: false
                    })
                };
                for (let item of event.$relations.partnerSendToUser.rows) {
                    usersToReceive.push({
                        notification_id: notification.id,
                        user_receiver_id: item.$relations.userReceiver.id,
                        viewed: false
                    })
                };
                for (let item of event.$relations.partnerReceivedFromUser.rows) {
                    usersToReceive.push({
                        notification_id: notification.id,
                        user_receiver_id: item.$relations.userSend.id,
                        viewed: false
                    })
                };

                await NotificationReceiver.createMany(usersToReceive);
            }
            else if (eventBody.organization_id) {
                await event.loadMany({
                    organization: builder => {
                        builder.with('inspireds')
                        builder.select('id')
                    }
                });
                const notificationBody = {
                    organization_send_id: eventBody.organization_id,
                    description: `A organização ${event.$relations.organization.name} criou um novo evento. Fique por dentro das informações deste evento.`
                }
                const notification = await Notification.create(notificationBody);
                for (let item of event.$relations.organization.$relations.inspireds.rows) {
                    await NotificationReceiver.create({
                        notification_id: notification.id,
                        user_receiver_id: item.id,
                        viewed: false
                    })
                }
            }
            return event
        }
        catch (error) {
            return error
        }
    }

    async update({request, response, params}) {
        try {
            const eventBody = request.only([
                'owner',
                'user_id',
                'organization_id',
                'project_id',
                'name',
                'date',
                'start_time',
                'end_time',
                'description',
                'local',
                'address_country',
                'address_state',
                'address_city',
                'address_neighborhood',
                'address_street',
                'address_complement',
                'address_number',
                'google_maps_url',
                'image_url',
                'telephone',
                'show_telephone',
                'cellphone',
                'show_cellphone',
                'frequency',
                'kind_of_help',
                'email',
                'youtube',
                'facebook',
                'instagram',
                'twitter',
                'site',
                'has_shortages'
            ]);
            const {
                administrators,
                passions,
                shortages,
                maintenance_times
            } = request.only([
                'administrators',
                'passions',
                'shortages',
                'maintenance_times'
            ]);

            const event = await Event.find(params.id);

            if (!event) return response.status(404).send({message: "Evento não encontrado"})

            const administratorsExistents = [];
            const administratorsToSend = [];
            for (let [index, item] of administrators.entries()) {
                const ev = await EventAdministrator.query()
                    .where('event_id', event.id)
                    .andWhere('user_id', item)
                    .first();
                if (ev && ev.status == 'rejected' && ev.updated_at > moment().subtract(30, 'days')) {
                    administratorsExistents.push(item)
                }
                if (ev && (ev.status == 'pending' || ev.status == 'accepted')) {
                    administratorsExistents.push(item)
                }
                if (ev && ev.status == 'rejected' && ev.updated_at < moment().subtract(30, 'days')) {
                    administratorsToSend.push(item)
                }
            }

            for (let item of administratorsExistents) {
                administrators.splice(administrators.indexOf(item), 1)
            }
            for (let item in administratorsToSend) {
                const ev = await EventAdministrator.query()
                    .where('event_id', event.id)
                    .andWhere('user_id', item)
                    .delete();
                await EventAdministrator.create({
                    event_id: event.id,
                    user_id: item
                })
            }
            const notificationBody = {
                event_send_id: event.id,
                description: `O evento ${event.name} fez alterações importantes em suas informações! Dê uma olhada para não ficar de fora de nenhuma novidade.`,
                type: 'message'
            };
            const notification = await Notification.create(notificationBody);

            const ecp = await EventConfirmedPeople.query()
                .select('id', 'user_id')
                .where('event_id', event.id)
                .with('user')
                .fetch()

            for (let item of ecp.rows) {
                sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                await NotificationReceiver.create({
                    notification_id: notification.id,
                    user_receiver_id: item.user_id,
                    viewed: false
                });
                const msg = {
                    to: item.$relations.user.email,
                    from: 'sparkeepdesenvolvimento@gmail.com',
                    subject: `Atualizações do evento ${event.name}`,
                    text: `O evento ${event.name} fez alterações importantes em suas informações! Dê uma olhada para não ficar de fora de nenhuma novidade.`,
                    html: `O evento ${event.name} fez alterações importantes em suas informações! Dê uma olhada para não ficar de fora de nenhuma novidade.`
                };
                await sgMail.send(msg);
            }

            await event.administrators().attach(administrators);
            await event.passions().detach();
            await event.passions().attach(passions);
            await event.shortages().detach();
            await event.shortages().attach(shortages);
            await event.maintenance_times().sync(maintenance_times)
            await event.merge(eventBody);
            await event.save();

            await event.load('maintenance_times');

            const notificationRequestBody = {
                event_send_id: event.id,
                description: `O evento ${event.name} fez uma solicitação para você ser administrador.`,
                type: 'request',
            }

            const notificationRequest = await Notification.create(notificationRequestBody);
            const notificationReceiverBody = [];
            for (let property of administrators) {
                notificationReceiverBody.push({
                    notification_id: notificationRequest.id,
                    user_receiver_id: property,
                    viewed: false
                })
            };
            await NotificationReceiver.createMany(notificationReceiverBody);

            return event;
        }
        catch (error) {
            return error;
        }
    }

    async destroy({auth, request, response, params}) {
        try {
            const user = await auth.getUser();

            const event = await Event.find(params.id);

            if (event.user_id && user.id != event.user_id) return response.status(400).send({ message: "Você não tem permissão para excluir este evento." });

            await event.merge({
                name: null,
                date: null,
                start_time: null,
                end_time: null,
                description: null,
                local: null,
                address_country: null,
                address_state: null,
                address_city: null,
                address_neighborhood: null,
                address_street: null,
                address_complement: null,
                address_number: null,
                google_maps_url: null,
                image_url: null,
                telephone: null,
                show_telephone: null,
                frequency: null,
                kind_of_help: null,
                email: null,
                youtube: null,
                facebook: null,
                instagram: null,
                twitter: null,
                site: null
            })

            await event.save();

            return event
        }
        catch (error) {
            return error
        }
    }

    async uploadAttachment({request, response, params}) {

        try {
            const event = await Event.find(params.id);

            const validationOptions = {
                types: ['jpeg', 'jpg', 'png'],
                size: '5mb'
            }

            request.multipart.file('attachment', validationOptions, async file => {
                // Set file size from stream byteCount, so adonis can validate file size
                file.size = file.stream.byteCount
                // Catches validation errors, if any and then throw exception
                const error = file.error()

                if (error.message) throw new Error(error.message)

                const Key =`event_images/${(Math.random() * 100).toString()}-${file.clientName}`
                const ContentType = file.headers['content-type']
                const ACL = 'public-read'
                // upload file to s3
                const path = await Drive.put(Key, file.stream, { ContentType, ACL })

                await event.merge({image_url: path});
                await event.save();
            })

            await request.multipart.process()

            return event
        }
        catch (error) {
            return error
        }
    }

    async inspire({request, response, auth}) {
        try {
            const user = await auth.getUser();

            const body = request.only(['event_inspiration_id']);
                  body.user_inspired_id = user.id;

            const inspired = await Inspired.create(body);

            return inspired
        }
        catch (error) {
            return response.status(500).send({message: "Ocorreu um erro inesperado. Tente novamente mais tarde"})
        }
    }
}

module.exports = EventController;
