'use strict'

const ConfirmedPerson = use('App/Models/EventsConfirmedPerson');

class EventsConfirmedPerson {
    
    async index({request, response, params}) {
        try {
            const {page, perpage} = request.only(['page', 'perpage']);

            const confirmedPersons = await ConfirmedPerson.query()
                .where('event_id', params.id)
                .paginate(page, perpage);
    
            return confirmedPersons
        }
        catch(error) {
            return response.status(500).send({message: error.message})
        }
    }

    async show() {
    
    }

    async create({request, response, params, auth}) {
        try {
            const user = await auth.getUser();

            const body = {
                event_id: params.id,
                user_id: user.id
            }

            let confirmedPerson = await ConfirmedPerson.query()
                .where('user_id', body.user_id)
                .andWhere('event_id', body.event_id)
                .first();

            if (confirmedPerson) {
                await confirmedPerson.delete();

                return response.status(200).send({
                    message: 'Confirmação em evento removida com sucesso',
                    status: false
                })
            }
            else {
                confirmedPerson = await ConfirmedPerson.create(body);
    
                return {
                    confirmedPerson,
                    status: true
                }
            }
        }
        catch(error) {
            return response.status(500).send({message: error.message})
        }
    }

    async update() {

    }

    async delete(params) {
        try {
            const confirmedPerson = await ConfirmedPersons.query()
                .where('id', params.id)
                .first();

            await confirmedPerson.delete();

            return response.status(200).send({message: 'Usuário excluído com sucesso'})
        }
        catch(error) {
            return response.status(500).send({message: error.message})
        }
    }
}

module.exports = EventsConfirmedPerson