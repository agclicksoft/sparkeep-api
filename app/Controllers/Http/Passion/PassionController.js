'use strict'

const Passion = use('App/Models/Passion')

class PassionController {
    async index() {
        try {
            const passions = await Passion.query()
            .orderBy('order', 'asc')
            .fetch();

            return passions

        }
        catch (error) {
            return error
        }
    }

    async show() {
        try {

        }
        catch (error) {
            return error
        }
    }

    async store({request, response}) {
        try {

            const passionBody = request.only([
                'name',
                'description'
            ])

            const findPassion = await Passion.query()
                .where('name', passionBody.name)
                .first()

            if (findPassion)
                return response.status(400).send({message: "Paixão já cadastrada"});

            const passion = await Passion.create(passionBody);

            return passion;

        }
        catch (error) {
            return error
        }
    }

    async update() {
        try {

        }
        catch (error) {
            return error
        }
    }

    async delete() {
        try {

        }
        catch (error) {
            return error
        }
    }
}

module.exports = PassionController
