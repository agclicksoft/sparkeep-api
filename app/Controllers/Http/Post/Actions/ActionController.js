'use strict'

const moment = require('moment')

const Post = use('App/Models/Post')
const Inspired = use('App/Models/Inspired')
const Notification = use('App/Models/Notification')

const mailTemplate = require('../../../../../mail-templates/report-post')
const sgMail = require('@sendgrid/mail');

class ActionController {

    async like({request, response, params, auth}) {
        try {
            const post = await Post.query()
                .where('id', params.id)
                .with('inspireds')
                .first();

            const user = await auth.getUser();

            const liked = await Inspired.query()
                .where('post_inspiration_id', post.id)
                .andWhere('user_inspired_id', user.id)
                .first()

            if (!liked) {
                await Inspired.create({
                    post_inspiration_id: post.id,
                    user_inspired_id: user.id
                })

                var notificationSend = await Notification.create({
                    user_send_id: user.id,
                    post_id: post.id,
                    description: `${user.name} deixou um clap em sua publicação`,
                    type: 'post'
                })

                var receiversIds = [post.user_id];

                await notificationSend.user_receiver().attach(receiversIds)

                return response.status(200).send({message: "like"})
            }
            else {
                await liked.delete();

                return response.status(200).send({message: "unlike"})
            }
        }
        catch (error) {
            return error
        }
    }

    async report({request, response, params, auth}) {
        try {

            const { comment } = request.only(['comment'])

            const post = await Post.query()
                .where('id', params.id)
                .with('authorUser')
                .first();

            const user = await auth.getUser();

            //TODO: SET REPORT TEMPLATE
            const template = mailTemplate.report(post, user, comment);

            sgMail.setApiKey(process.env.SENDGRID_API_KEY)

            //TODO: ALTERAR EMAIL DE RECEBIMENTO
            const msg = {
                to: 'yurifrascino@gmail.com',
                // to: 'alan.neres@clicksoft.com.br',
                from: 'sparkeepdesenvolvimento@gmail.com',
                subject: 'Post denunciado',
                text: 'Há um post recebendo uma denúncia. Por favor, verifique !',
                html: template,
              };

            sgMail.send(msg);

            return response.status(200).send({message: "Email enviado com sucesso"})


        }
        catch (error) {
            return error
        }
    }


}

module.exports = ActionController

