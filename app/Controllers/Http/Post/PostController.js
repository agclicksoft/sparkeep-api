'use strict'

const moment = require('moment')
const Database = use('Database');

const Drive = use('Drive')
const Post = use('App/Models/Post')
const User = use('App/Models/User')
const Event = use('App/Models/Event')
const Organization = use('App/Models/Organization')
const Project = use('App/Models/Project')
const Inspired = use('App/Models/Inspired')

class PostController {
    async index({request, response, params, auth}) {
        try {

            let {page = 1, perpage = 8} = request.only(['page', 'perpage'])

            const user = await auth.getUser();

            const time = moment().subtract(48, 'hours').format('YYYY-MM-DD ');

            const posts = await Post.query()
            .select(
                Database.raw(`*,
                    CASE
                        WHEN posts.created_at > '${time}' THEN 1
                        WHEN posts.created_at < '${time}' THEN 2
                    END as ord`
                )
            )
            .with('user')
            .with('authorUser')
            .with('authorEvent')
            .with('authorOrganization')
            .with('authorProject')
            .with('images')
            .whereNot('exc', true)
            .withCount('inspireds as total_inspireds')
            // .orderByRaw( `
            //     (CASE WHEN ord = 1 THEN total_inspireds END) DESC, id DESC,
            //     (CASE WHEN ord = 2 THEN id END) DESC
            // ` )
            .paginate(page, perpage);

            for (let item of posts.rows) {
                if (item.$relations.authorUser) {
                    item.authorName = item.$relations.authorUser.name;
                    item.authorImage = item.$relations.authorUser.image_url
                }
                else if (item.$relations.authorEvent) {
                    item.authorName = item.$relations.authorEvent.name;
                    item.authorImage = item.$relations.authorEvent.image_url
                }
                else if (item.$relations.authorProject) {
                    item.authorName = item.$relations.authorProject.name;
                    item.authorImage = item.$relations.authorProject.image_url
                }
                else if (item.$relations.authorOrganization) {
                    item.authorName = item.$relations.authorOrganization.name;
                    item.authorImage = item.$relations.authorOrganization.image_url
                }
            }

            //VERIFICA SE O USUÁRIO QUE ESTÁ FAZENDO A REQUISIÇÃO CURTIU OU NÃO O POST EXIBIDO
            for (let item of posts.rows) {
                const liked = await Inspired.query()
                    .where('post_inspiration_id', item.id)
                    .andWhere('user_inspired_id', user.id)
                    .first()

                liked ? item.liked = true : item.liked = false;
            }

            return posts;
        }
        catch (error) {
            return error;
        }
    }

    async show({params}) {
        try {
            const post = await Post.query()
                .where('id', params.id)
                .with('authorUser')
                .with('authorEvent')
                .with('authorOrganization')
                .with('authorProject')
                .with('passions')
                .with('images')
                .whereNot('exc', true)
                .withCount('inspireds as total_inspireds')
                .first()

            if (post.$relations.authorUser) {
                post.authorName = post.$relations.authorUser.name;
                post.authorImage = post.$relations.authorUser.image_url
            }
            else if (post.$relations.authorEvent) {
                post.authorName = post.$relations.authorEvent.name;
                post.authorImage = post.$relations.authorEvent.image_url
            }
            else if (post.$relations.authorProject) {
                post.authorName = post.$relations.authorProject.name;
                post.authorImage = post.$relations.authorProject.image_url
            }
            else if (post.$relations.authorOrganization) {
                post.authorName = post.$relations.authorOrganization.name;
                post.authorImage = post.$relations.authorOrganization.image_url
            }

            return post
        }
        catch (error) {
            return error
        }
    }

    async store({request, response}) {
        try {
            const body = request.only([
                'user_id',
                'author',
                'author_user_id',
                'author_event_id',
                'author_organization_id',
                'author_project_id',
                'description',
                'privacy',
            ])
            //AUTHOR = 'authorUser' OU 'authorEvent' OU 'authorOrganization' OU 'authorProject'
            const author = body.author[0].toUpperCase() + body.author.substr(1);

            const id = body[`author_${body.author}_id`]

            const passionsBody = request.only(['passions'])['passions'];

            //PEGA OS ULTIMOS 3 POSTS DESTE AUTHOR, PARA VERIFICAR O LIMITE DE 3 POSTS POR DIA.
            const lastPost = await Post.query()
                .with(`author${author}`)
                .whereHas(`author${author}`, builder => {
                    builder.where('id', id )
                })
                .pickInverse(3)

            var limitReached = false;
            if (lastPost.rows.length === 3) {
                const pub1 = moment(lastPost.rows[0].created_at).format('DD/MM/yyyy')
                const pub2 = moment(lastPost.rows[1].created_at).format('DD/MM/yyyy')
                const pub3 = moment(lastPost.rows[2].created_at).format('DD/MM/yyyy')
                const today = moment().format('DD/MM/yyyy')

                if (pub1 === pub2 && pub2 === pub3 && pub3 === today )
                    limitReached = true;
            }

            // SE OS 3 ULTIMOS POSTS TIVEREM A MESMA DATA, RETORNA ESTE ERRO.
            // if (limitReached)
            //     return response.status(401).send({message: 'Permitido apenas 03 publicações por dia'})


            'ok'.charAt(1).toUpperCase();

            const relation = `author${ body.author.charAt(0).toUpperCase() + body.author.substr(1)}`

            const post = await Post.create(body);
                await post.loadMany(['user', relation]);


            if (passionsBody) await post.passions().attach(passionsBody);

            if (body.author == 'user') {
                const id = post.author_user_id
                const user = await User.find(id)
                var notificationSend = await post.userNotifications().create({
                    user_send_id: id,
                    post_id: post.id,
                    description: `${user.name} fez uma nova postagem`,
                    type: 'post'
                })

                var receiversIds = [];
                const userConnections = await User.query()
                    .where('id', id)
                    .with('connectionsSend', builder => {
                        builder.where('status', '1')
                        builder.andWhere('user_send_id', id)
                    })
                    .with('connectionsReceived', builder => {
                        builder.where('status', '1')
                        builder.andWhere('user_receiver_id', id)
                    })
                    .with('userInspireds', builder => {
                        builder.where('user_inspiration_id', id)
                        builder.whereNotNull('user_inspired_id')
                    })
                    .first();

                    userConnections.$relations.connectionsSend.rows.map( el => {
                        receiversIds.push(el.user_receiver_id)
                    })

                    userConnections.$relations.connectionsReceived.rows.map( el => {
                        receiversIds.push(el.user_send_id)
                    })

                    userConnections.$relations.userInspireds.rows.map( el => {
                        receiversIds.push(el.id)
                    })

                    var userReceivedsNotification = await notificationSend.user_receiver().attach(receiversIds)
            }

            if (body.author == 'event') {
                const id = post.author_event_id;
                const event = await Event.find(id);

                var notificationSend = await post.eventNotifications().create({
                    event_send_id: id,
                    post_id: post.id,
                    description: `${event.name} fez uma nova postagem`,
                    type: 'post'
                })

                var receiversIds = [];
                const eventReceivers = await Event.query()
                    .where('id', id)
                    .with('confirmedPeople')
                    .first();

                // ADICIONANDO OS IDS DE TODOS QUE SE INSPIRARAM NESTE EVENTO PARA O ARRAY DE RECEBEDORES DA NOTIFICAÇÃO
                eventReceivers.$relations.confirmedPeople.rows.map( el => {
                    receiversIds.push(el.id)
                })

                // ADICIONANDO A TABELA DE RELACIONAMENTO OS RECEBEDORES DA NOTIFICAÇÃO E A PRÓPRIA NOTIFICAÇÃO
                var userReceivedsNotification = await notificationSend.user_receiver().attach(receiversIds)

                // NOTIFICANDO O DONO DO EVENTO, CASO QUEM TENHA FEITA A PUBLICAÇÃO SEJA UM ADMINISTRADOR
                if (post.user_id != event.user_id) {
                    var notificationToEventOwner = await post.eventNotifications().create({
                        post_id: post.id,
                        description: `O administrador ${post.$relations.user.name} fez uma postagem pelo o seu evento ${event.name}.`,
                        type: 'post'
                    })

                    await notificationToEventOwner.user_receiver().attach([event.user_id]);
                }
            }

            if (body.author == 'project') {
                const id = post.author_project_id
                const project = await Project.find(id)

                var notificationSend = await post.projectNotifications().create({
                    project_send_id: id,
                    post_id: post.id,
                    description: `${project.name} fez uma nova postagem`,
                    type: 'post'
                })

                var receiversIds = [];
                const projectInspireds = await Project.query()
                    .where('id', id)
                    .with('partnerSendToUser', builder => {
                        builder.wherePivot('status', '1')
                    })
                    .with('partnerReceivedFromUser', builder => {
                        builder.wherePivot('status', '1')
                    })
                    .first();

                // ADICIONANDO OS IDS DE TODOS QUE SE INSPIRARAM NESTE PROJETO PARA O ARRAY DE RECEBEDORES DA NOTIFICAÇÃO
                projectInspireds.$relations.partnerSendToUser.rows.map( el => {
                    receiversIds.push(el.id)
                })
                projectInspireds.$relations.partnerReceivedFromUser.rows.map( el => {
                    receiversIds.push(el.id)
                })

                // ADICIONANDO A TABELA DE RELACIONAMENTO OS RECEBEDORES DA NOTIFICAÇÃO E A PRÓPRIA NOTIFICAÇÃO
                var userReceivedsNotification = await notificationSend.user_receiver().attach(receiversIds)

                // NOTIFICANDO O DONO DO PROJETO, CASO QUEM TENHA FEITA A PUBLICAÇÃO SEJA UM ADMINISTRADOR
                if (post.user_id != project.user_id) {

                    var notificationToProjectOwner = await post.projectNotifications().create({
                        post_id: post.id,
                        description: `O administrador ${post.$relations.user.name} fez uma postagem pelo o seu projeto ${project.name}.`,
                        type: 'post'
                    })

                    await notificationToProjectOwner.user_receiver().attach([project.user_id]);
                }
            }

            if (body.author == 'organization') {
                const id = post.author_organization_id
                const organization = await Organization.find(id)
                var notificationSend = await post.organizationNotifications().create({
                    organization_send_id: id,
                    post_id: post.id,
                    description: `${organization.name} fez uma nova postagem`,
                    type: 'post'
                })

                var receiversIds = [];
                const organizationInspireds = await Organization.query()
                    .where('id', id)
                    .with('inspireds', builder => {
                        builder.where('organization_inspiration_id', id)
                        builder.whereNotNull('user_inspired_id')
                    })
                    .with('partnerSendToUser', builder => {
                        builder.wherePivot('status', '1')
                    })
                    .with('partnerReceivedFromUser', builder => {
                        builder.wherePivot('status', '1')
                    })
                    .first();

                // ADICIONANDO OS IDS DE TODOS QUE SE INSPIRARAM NESTA ORGANIZAÇÃO PARA O ARRAY DE RECEBEDORES DA NOTIFICAÇÃO
                organizationInspireds.$relations.inspireds.rows.map( el => {
                    receiversIds.push(el.user_inspired_id)
                })
                organizationInspireds.$relations.partnerSendToUser.rows.map( el => {
                    receiversIds.push(el.id)
                })
                organizationInspireds.$relations.partnerReceivedFromUser.rows.map( el => {
                    receiversIds.push(el.id)
                })

                // ADICIONANDO A TABELA DE RELACIONAMENTO OS RECEBEDORES DA NOTIFICAÇÃO E A PRÓPRIA NOTIFICAÇÃO
                var userReceivedsNotification = await notificationSend.user_receiver().attach(receiversIds)

                // NOTIFICANDO O DONO DA ORGANIZAÇÃO, CASO QUEM TENHA FEITA A PUBLICAÇÃO SEJA UM ADMINISTRADOR
                if (post.user_id != organization.user_id) {
                    var notificationToOrganizationOwner = await post.organizationNotifications().create({
                        post_id: post.id,
                        description: `O administrador ${post.$relations.user.name} fez uma postagem pela a sua organização ${organization.name}.`,
                        type: 'post'
                    })

                    await notificationToOrganizationOwner.user_receiver().attach([organization.user_id]);
                }
            }

            if (post.$relations.authorUser) {
                post.authorName = post.$relations.authorUser.name;
                post.authorImage = post.$relations.authorUser.image_url
            }
            else if (post.$relations.authorEvent) {
                post.authorName = post.$relations.authorEvent.name;
                post.authorImage = post.$relations.authorEvent.image_url
            }
            else if (post.$relations.authorProject) {
                post.authorName = post.$relations.authorProject.name;
                post.authorImage = post.$relations.authorProject.image_url
            }
            else if (post.$relations.authorOrganization) {
                post.authorName = post.$relations.authorOrganization.name;
                post.authorImage = post.$relations.authorOrganization.image_url
            }

            return {post, notificationSend, userReceivedsNotification}

        }
        catch (error) {
            return error.message
        }
    }

    async update({request, params}) {
        try {
            const post = await Post.query()
            .where('id', params.id)
            .with('passions')
            .with('user')
            .with('authorUser')
            .with('authorEvent')
            .with('authorOrganization')
            .with('authorProject')
            .with('images')
            .withCount('inspireds as total_inspireds')
            .first();

            const body = request.only([
                'user_id',
                'author',
                'author_user_id',
                'author_event_id',
                'author_organization_id',
                'author_project_id',
                'description',
                'privacy',
            ])

            const passionsBody = request.only(['passions'])['passions']

            const inspiredsBody = request.only(['inspireds'])['inspireds'];

            const inspirationsBody = request.only(['inspirations'])['inspirations'];

            if (passionsBody && passionsBody.length > 0) {
                await post.passions().detach();
                await post.passions().attach(passionsBody);
            }

            await post.merge(body);
            await post.save();

            return post
        }
        catch (error) {
            return error
        }
    }

    async destroy({params, response}) {
        try {
            const post = await Post.find(params.id);

            if (!post)
                return response.status(404).send({message: 'Nenhum post encontrado'})

            await post.merge({exc: true});
            await post.save()

            return response.status(200).send({message: 'Post excluído.'})
        }
        catch (error) {
            return error
        }
    }

    async uploadFile({ request, response, params }){
        const post = await Post.find(params.id)

            try {
              const validationOptions = {
                types: ['jpeg', 'jpg', 'png', 'pdf'],
                size: '5mb'
              }

              request.multipart.file('attachments', validationOptions, async file => {
                // set file size from stream byteCount, so adonis can validate file size
                file.size = file.stream.byteCount

                // catches validation errors, if any and then throw exception
                const error = file.error()

                if (error.message)
                  throw new Error(error.message)

                const Key =`uploads/${(Math.random() * 100).toString()}-${file.clientName}`
                const ContentType = file.headers['content-type']
                const ACL = 'public-read'
                // upload file to s3
                const path = await Drive.put(Key, file.stream, {
                  ContentType,
                  ACL
                })

                await post.images().create({
                    name: file.clientName,
                    path,
                    key: Key
                })
              })

              await request.multipart.process()

              await post.load('images')

              return response.status(201).send({data : post})

            }
            catch (err) {
              return response.status(err.status).send({ error: err.message })
            }


    }
}

module.exports = PostController
