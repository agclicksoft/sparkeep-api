'use strict'

const Post = use('App/Models/Post')
const Inspired = use('App/Models/Inspired')
const moment = require('moment')
const Database = use('Database');

class PostController {

    async index({auth, request}) {
        try {
            //BUSCANDO PAGINA, ITEMS POR PAGINA E PERFIL A SER EXIBINDO
            const {page = 1, perpage = 8, profile} = request.only(['page', 'paginate', 'profile'])

            //BUSCANDO USUÁRIO LOGADO PELO TOKEN
            let user = await auth.getUser();

            await user.loadMany([
                'passions',
                'connectionsSend',
                'connectionsReceived',
                'userInspirations',
                // 'eventInspirations',
                // 'organizationInspirations',
                'inspirations'
            ])

            const userIds = [];
            const projectIds = [];
            const eventIds = [];
            const organizationIds = [];

            // BUSCANDO TODAS AS CONEXÕES ONDE O USUÁRIO ENVIOU A SOLICITAÇÃO
            for (let property of user.$relations.connectionsSend.rows)
                userIds.push(property.user_receiver_id)

            // BUSCANDO TODAS AS CONEXÕES ONDE O USUÁRIO RECEBEU A SOLICITAÇÃO
            for (let property of user.$relations.connectionsReceived.rows)
                userIds.push(property.user_send_id)

            // BUSCANDO TODAS AS CONEXÕES ONDE O USUÁRIO RECEBEU A SOLICITAÇÃO
            for (let property of user.$relations.inspirations.rows) {
                if (property.user_inspiration_id) userIds.push(property.user_inspiration_id)
                else if (property.event_inspiration_id) eventIds.push(property.event_inspiration_id)
                else if (property.project_inspiration_id) projectIds.push(property.project_inspiration_id)
                else if (property.organization_inspiration_id) organizationIds.push(property.organization_inspiration_id)
            }

            const time = moment().subtract(48, 'hours').format('YYYY-MM-DD ');

            const posts = await Post.query()
            .select(
                Database.raw(`*,
                    CASE
                        WHEN posts.created_at > '${time}' THEN 1
                        WHEN posts.created_at < '${time}' THEN 2
                    END as ord`
                )
            )
            .where(function() {

                if (userIds.length == 0) userIds.push(0)
                for (let property of userIds) this.orWhere('author_user_id', property)

                if (projectIds.length == 0) projectIds.push(0)
                for (let property of projectIds) this.orWhere('author_project_id', property)

                if (eventIds.length == 0) eventIds.push(0)
                for (let property of eventIds) this.orWhere('author_event_id', property)

                if (organizationIds.length == 0) organizationIds.push(0)
                for (let property of organizationIds) this.orWhere('author_organization_id', property)
            })
            .andWhere(function() {
                if (profile === 'user') this.whereHas('authorUser')

                else if (profile === 'event')this.whereHas('authorEvent')

                else if (profile === 'project') this.whereHas('authorProject')

                else if (profile === 'organization') this.whereHas('authorOrganization')
            })
            .with('authorUser', builder => {
                builder.select(
                    'id',
                    'name',
                    'email',
                    'description',
                    'image_url'
                )
            })
            .with('authorEvent', builder => {
                builder.select(
                    'id',
                    'name',
                    'email',
                    'description',
                    'image_url'
                )
            })
            .with('authorProject', builder => {
                builder.select(
                    'id',
                    'name',
                    'email',
                    'description',
                    'image_url'
                )
            })
            .with('authorOrganization', builder => {
                builder.select(
                    'id',
                    'name',
                    'email',
                    'description',
                    'image_url'
                )
            })
            .with('passions')
            .with('images')
            .whereNot('exc', true)
            .withCount('inspireds as total_inspireds')
            // .orderByRaw( `
            //     (CASE WHEN ord = 1 THEN total_inspireds END) DESC, id DESC,
            //     (CASE WHEN ord = 2 THEN id END) DESC
            // ` )
            .paginate(page, perpage)

            for (let item of posts.rows) {
                if (item.$relations.authorUser) {
                    item.authorName = item.$relations.authorUser.name;
                    item.authorImage = item.$relations.authorUser.image_url
                }
                else if (item.$relations.authorEvent) {
                    item.authorName = item.$relations.authorEvent.name;
                    item.authorImage = item.$relations.authorEvent.image_url
                }
                else if (item.$relations.authorProject) {
                    item.authorName = item.$relations.authorProject.name;
                    item.authorImage = item.$relations.authorProject.image_url
                }
                else if (item.$relations.authorOrganization) {
                    item.authorName = item.$relations.authorOrganization.name;
                    item.authorImage = item.$relations.authorOrganization.image_url
                }
            }

            for (let item of posts.rows) {
                const liked = await Inspired.query()
                    .where('post_inspiration_id', item.id)
                    .andWhere('user_inspired_id', user.id)
                    .first()

                liked ? item.liked = true : item.liked = false;
            }

            return posts
        }
        catch(error) {
            return error.message
        }
    }

}

module.exports = PostController;
