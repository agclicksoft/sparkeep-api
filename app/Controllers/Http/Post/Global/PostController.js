'use strict'

const Post = use('App/Models/Post')
const User = use('App/Models/User')
const Inspired = use('App/Models/Inspired')
const moment = require('moment');
const Database = use('Database');

class PostController {

    async index({auth, request, response}) {
        try {
            const {profile} = request.only(['profile']);
            const {page = 1, perpage = 8} = request.only(['page', 'perpage'])

            const time = moment().subtract(12, 'hours').format('YYYY-MM-DD ');

            let user = await auth.getUser();

            user = await User.query()
                .where('id', user.id)
                .with('passions')
                .first()

            const posts = await Post.query()
                .select(
                    Database.raw(`*,
                        CASE
                            WHEN posts.created_at > '${time}' THEN 1
                            WHEN posts.created_at < '${time}' THEN 2
                        END as ord`
                    )
                )
                .where(function() {
                    if (profile === 'user') this.whereHas('authorUser')
                    else if (profile === 'event')this.whereHas('authorEvent')
                    else if (profile === 'project') this.whereHas('authorProject')
                    else if (profile === 'organization') this.whereHas('authorOrganization')
                })
                .andWhere(function() {
                    this.where('privacy', 'public')
                    this.orWhereHas('authorUser', builder => {
                        builder.whereHas('userInspireds', builder => {
                            builder.where('user_inspired_id', user.id)
                        })
                    })
                })
                .whereNot('exc', true)
                .with('authorUser', builder => {
                    builder.select(
                        'id',
                        'name',
                        'email',
                        'description',
                        'image_url'
                    )
                })
                .with('authorEvent', builder => {
                    builder.select(
                        'id',
                        'name',
                        'email',
                        'description',
                        'image_url'
                    )
                })
                .with('authorProject', builder => {
                    builder.select(
                        'id',
                        'name',
                        'email',
                        'description',
                        'image_url'
                    )
                })
                .with('authorOrganization', builder => {
                    builder.select(
                        'id',
                        'name',
                        'email',
                        'description',
                        'image_url'
                    )
                })
                .with('passions')
                .with('images')
                .withCount('inspireds as total_inspireds')
                .orderByRaw( `
                    (CASE WHEN ord = 1 THEN total_inspireds END) DESC, id DESC,
                    (CASE WHEN ord = 2 THEN id END) DESC
                ` )
                .paginate(page, perpage)


            for (let item of posts.rows) {
                if (item.$relations.authorUser) {
                    item.authorName = item.$relations.authorUser.name;
                    item.authorImage = item.$relations.authorUser.image_url
                }
                else if (item.$relations.authorEvent) {
                    item.authorName = item.$relations.authorEvent.name;
                    item.authorImage = item.$relations.authorEvent.image_url
                }
                else if (item.$relations.authorProject) {
                    item.authorName = item.$relations.authorProject.name;
                    item.authorImage = item.$relations.authorProject.image_url
                }
                else if (item.$relations.authorOrganization) {
                    item.authorName = item.$relations.authorOrganization.name;
                    item.authorImage = item.$relations.authorOrganization.image_url
                }
            }

            for (let item of posts.rows) {

                const liked = await Inspired.query()
                    .where('post_inspiration_id', item.id)
                    .andWhere('user_inspired_id', user.id)
                    .first()

                liked ? item.liked = true : item.liked = false;
            }

            return posts
        }
        catch(error) {
            return response.status(500).send({message: error.message});
        }
    }

}

module.exports = PostController;
