'use strict'

const moment = require('moment')
const Database = use('Database');

const Post = use('App/Models/Post')
const User = use('App/Models/User')
const Inspired = use('App/Models/Inspired')


class PostController {

    async index({auth, request, response}) {
        try {
            const {profile} = request.only(['profile']);
            const {page = 1, perpage = 8} = request.only(['page', 'paginate'])

            let user = await auth.getUser();

            user = await User.query()
                .where('id', user.id)
                .with('passions')
                .with('interest_regions')
                .first()

            if (user.$relations.passions.rows.length == 0 && user.$relations.interest_regions.rows.length == 0)
                return response.status(400).send({message: 'Usuário não possui nenhuma paixão e/ou região de interesse.'})

            const time = moment().subtract(48, 'hours').format('YYYY-MM-DD ');


            const posts = await Post.query()
            .select(
                Database.raw(`*,
                    CASE
                        WHEN posts.created_at > '${time}' THEN 1
                        WHEN posts.created_at < '${time}' THEN 2
                    END as ord`
                )
            )
            .where(function() {
                if (profile === 'user')
                    this.whereHas('authorUser')

                if (profile === 'event')
                    this.whereHas('authorEvent')

                if (profile === 'project')
                    this.whereHas('authorProject')

                if (profile === 'organization')
                    this.whereHas('authorOrganization')
            })
            .andWhere(function() {
                this.whereHas('passions', builder => {
                    builder.where(function() {
                        for (let property of user.$relations.passions.rows) {
                            this.orWhere('passions.id', property.id)
                        }
                    })
                })
                this.orWhere(function() {
                    this.whereHas('authorUser', builder => {
                        builder.where(function() {
                            for (let property of user.$relations.interest_regions.rows) {
                                this.orWhere('address_state', property.interest_region_state)
                                this.orWhere('address_city', property.interest_region_city)
                            }
                        })
                    })
                    this.orWhereHas('authorEvent', builder => {
                        builder.where(function() {
                            for (let property of user.$relations.interest_regions.rows) {
                                this.orWhere('address_state', property.interest_region_state)
                                this.orWhere('address_city', property.interest_region_city)
                            }
                        })
                    })
                    this.orWhereHas('authorProject', builder => {
                        builder.where(function() {
                            for (let property of user.$relations.interest_regions.rows) {
                                this.orWhere('address_state', property.interest_region_state)
                                this.orWhere('address_city', property.interest_region_city)
                            }
                        })
                    })
                    this.orWhereHas('authorOrganization', builder => {
                        builder.where(function() {
                            for (let property of user.$relations.interest_regions.rows) {
                                this.orWhere('address_state', property.interest_region_state)
                                this.orWhere('address_city', property.interest_region_city)
                            }
                        })
                    })

                    //  this.whereHas('interest_regions', builder => {
                    //         builder.where(function() {
                    //             for (let property of user.$relations.interest_regions.rows) {
                    //                 this.orWhere('interest_region_city', property.interest_region_city)
                    //                 this.orWhere('interest_region_state', property.interest_region_state)
                    //             }
                    //         })
                    //     })
                })
            })
            .whereNot('exc', true)
            .with('authorUser', builder => {
                builder.select(
                    'id',
                    'name',
                    'email',
                    'description',
                    'image_url'
                )
            })
            .with('authorEvent', builder => {
                builder.select(
                    'id',
                    'name',
                    'email',
                    'description',
                    'image_url'
                )
            })
            .with('authorProject', builder => {
                builder.select(
                    'id',
                    'name',
                    'email',
                    'description',
                    'image_url'
                )
            })
            .with('authorOrganization', builder => {
                builder.select(
                    'id',
                    'name',
                    'email',
                    'description',
                    'image_url'
                )
            })
            .with('passions')
            .with('images')
            .withCount('inspireds as total_inspireds')
            .with('user')
            // .orderByRaw( `
            //     (CASE WHEN ord = 1 THEN total_inspireds END) DESC, id DESC,
            //     (CASE WHEN ord = 2 THEN id END) DESC
            // ` )
            .paginate(page, perpage)

            for (let item of posts.rows) {
                if (item.$relations.authorUser) {
                    item.authorName = item.$relations.authorUser.name;
                    item.authorImage = item.$relations.authorUser.image_url
                }
                else if (item.$relations.authorEvent) {
                    item.authorName = item.$relations.authorEvent.name;
                    item.authorImage = item.$relations.authorEvent.image_url
                }
                else if (item.$relations.authorProject) {
                    item.authorName = item.$relations.authorProject.name;
                    item.authorImage = item.$relations.authorProject.image_url
                }
                else if (item.$relations.authorOrganization) {
                    item.authorName = item.$relations.authorOrganization.name;
                    item.authorImage = item.$relations.authorOrganization.image_url
                }
            }

            for (let item of posts.rows) {
                const liked = await Inspired.query()
                    .where('post_inspiration_id', item.id)
                    .andWhere('user_inspired_id', user.id)
                    .first()

                liked ? item.liked = true : item.liked = false;
            }

            return posts
        }
        catch(error) {
            return error.message
        }
    }

}

module.exports = PostController;
