FROM node:12-slim

ENV NPM_CONFIG_LOGLEVEL error

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm i -g @adonisjs/cli
RUN npm ci
