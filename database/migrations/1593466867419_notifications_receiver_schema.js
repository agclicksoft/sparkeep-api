'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NotificationsReceiverSchema extends Schema {
  up () {
    this.create('notifications_receivers', (table) => {
      table.increments()
      table.integer('notification_id').unsigned().references('id').inTable('notifications')
      table.integer('user_receiver_id').unsigned().references('id').inTable('users')
      table.boolean('viewed').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('notifications_receivers')
  }
}

module.exports = NotificationsReceiverSchema
