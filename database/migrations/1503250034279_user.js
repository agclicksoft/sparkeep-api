'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('email', 254).nullable().unique()
      table.string('password', 60).notNullable()
      table.string('name', 254).nullable()
      table.text('description', 6000).nullable()
      table.string('image_url', 500).nullable()
      table.string('cellphone', 20).nullable()
      table.string('address_country', 100).nullable()
      table.string('address_state', 100).nullable()
      table.string('address_city', 200).nullable()
      table.string('address_neighborhood', 200).nullable()
      table.string('address_street', 200).nullable()
      table.string('address_zip_code', 15).nullable()
      table.string('address_complement', 200).nullable()
      table.string('address_number', 10).nullable()
      table.string('help_duration', 255).nullable()
      table.text('help_description', 6000 ).nullable()
      table.boolean('accepted_terms', 255).nullable().defaultTo(false)
      table.string('socialid', 255).nullable()
      table.string('provider', 255).nullable()
      table.string('interest_region_city', 254).nullable()
      table.string('interest_region_state', 254).nullable()
      table.boolean('receive_administrator_proposals').defaultTo(true)
      table.string('telephone', 254).nullable()
      table.string('youtube', 254).nullable()
      table.string('facebook', 254).nullable()
      table.string('instagram', 254).nullable()
      table.string('twitter', 254).nullable()
      table.string('site', 254).nullable()
      table.boolean('exc',).defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
