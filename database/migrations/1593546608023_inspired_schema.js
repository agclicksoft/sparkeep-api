'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InspiredSchema extends Schema {
  up () {
    this.create('inspireds', (table) => {
      table.increments()
      table.integer('user_inspired_id').unsigned().references('id').inTable('users')
      table.integer('user_inspiration_id').unsigned().references('id').inTable('users')
      table.integer('organization_inspiration_id').unsigned().references('id').inTable('organizations')
      table.integer('post_inspiration_id').unsigned().references('id').inTable('posts')
      table.timestamps()
    })
  }

  down () {
    this.drop('inspireds')
  }
}

module.exports = InspiredSchema
