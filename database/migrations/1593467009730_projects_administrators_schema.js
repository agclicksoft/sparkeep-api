'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectsAdministratorsSchema extends Schema {
  up () {
    this.create('projects_administrators', (table) => {
      table.increments()
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('status', 40).defaultTo('pending')
      table.timestamps()
    })
  }

  down () {
    this.drop('projects_administrators')
  }
}

module.exports = ProjectsAdministratorsSchema
