'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrganizationsSchema extends Schema {
  up () {
    this.create('organizations', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.string('owner', 255).nullable()
      table.string('name', 255).nullable()
      table.string('description', 6000).nullable()
      table.string('local', 255).nullable()
      table.string('address_country', 255).nullable()
      table.string('address_state', 255).nullable()
      table.string('address_city', 255).nullable()
      table.string('address_neighborhood', 255).nullable()
      table.string('address_street', 255).nullable()
      table.string('address_complement', 255).nullable()
      table.string('cnpj', 20).nullable().unique()
      table.string('cpf', 20).nullable()
      table.string('philantropy_record', 254)
      table.boolean('has_philantropy').nullable()
      table.string('telephone', 15).nullable()
      table.boolean('show_telephone').nullable()
      table.string('cellphone', 15).nullable()
      table.boolean('show_cellphone').nullable()
      table.string('image_url', 500).nullable()
      table.string('email', 254).nullable()
      table.string('youtube', 254).nullable()
      table.string('facebook', 254).nullable()
      table.string('instagram', 254).nullable()
      table.string('twitter', 254).nullable()
      table.string('site', 254).nullable()
      table.boolean('exc',).defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.raw('SET foreign_key_checks = 0;')
    this.drop('organizations')
  }
}

module.exports = OrganizationsSchema
