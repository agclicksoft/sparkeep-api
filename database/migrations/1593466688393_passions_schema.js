'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PassionsSchema extends Schema {
  up () {
    this.create('passions', (table) => {
      table.increments()
      table.string('name', 255).nullable()
      table.string('description', 2000).nullable()
      table.integer('order').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('passions')
  }
}

module.exports = PassionsSchema
