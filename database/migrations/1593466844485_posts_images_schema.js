'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PostsImagesSchema extends Schema {
  up () {
    this.create('posts_images', (table) => {
      table.increments()
      table.integer('post_id').unsigned().references('id').inTable('posts')
      table.string('path', 500).nullable()
      table.string('key', 500).nullable()
      table.string('name', 500).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('posts_images')
  }
}

module.exports = PostsImagesSchema
