'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserPassionsSchema extends Schema {
  up () {
    this.create('user_passions', (table) => {
      table.increments()
      table.integer('user_id', 100).unsigned().references('id').inTable('users')
      table.integer('passion_id', 100).unsigned().references('id').inTable('passions')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_passions')
  }
}

module.exports = UserPassionsSchema
