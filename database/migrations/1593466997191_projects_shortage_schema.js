'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectsShortageSchema extends Schema {
  up () {
    this.create('projects_shortages', (table) => {
      table.increments()
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.integer('shortage_id').unsigned().references('id').inTable('shortages')
      table.timestamps()
    })
  }

  down () {
    this.drop('projects_shortages')
  }
}

module.exports = ProjectsShortageSchema
