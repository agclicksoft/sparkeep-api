'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrganizationsAdministratorsSchema extends Schema {
  up () {
    this.create('organizations_administrators', (table) => {
      table.increments()
      table.integer('organization_id').unsigned().references('id').inTable('organizations')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('status', 40).defaultTo('pending')
      table.timestamps()
    })
  }

  down () {
    this.drop('organizations_administrators')
  }
}

module.exports = OrganizationsAdministratorsSchema
