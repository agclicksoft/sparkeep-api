'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShortageSchema extends Schema {
  up () {
    this.create('shortages', (table) => {
      table.increments()
      table.string('description', 200).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('shortages')
  }
}

module.exports = ShortageSchema
