'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PostsSchema extends Schema {
  up () {
    this.create('posts', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('author', 255).nullable()
      table.integer('author_user_id').unsigned().references('id').inTable('users')
      table.text('description', 6000).nullable()
      table.string('privacy', 200).nullable()
      table.boolean('exc').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('posts')
  }
}

module.exports = PostsSchema
