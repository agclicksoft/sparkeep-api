'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EventsShortagesSchema extends Schema {
  up () {
    this.create('events_shortages', (table) => {
      table.increments()
      table.integer('event_id').unsigned().references('id').inTable('events')
      table.integer('shortage_id').unsigned().references('id').inTable('shortages')
      table.timestamps()
    })
  }

  down () {
    this.drop('events_shortages')
  }
}

module.exports = EventsShortagesSchema
