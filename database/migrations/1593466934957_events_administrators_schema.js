'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EventsAdministratorsSchema extends Schema {
  up () {
    this.create('events_administrators', (table) => {
      table.increments()
      table.integer('event_id').unsigned().references('id').inTable('events')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('status', 40).defaultTo('pending')
      table.timestamps()
    })
  }

  down () {
    this.drop('events_administrators')
  }
}

module.exports = EventsAdministratorsSchema
