'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserInterestRegionSchema extends Schema {
  up () {
    this.create('user_interest_regions', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('interest_region_city', 254).nullable()
      table.string('interest_region_state', 254).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('user_interest_regions')
  }
}

module.exports = UserInterestRegionSchema
