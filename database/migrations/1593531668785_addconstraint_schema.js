'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddconstraintSchema extends Schema {
  up () {
    this.table('posts', (table) => {
      table.integer('author_event_id').unsigned().references('id').inTable('events')
      table.integer('author_organization_id').unsigned().references('id').inTable('organizations')
      table.integer('author_project_id').unsigned().references('id').inTable('projects')
    })

    this.table('events', (table) => {
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.integer('organization_id').unsigned().references('id').inTable('organizations')
    })

    this.table('projects', (table) => {
      table.integer('organization_id').unsigned().references('id').inTable('organizations')
    })

    this.table('notifications', (table) => {
      table.integer('event_send_id').unsigned().references('id').inTable('events')
      table.integer('organization_send_id').unsigned().references('id').inTable('organizations')
      table.integer('project_send_id').unsigned().references('id').inTable('projects')
      table.integer('partnership_id').unsigned().references('id').inTable('partners')
    })
}
  down () {
    this.table('addconstraints', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddconstraintSchema
