'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PartnersSchema extends Schema {
  up () {
    this.create('partners', (table) => {
      table.increments()
      table.integer('user_send_id').unsigned().references('id').inTable('users')
      table.integer('user_receiver_id').unsigned().references('id').inTable('users')
      table.integer('project_send_id').unsigned().references('id').inTable('projects')
      table.integer('project_receiver_id').unsigned().references('id').inTable('projects')
      table.integer('organization_send_id').unsigned().references('id').inTable('organizations')
      table.integer('organization_receiver_id').unsigned().references('id').inTable('organizations')
      table.integer('event_send_id').unsigned().references('id').inTable('events')
      table.integer('event_receiver_id').unsigned().references('id').inTable('events')
      table.integer('shortage_id').unsigned().references('id').inTable('shortages')
      table.integer('maintenance_time_id').unsigned().references('id').inTable('maintenance_times')
      table.string('message', 255).nullable()
      table.string('partnership_agreement', 255).nullable()
      table.string('status').defaultTo('0')
      table.boolean('viewed').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.raw('SET foreign_key_checks = 0;')
    this.drop('partners')
  }
}

module.exports = PartnersSchema
