'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EventsConfirmedPeopleSchema extends Schema {
  up () {
    this.create('events_confirmed_people', (table) => {
      table.increments()
      table.integer('event_id').unsigned().references('id').inTable('events')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('events_confirmed_people')
  }
}

module.exports = EventsConfirmedPeopleSchema
