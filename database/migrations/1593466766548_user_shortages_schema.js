'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserShortageSchema extends Schema {
  up () {
    this.create('user_shortages', (table) => {
      table.increments()
      table.integer('user_id', 100).unsigned().references('id').inTable('users')
      table.integer('shortage_id', 100).unsigned().references('id').inTable('shortages')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_shortages')
  }
}

module.exports = UserShortageSchema
