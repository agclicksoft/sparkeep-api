'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectsSchema extends Schema {
  up () {
    this.create('projects', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('name', 255).nullable()
      table.string('description', 6000).nullable()
      table.string('local', 255).nullable()
      table.string('telephone', 20).nullable()
      table.string('cellphone', 20).nullable()
      table.boolean('show_telephone').defaultTo(true)
      table.boolean('show_cellphone').defaultTo(false)
      table.string('address_country', 255).nullable()
      table.string('address_state', 255).nullable()
      table.string('address_city', 255).nullable()
      table.string('address_neighborhood', 255).nullable()
      table.string('address_street', 255).nullable()
      table.string('address_complement', 255).nullable()
      table.string('image_url', 500).nullable()
      table.string('kind_of_help', 500).nullable()
      table.string('interest_region', 254).nullable()
      table.string('email', 254).nullable()
      table.string('youtube', 254).nullable()
      table.string('facebook', 254).nullable()
      table.string('instagram', 254).nullable()
      table.string('twitter', 254).nullable()
      table.string('site', 254).nullable()
      table.boolean('has_shortages').nullable()
      table.boolean('exc',).defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.raw('SET foreign_key_checks = 0;')
    this.drop('projects')
  }
}

module.exports = ProjectsSchema
