'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrganizationsPassionsSchema extends Schema {
  up () {
    this.create('organizations_passions', (table) => {
      table.increments()
      table.integer('organization_id').unsigned().references('id').inTable('organizations')
      table.integer('passion_id').unsigned().references('id').inTable('passions')
      table.timestamps()
    })
  }

  down () {
    this.drop('organizations_passions')
  }
}

module.exports = OrganizationsPassionsSchema
