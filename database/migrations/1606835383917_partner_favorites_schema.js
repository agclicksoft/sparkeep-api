'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PartnerFavoritesSchema extends Schema {
  up () {
    this.create('partner_favorites', (table) => {
      table.increments()
      table.integer('partner_id').unsigned().references('id').inTable('partners');
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('partner_favorites')
  }
}

module.exports = PartnerFavoritesSchema
