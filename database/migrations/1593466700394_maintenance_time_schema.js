'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MaintenanceTimeSchema extends Schema {
  up () {
    this.create('maintenance_times', (table) => {
      table.increments()
      table.string('description', 200).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('maintenance_times')
  }
}

module.exports = MaintenanceTimeSchema
