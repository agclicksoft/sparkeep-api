'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EventsMaintenanceTimesSchema extends Schema {
  up () {
    this.create('events_maintenance_times', (table) => {
      table.increments()
      table.integer('event_id').unsigned().references('id').inTable('events')
      table.integer('maintenance_time_id').unsigned().references('id').inTable('maintenance_times')
      table.timestamps()
    })
  }

  down () {
    this.drop('events_maintenance_times')
  }
}

module.exports = EventsMaintenanceTimesSchema
