'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserConnectionsSchema extends Schema {
  up () {
    this.create('user_connections', (table) => {
      table.increments()
      table.integer('user_send_id', 100).unsigned().references('id').inTable('users')
      table.integer('user_receiver_id', 100).unsigned().references('id').inTable('users')
      table.boolean('status').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('user_connections')
  }
}

module.exports = UserConnectionsSchema
