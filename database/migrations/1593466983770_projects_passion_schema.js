'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectsPassionSchema extends Schema {
  up () {
    this.create('projects_passions', (table) => {
      table.increments()
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.integer('passion_id').unsigned().references('id').inTable('passions')
      table.timestamps()
    })
  }

  down () {
    this.drop('projects_passions')
  }
}

module.exports = ProjectsPassionSchema
