'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EventsPassionsSchema extends Schema {
  up () {
    this.create('events_passions', (table) => {
      table.increments()
      table.integer('event_id').unsigned().references('id').inTable('events')
      table.integer('passion_id').unsigned().references('id').inTable('passions')
      table.timestamps()
    })
  }

  down () {
    this.drop('events_passions')
  }
}

module.exports = EventsPassionsSchema
