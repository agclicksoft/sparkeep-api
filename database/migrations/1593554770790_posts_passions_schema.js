'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PostsPassionsSchema extends Schema {
  up () {
    this.create('posts_passions', (table) => {
      table.increments()
      table.integer('post_id').unsigned().references('id').inTable('posts')
      table.integer('passion_id').unsigned().references('id').inTable('passions')
      table.timestamps()
    })
  }

  down () {
    this.drop('posts_passions')
  }
}

module.exports = PostsPassionsSchema
