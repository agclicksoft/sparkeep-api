'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectsMaintenanceTimesSchema extends Schema {
  up () {
    this.create('projects_maintenance_times', (table) => {
      table.increments()
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.integer('maintenance_time_id').unsigned().references('id').inTable('maintenance_times')
      table.timestamps()
    })
  }

  down () {
    this.drop('projects_maintenance_times')
  }
}

module.exports = ProjectsMaintenanceTimesSchema
